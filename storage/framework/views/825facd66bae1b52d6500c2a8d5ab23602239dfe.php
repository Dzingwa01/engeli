

<?php $__env->startSection('content'); ?>
    <br>
    <br>
    <br>
    <div class="card"style="width:700px;margin:  0 auto;">
        <br>
        <div class="row">
            <div class="col s4">
                <img style="width:150px;height: 100px;margin-left: 530px;" src="/images/home/logoo.png" class="engeliLogo"/>
            </div>
            <div class="col s4">
                <h6 style="margin-left: 10px;font-size: 2em;color: #9d5821"><strong>Uploads</strong></h6>
            </div>
        </div>
        <div class="row"style="margin-left: 10px;">
            <div class="col s4">
                <h6 style="font-size: 2em;"><?php echo e($library->title); ?></h6>
            </div>
        </div>
        <div class="row"style="margin-left: 10px">
            <div class="col s4">
                <p><?php echo e($library->description); ?></p>
            </div>
        </div>
        <div class="row"style="margin-left: 10px">
            <div class="col s4">
                <?php if(isset($library->image_url)?$library->image_url:'Not set'): ?>
                    <h6>Uploaded Image:</h6>
                    <img src="<?php echo e($library->image_url); ?>" style="width:650px; height:200px;">
                    <a href="<?php echo e($library->image_url); ?>" download>Download Image</a>
                    <?php if($library->image_url == 'Not set'): ?>
                    <p>No image has been uploaded.</p>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="row"style="margin-left: 10px">
            <div class="col s4">
                <?php if(isset($library->pdf_url)?$library->pdf_url:'Not set'): ?>
                    <h6>PDF File</h6>
                    <a href="<?php echo e($library->pdf_url); ?>" download>Download PDF</a>
                    <?php if($library->pdf_url == 'Not set'): ?>
                        <p>No pdf file uploaded</p>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="row"style="margin-left: 10px">
            <div class="col s4">
                <?php if(isset($library->pdf_url)?$library->video_url:'Not set'): ?>
                    <h6>Video</h6>
                    <a href="<?php echo e($library->video_url); ?>" download>Download PDF</a>
                    <?php if($library->video_url == 'Not set'): ?>
                        <p>No video uploaded</p>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
        <br>
    </div>
    <br>
    <br>

    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.clerk-layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views//clerk-views/library-show.blade.php ENDPATH**/ ?>