<?php $__env->startSection('content'); ?>
    <link rel="stylesheet" type="text/css" href="/css/about/about.css" />

    <div class="section">
        <div class="row">
            <div class="parallax-container">
                <div class="parallax">
                    <img src="/images/home/msp_1410_6274.jpg">
                </div>
                <h1 class="header1" style="color:white;margin-left: 1100px;margin-top: 50vh;text-shadow: 5px 3px 1px #000000;">About Us</h1>
            </div>
        </div>
    </div>

    <div class="section center-align"style="margin-left: 2em;margin-right: 2em;">
        <div class="row left-align">
            <div class="col s4" id="">
                <div class="standard-left-buttons homepage">
                    <h6 class="standard-left-button-headers ">Home</h6>
                </div>
                <div class="standard-left-buttons ourpeople">
                    <h6 class="standard-left-button-headers ">Engeli Team</h6>
                </div>
                <div class="standard-left-buttons ourMeth">
                    <h6 class="standard-left-button-headers ">Our Methodology</h6>
                </div>
                <div class="standard-left-buttons serv">
                    <h6 class="standard-left-button-headers ">Services</h6>
                </div>
                <div class="standard-left-buttons docu">
                    <h6 class="standard-left-button-headers ">Documents</h6>
                </div>
                <div class="standard-left-buttons cntUS">
                    <h6 class="standard-left-button-headers ">Contact Us</h6>
                </div>
            </div>

            <div class="col s1"></div>
            <div class="col s7">
                <div class="row">
                    <p style="color: #9d5821;"><strong>Engeli Enterprise Development(Pty) Ltd Engeli was established in 2011 by a group
                            of Enterprise Development Specialists who has
                            extensive experience, both locally and abroad. The team consists of Engineers, Business Support Specialist, Lawyers
                            and Accountants..&nbsp;Engeli was established in 2011 by a group of Enterprise Development Specialists who has extensive
                            experience, both locally and abroad. The team consists of Engineers, Business Support Specialists, Lawyers
                            and Accountants.&nbsp;</strong></p>

                    <p style="color: #9d5821;"><strong>Engeli is a 51% black owned and 46% black women owned business focusing on supplier development
                            (transformation and localisation), business incubation and skills development.&nbsp;</strong></p>

                    <p style="color: #9d5821;"><strong>Engeli has also established an Employee Empowerment Trust for the benefit of their black owned women staff
                            which has a direct interest in Engeli Enterprise Development (Pty) Ltd.&nbsp;</strong></p>

                    <p style="color: #9d5821;"><strong>Engeli is a verified Black Fund Manager trading under FSCA (Financial Sector Conduct Authority)licence number 48139.&nbsp;</strong></p>


                    <h6 style="color: #9d5821;font-size: 1em;">Our purpose :</h6>
                    <p>We believe that equipping entrepreneurs and enterprises with our innovative business solutions will contribute&nbsp;
                    towards robust economic development. We focus on addressing the primary needs of Small Enterprises, with the core thrust of Engeli’s
                        efforts being aligned to addressing three fundamental issues and deficits that plague Small Enterprises (SE’s). These are;&nbsp;</p>
                    <ul>
                        <li>• Access to Markets&nbsp;</li>
                        <li>• Access to Finance&nbsp;</li>
                        <li>• Access to high level business support and mentorship&nbsp;</li>
                    </ul>


                    <div class="row">
                        <h6 style="color: #9d5821;font-size: 1em;">Our Offerings includes:&nbsp;</h6>

                        <div class="">
                            <ul>
                                <li>• Business Analysis and Assessment  </li>
                                <li>• Entrepreneurial profiling&nbsp;</li>
                                <li>• Private sector development&nbsp;</li>
                                <li>• Localisation&nbsp</li>
                                <li>• Design and roll out of business support centres&nbsp;</li>
                                <li>• Design and implementation of Business Incubators&nbsp;</li>
                                <li>• Development, accreditation and delivering of training programmes&nbsp;</li>
                                <li>• BBBEE consulting&nbsp;</li>
                                <li>• ED Strategy formulation and implementation&nbsp;</li>
                                <li>• Market Development&nbsp;</li>
                                <li>• Finance Raising&nbsp;</li>
                                <li>• Product and process innovation&nbsp;</li>
                                <li>• Sector studies and optimization&nbsp;</li>

                            </ul>
                        </div>
                    </div>

                    <p>The Engeli team has been working within the business development environment for many years now and are appropriately skilled and experienced in
                        both the technological and business aspects of enterprise development and support to provide the required technical
                        assistance to small businesses.&nbsp;</p>
                    <p>Engeli are of the opinion that a synergistic approach is necessary to grow and sustain Small Enterprises, in support of wealth and job creation.</p>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.homepage').on('click', function () {
                location.href = "/"
            });
            $('.ourpeople').on('click', function () {
                location.href = "/home/engeliTeam"
            });
            $('.ourMeth').on('click', function () {
                location.href = "/home/ourMethodology"
            });
            $('.serv').on('click', function () {
                location.href = "/home/ourServices"
            });
            $('.docu').on('click', function () {
                location.href = "/home/documents"
            });
            $('.cntUS').on('click', function () {
                location.href = "/home/contactUs"
            });
        });

    </script>



    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>