<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>Engeli Platform</title>
    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    
    <link href="/css/site-styles.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <!-- Compiled and minified JavaScript -->
</head>
<body>

<div class="navbar-fixed">
    <nav class="white" role="navigation" style="height: 100px!important;">
        <div class="nav-wrapper">
            <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <a id="logo-container" href="<?php echo e(url('/home')); ?>" class="brand-logo center">
                <img style="width:260px;height: 100px;margin:0 auto;" src="/images/home/logoo.png" class="engeliLogo"/>
            </a>
            <ul class="right hide-on-med-and-down">
                <li><a class="dropdown-trigger" href="#!" data-target="dropdown1">Account<i
                                class="material-icons right">arrow_drop_down</i></a></li>
            </ul>
        </div>
    </nav>
    <ul id="dropdown1" class="dropdown-content">
        <li><a style="color:black;" href="<?php echo e(url('/logout')); ?>" class=""
               onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                Sign Out
            </a></li>
    </ul>
    <ul id="slide-out" class="sidenav ">
        <li><div class="user-view">
                <a href="#user"><img class="circle" src="<?php echo e(!is_null(\Illuminate\Support\Facades\Auth::user()->profile_picture_url)?'/storage/'.\Illuminate\Support\Facades\Auth::user()->profile_picture_url:'/img/profile_placeholder.jpg'); ?>"/></a>
                <a href="#name"><span class="name" style="color:black;font-weight: bolder"><?php echo e(\Illuminate\Support\Facades\Auth::user()->name . " ".\Illuminate\Support\Facades\Auth::user()->surname); ?></span></a>
                <a href="#email"><span class="email" style="color:black;font-weight: bolder"><?php echo e(\Illuminate\Support\Facades\Auth::user()->roles[0]->display_name); ?></span></a>

            </div></li>
        <li>
            <div class="user-view">
                


            </div>
        </li>
        <div class="divider"></div>

        <ul class="collapsible popout" style="margin-top:1em;" onclick="dashboard_show()">
            <li>
                <div class="collapsible-header" style="color:black;font-weight: bolder"><i class="tiny material-icons">home</i><a
                            href="/home" class="" style="color:black;"> Home</a>
                </div>
                <div class="collapsible-body">
                </div>
            </li>
        </ul>
        <ul class="collapsible popout" style="margin-top:1em;">
            <li>
                <div class="collapsible-header" style="color:black;font-weight: bolder"><i class="tiny material-icons">event_note</i>
                    Enquiries
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a style="color:black;font-weight: bolder" class="" href="<?php echo e(url('/home/enquiry-index')); ?>"><i
                                        class="tiny material-icons">business_center</i>Manage Enquiries</a></li>
                    </ul>
                </div>
            </li>
        </ul>
        <ul class="collapsible popout" style="margin-top:1em;">
            <li>
                <div class="collapsible-header" style="color:black;font-weight: bolder"><i class="tiny material-icons">attachments</i>
                    Uploads
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a style="color:black;font-weight: bolder" class="" href="<?php echo e(url('/home/library-uploads')); ?>"><i
                                        class="tiny material-icons">attachments</i>Upload file</a></li>
                        <li><a style="color:black;font-weight: bolder" class="" href="<?php echo e(url('/home/library-index')); ?>"><i
                                        class="tiny material-icons">attachments</i>View Uploads</a></li>
                    </ul>
                </div>
            </li>
        </ul>

        <ul class="collapsible popout" style="margin-top:1em;">
            <li>
                <div class="collapsible-header" style="color:black;font-weight: bolder"><i class="tiny material-icons">people</i>
                    Users
                </div>
                <div class="collapsible-body">
                    <ul>
                        <li><a style="color:black;font-weight: bolder" class="" href="<?php echo e(url('/users/create-users')); ?>"><i
                                        class="tiny material-icons">people</i>Add Users</a></li>
                        <li><a style="color:black;font-weight: bolder" class="" href="<?php echo e(url('/users/index')); ?>"><i
                                        class="tiny material-icons">people</i>View Users</a></li>
                    </ul>
                </div>
            </li>
        </ul>



        <div class="divider"></div>

        <ul class="collapsible popout" style="margin-top:1em;">
            <li>
                <div class="collapsible-header" style="color:black;font-weight: bolder" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                    <a style="color:black;" href="<?php echo e(url('/logout')); ?>" class=""
                    ><i
                                class="tiny material-icons">vpn_key</i>
                        Sign Out
                    </a>
                </div>
                <div class="collapsible-body">
                </div>
            </li>
        </ul>

    </ul>
    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
        <?php echo csrf_field(); ?>
    </form>
</div>
<div class="container-fluid">
    <?php echo $__env->yieldContent('content'); ?>
</div>

</div>
<style>
    .sidenav-overlay {
        z-index: 996;
    }

    @media  only screen and (min-width: 993px) {
        nav a.sidenav-trigger {
            display: inline;
        }
    }

    nav a {
        color: black !important;
        font-weight: bolder !important;
    }

</style>
<!--  Scripts-->

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>




<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script>
    let options = {
        format: 'yyyy-mm-dd'
    }
    document.addEventListener('DOMContentLoaded', function () {
        var elems = document.querySelectorAll('.datepicker');
        var instances = M.Datepicker.init(elems, options);
    });

    function bulk_mailer() {
        window.location.href = '/bulk-mailer';
    }

    $(document).ready(function () {
        console.log("initializing");
        $('input.autocomplete').autocomplete({
            data: {
                "Apple": null,
                "Microsoft": null,
                "Google": 'https://placehold.it/250x250'
            },
        });
        $('.collapsible').collapsible();
        $('.sidenav').sidenav();
        $(".dropdown-trigger").dropdown();
        $('.carousel').carousel();
        $('.tabs').tabs();
        $('.modal').modal();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

    function dashboard_show() {
        window.location.href = '/home';
    }

    function reports_show() {

    }

</script>
<?php echo $__env->yieldPushContent('custom-scripts'); ?>
</body>
</html>
<?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views/layouts/admin-layout.blade.php ENDPATH**/ ?>