

<?php $__env->startSection('content'); ?>
    <br>
    <br>
    <br>

    <div class="container-fluid" style="margin-top: 2em;">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;color: #9d5821">Uploads</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="doc-table">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>PDF</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Upload" href="<?php echo e(url('/home/library-upload')); ?>">
            <i class="large material-icons">add</i>
        </a>

    <script>
        $(document).ready(function () {
            $(function () {
                $('#doc-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '<?php echo e(route('get-clerk-library')); ?>',
                    columns: [
                        {data: 'title', name: 'title'},
                        {data: 'description', name: 'description'},
                        {data: 'image_url', name: 'image_url'},
                        {data: 'pdf_url', name: 'pdf_url'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="doc-table_length"]').css("display","inline");
            });

        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.clerk-layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views//clerk-views/library-index.blade.php ENDPATH**/ ?>