

<?php $__env->startSection('content'); ?>
    <div class="container-fluid">
        <div class="card">
            <br>
            <div class="row">
                <h6 style="text-align: center;font-size: 2em;color: #9d5821"><strong>Uploads</strong></h6>
            </div>
            <h6 style="text-align:center;font-size: 1.3em;"><?php echo e($library->title); ?></h6>
            <h6><?php echo e($library->category); ?></h6>

            <div class="row" style="margin-left:2em;margin-right: 2em;">
                <?php if(isset($library->images)): ?>
                    <?php $__currentLoopData = $library->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col s6 card">
                            <div class="card-image">
                                <img src="<?php echo e('/storage/'.$upload->image_url); ?>" style="width:650px; height:200px;">
                            </div>
                            <div class="card-content">
                                <p><?php echo e($library->description); ?></p>
                            </div>

                            <div class="card-action">
                                <a href="<?php echo e('/storage/'.$upload->image_url); ?>" download>Download Image</a>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                    <p>No image has been uploaded.</p>
                <?php endif; ?>
            </div>
            <div class="row" style="margin-left: 10px">
                <div class="col s4">
                    <?php if(isset($library->pdfs)): ?>
                        <h6>PDF File</h6>
                        <?php $__currentLoopData = $library->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <a href="<?php echo e($upload->pdf_url); ?>" download>Download PDF</a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <p>No pdf file uploaded</p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col s4">
                    <?php if(isset($library->videos)): ?>
                        <?php $__currentLoopData = $library->videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <a href="<?php echo e('/storage/'.$upload->video_url); ?>">Watch this video</a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                        <p>No video uploaded</p>
                    <?php endif; ?>
                </div>
            </div>
            <br>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views/home/library-show.blade.php ENDPATH**/ ?>