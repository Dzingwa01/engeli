

<?php $__env->startSection('content'); ?>
    <br>
    <div class="container-fluid">
        <div class="" style="margin-left: 10em">
            <br>
            <div class="row">
                <h6 style="text-align: center;font-size: 2em;color: #9d5821"><strong>Uploads</strong></h6>
            </div>
            <div class="row" style="margin-left:2em;margin-right: 2em;">
                <h6 style="text-align:center;font-size: 1.3em;"><?php echo e($library->title); ?></h6>
                <br>
                <h6><?php echo e($library->category); ?></h6>
                <?php if(isset($library->excel)): ?>
                    <?php $__currentLoopData = $library->excel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <a href="<?php echo e($upload->excel_sheet); ?>"><?php echo e($library->title); ?></a>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </div>
            <div class="row" style="margin-left: 10px">
                <div class="col s4">
                    <?php if(isset($library->pdfs)): ?>
                        <?php $__currentLoopData = $library->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <a href="<?php echo e($upload->pdf_url); ?>"><?php echo e($library->title); ?></a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row" style="margin-left: 10px">
                <div class="col s4">
                    <?php if(isset($library->videos)): ?>
                        <?php $__currentLoopData = $library->videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <a href="<?php echo e($upload->video_url); ?>"><?php echo e($library->title); ?></a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row" style="margin-left: 10px">
                <div class="col s4">
                    <?php if(isset($library->images)): ?>
                        <?php $__currentLoopData = $library->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <img src="<?php echo e($upload->image_url); ?>" width="400" height="300">
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
            <br>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views//home/library-show.blade.php ENDPATH**/ ?>