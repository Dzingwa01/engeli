
<head>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
</head>

<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="card hoverable" style="margin-top: 5em!important;">
        <br>
        <div class="row">
            <div class="col s12">
                <ul class="tabs">
                    <li class="tab col s3"><a class="active" href="#test1">Images</a></li>
                    <li class="tab col s3"><a  href="#test2">Videos</a></li>
                    <li class="tab col s3"><a href="#test3">PDF Document</a></li>
                    <li class="tab col s3"><a href="#test4">Excel Sheet</a></li>

                </ul>
            </div>
            <!--Images-->
            <div id="test1" class="col s12">
                <form class="col s12">
                    <label style="font-size: 1.2em;margin-left: 300px;color: #9d5821"><strong>Image Uploading</strong></label>

                    <div class="row"style="margin-right: 2em;margin-left: 2em;">
                        <div class="input-field col s6">
                            <input id="image-title" type="text" class="validate">
                            <label for="image-title">Title</label>
                        </div>
                        <div class="input-field col s6">
                            <textarea id="image-description" type="text" class="materialize-textarea"></textarea>
                            <label for="image-description">Description</label>
                        </div>
                    </div>
                    <div class="row"style="margin-left: 2em;margin-right: 2em;">
                        <div class="col m6">
                            <div class="file-field input-field" style="bottom:0px!important;">
                                <div class="btn">
                                    <span>Upload Image</span>
                                    <input id="image_url" type="file" name="image_url" onchange="appendImagePreviews()">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path" type="text">
                                </div>
                            </div>
                            <br/>
                            <span id="select-message"></span><br/>
                            <span id="message"></span>
                        </div>
                        <div class="input-field col m6"style="width: 500px">
                            <select id="category">
                                <option value="">Select Category</option>
                                <option value="Supply Chain Development & Localization">Supply Chain Development & Localization</option>
                                <option value="Transformation Solution">Transformation Solution</option>
                                <option value="Business Incubation">Business Incubation</option>
                                <option value="Loan Management">Loan Management</option>
                                <option value="Fund Management">Fund Management</option>
                                <option value="Private Equity Solution">Private Equity Solution</option>
                            </select>
                            <label>Category</label>
                            <br />
                        </div>
                    </div>
                    <div class="row" id="image-preview">
                    </div>

                    <div class="row" style="text-align: center;">
                        
                        <a class="waves-effect waves-light btn" id="image-upload-submit-button">Submit</a>
                    </div>

                </form>
            </div>
            <!--Videos-->
            <div id="test2" class="col s12">
                <form class="col s12">
                    <label style="font-size: 1.2em;margin-left: 300px;color: #9d5821"><strong>Video Uploading</strong></label>
                    <div class="row"style="margin-right: 2em;margin-left: 2em;">
                        <div class="input-field col s6">
                            <input id="video-title" type="text" class="validate">
                            <label for="video-title">Title</label>
                        </div>
                        <div class="input-field col s6">
                            <textarea id="video-description" type="text" class="materialize-textarea"></textarea>
                            <label for="video-description">Description</label>
                        </div>
                    </div>
                    <div class="row"style="margin-left: 2em;margin-right: 2em;">
                        <div class="col m6">
                            <div class="file-field input-field" style="bottom:0px!important;">
                                    <input id="video_url" type="text" class="validate">
                                    <label for="video_url">URL for a video</label>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="video-preview">

                    </div>

                    <div class="row" style="text-align: center;">
                        <a class="waves-effect waves-light btn" id="video-upload-submit-button">Submit</a>
                    </div>

                </form>
            </div>
            <!--PDF-->
            <div id="test3" class="col s12">
                <form class="col s12">
                    <h6 style="font-size: 1.2em;margin-left: 300px;color: #9d5821"><strong>PDF Uploads</strong></h6>
                    <div class="row"style="margin-right: 2em;margin-left: 2em;">
                        <div class="input-field col s6">
                            <input id="pdf-title" type="text" class="validate">
                            <label for="pdf-title">Title</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="pdf-description" type="text" class="validate">
                            <label for="pdf-description">Description</label>
                        </div>
                    </div>
                    <div class="input-field col m6"style="width: 500px">
                        <select id="category_pdf">
                            <option value="">Select Category</option>
                            <option value="Supply Chain Development & Localization">Supply Chain Development & Localization</option>
                            <option value="Transformation Solution">Transformation Solution</option>
                            <option value="Business Incubation">Business Incubation</option>
                            <option value="Loan Management">Loan Management</option>
                            <option value="Fund Management">Fund Management</option>
                            <option value="Private Equity Solution">Private Equity Solution</option>
                        </select>
                        <label>Category</label>
                        <br />
                    </div>
                    <div class="row"style="margin-left: 2em;margin-right: 2em;">
                        <div class="col m6">
                            <div class="file-field input-field" style="bottom:0px!important;">
                                <div class="btn">
                                    <span>Upload PDF</span>
                                    <input id="pdf_url" type="file" name="pdf_url">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path"
                                           type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 2em;margin-right: 2em;">
                        <div class="input-field col s6">
                            <input id="pdf-fileName" type="text" class="validate">
                            <label for="pdf-fileName">FileName</label>
                        </div>
                    </div>
                    <div class="row" id="pdf-preview">

                    </div>
                    <div class="row" style="text-align: center;">
                        <a class="waves-effect waves-light btn" id="pdf-upload-submit-button">Submit</a>
                    </div>
                </form>
            </div>
            <!--Excel-->
            <div id="test4" class="col s12">
                <form class="col s12">
                    <h6 style="font-size: 1.2em;margin-left: 300px;color: #9d5821"><strong>PDF Uploads</strong></h6>
                    <div class="row"style="margin-right: 2em;margin-left: 2em;">
                        <div class="input-field col s6">
                            <input id="excel-title" type="text" class="validate">
                            <label for="excel-title">Title</label>
                        </div>
                        <div class="input-field col s6">
                            <input id="excel-description" type="text" class="validate">
                            <label for="excel-description">Description</label>
                        </div>
                    </div>
                    <div class="input-field col m6"style="width: 500px">
                        <select id="category_excel">
                            <option value="">Select Category</option>
                            <option value="Supply Chain Development & Localization">Supply Chain Development & Localization</option>
                            <option value="Transformation Solution">Transformation Solution</option>
                            <option value="Business Incubation">Business Incubation</option>
                            <option value="Loan Management">Loan Management</option>
                            <option value="Fund Management">Fund Management</option>
                            <option value="Private Equity Solution">Private Equity Solution</option>
                        </select>
                        <label>Category</label>
                        <br />
                    </div>
                    <div class="row"style="margin-left: 2em;margin-right: 2em;">
                        <div class="col m6">
                            <div class="file-field input-field" style="bottom:0px!important;">
                                <div class="btn">
                                    <span>Upload Excel sheet</span>
                                    <input id="excel_sheet" type="file" name="excel_sheet">
                                </div>
                                <div class="file-path-wrapper">
                                    <input class="file-path" type="text">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-left: 2em;margin-right: 2em;">
                        <div class="input-field col s6" >
                            <input id="excel-fileName" type="text" class="validate">
                            <label for="excel-fileName">FileName</label>
                        </div>
                    </div>
                    <div class="row" id="pdf-preview">

                    </div>
                    <div class="row" style="text-align: center;">
                        <a class="waves-effect waves-light btn" id="excel-upload-submit-button">Submit</a>
                    </div>
                </form>
            </div>


        </div>

    </div>
</div>


    <script>

        let images_array =[];
        let videos_array =[];
        let pdf_array =[];
        let excel_array =[];
        let image_array_count = 0;
        let pdf_array_count = 0;
        let excel_array_count = 0;
        let video_array_count = 0;
        var image_counter=0;
        var preview_attr ="";

        $(document).ready(function () {

            $('select').formSelect();
           //Code for uploading images. Try to avoid clashes in the naming of the buttons
            $('#image-upload-submit-button').on('click', function () {
                let formData = new FormData();
                formData.append('category', $('#category').val());
                formData.append('title', $('#image-title').val());
                formData.append('description' ,$('#image-description').val());
                images_array.forEach(function(image, i){
                    formData.append('image_' + i, image);
                    image_array_count += 1;
                });
                formData.append('image_array_count', image_array_count);

                let url = "<?php echo e(route('library.store')); ?>";
                $.ajax({
                    url:url,
                    data: formData,
                    type: 'post',
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            //Put code for video uploading here. The code is similar to the image upload.Utilize the LibraryVideo controller for storing and editing
            $("#video-upload-submit-button").on('click',function () {
                let formData = new FormData();
                formData.append('title', $('#video-title').val());
                formData.append('description' ,$('#video-description').val());
                formData.append('video_url' ,$('#video_url').val());


                let url = "<?php echo e(route('video.store')); ?>";
                $.ajax({
                    url:url,
                    data: formData,
                    type: 'post',
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            //Put code for pdf uploading here. Utilize the LibraryPDF controller for storing and editing
            $("#pdf-upload-submit-button").on('click',function () {
                let formData = new FormData();
                formData.append('category', $('#category_pdf').val());
                formData.append('title', $('#pdf-title').val());
                formData.append('description' ,$('#pdf-description').val());
                formData.append('fileName' ,$('#pdf-fileName').val());

                jQuery.each(jQuery('#pdf_url')[0].files, function (i, file) {
                    pdf_array.push(file);
                });

                pdf_array.forEach(function(pdf, i){
                    formData.append('pdf_' + i, pdf);
                    console.log('pdf: ' + formData.get('pdf_' + i));
                    pdf_array_count += 1;
                });

                formData.append('pdf_array_count', pdf_array_count);
                let url = "<?php echo e(route('pdf.store')); ?>";
                $.ajax({
                    url:url,
                    data: formData,
                    type: 'post',
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                       window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            //Excel sheet
            $("#excel-upload-submit-button").on('click',function () {
                let formData = new FormData();
                formData.append('category', $('#category_excel').val());
                formData.append('title', $('#excel-title').val());
                formData.append('description' ,$('#excel-description').val());
                formData.append('fileName' ,$('#excel-fileName').val());

                jQuery.each(jQuery('#excel_sheet')[0].files, function (i, file) {
                    excel_array.push(file);
                });

                excel_array.forEach(function(excel, i){
                    formData.append('excel_' + i, excel);
                    console.log('excel: ' + formData.get('excel_' + i));
                    excel_array_count += 1;
                });

                formData.append('excel_array_count', excel_array_count);
                let url = "<?php echo e(route('excel.store')); ?>";
                $.ajax({
                    url:url,
                    data: formData,
                    type: 'post',
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });

        //Function for saving selected images to the image array
        //And appending images for preview
        function appendImagePreviews(){
            image_counter++;
            $("#message").empty(); // To remove the previous error message
            //Getting the selected file. Only one is being selected at a time in this case. You can do multiple, but its ugly
            var file = jQuery('#image_url')[0].files[0];
            var imagefile = file.type;

            //Variables for dynamic adding and removal of cards with images
            let id ="image_"+image_counter;
            let card_id = "card_"+image_counter;
            let card_action = "action_"+image_counter;

            //Appending a card to the image preview area
            $("#image-preview").append(' <div class="col s12 m6">\n' +
                '      <div class="card" id='+card_id+'>\n' +
                '        <div class="card-image">\n' +
                '          <img id='+id+'>\n' +
                '        </div>\n' +
                '        <div class="card-action">\n' +
                '          <a href="#" id='+card_action+' onclick="removePreview('+card_id+')">Delete</a>\n' +
                '        </div>\n' +
                '      </div>');

            //Only allow certain file types
            var match = ["image/jpeg", "image/png", "image/jpg"];

            if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
                // $('#'+id).attr('src', 'noimage.png');
                $("#"+card_id).remove();
                $("#message").html("<p id='error'>Please Select A valid Image File</p>" + "<h4>Note</h4>" + "<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
                image_counter--;
                return false;
            } else {
                $("#select-message").empty();
                $("#select-message").append("Click again to add more images");

                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(file);
                preview_attr = id;
                jQuery.each(jQuery('#image_url')[0].files, function (i, file) {
                    images_array.push(file);
                });
            }
        }

        //Callback to append the file to the preview section
        function imageIsLoaded(e) {
            $("#"+preview_attr).css("color", "green");
            $("#"+preview_attr).css("display", "block");
            $("#"+preview_attr).attr('src', e.target.result);
            $("#"+preview_attr).attr('width', '200px');
            $("#"+preview_attr).attr('height', '200px');
        }

        function removePreview(card_id){

            $("#"+card_id.id).remove();
        }
    </script>

    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views//home/library-uploads.blade.php ENDPATH**/ ?>