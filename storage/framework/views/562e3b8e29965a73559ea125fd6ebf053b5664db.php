<?php $__env->startSection('content'); ?>
    <link rel="stylesheet" type="text/css" href="/css/methodology/methodology.css" />


    <div class="section">
        <div class="row">
            <div class="parallax-container">
                <div class="parallax">
                    <img src="/images/home/rawpixel-983726-unsplash.jpg">
                </div>
                <h1 class="header1" style="color:white;margin-left: 950px;margin-top: 50vh;text-shadow: 5px 3px 1px #000000;">Our Methodology</h1>
            </div>
        </div>
    </div>

    <div class="section center-align">
        <div class="row left-align">
            <div class="col s4" id="">
                <div class="standard-left-buttons homepage">
                    <h6 class="standard-left-button-headers ">Home</h6>
                </div>
                <div class="standard-left-buttons abt">
                    <h6 class="standard-left-button-headers ">About Us</h6>
                </div>
                <div class="standard-left-buttons ourpeople">
                    <h6 class="standard-left-button-headers ">Engeli Team</h6>
                </div>
                <div class="standard-left-buttons ourMeth">
                    <h6 class="standard-left-button-headers ">Our Methodology</h6>
                </div>
                <div class="standard-left-buttons serv">
                    <h6 class="standard-left-button-headers ">Services</h6>
                </div>
                <div class="standard-left-buttons docu">
                    <h6 class="standard-left-button-headers ">Documents</h6>
                </div>
                <div class="standard-left-buttons cntUS">
                    <h6 class="standard-left-button-headers ">Contact Us</h6>
                </div>
            </div>

            <div class="col s1"></div>
            <div class="col s7">
                <h6 style="font-size: 2em;"><b>OUR METHODOLOGY</b></h6>
                <p ><span style="color: #9d5821;"><strong>Engeli signifies the quest to go beyond that which seems impenetrable; to create a business that knows no barriers in the pursuit of excellence for the success of all stakeholders.</strong></span></p>
                <div class="row">
                    <p>&nbsp;<img title="Our Methodology" src="http://engeli.co.za/files/6814/4126/4433/methodology_org.jpg" alt="Our Methodology" width="680" height="643"></p>			</div></div></td>
               </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.homepage').on('click', function () {
                location.href = "/"
            });
            $('.ourpeople').on('click', function () {
                location.href = "/home/engeliTeam"
            });
            $('.abt').on('click', function () {
                location.href = "/home/aboutpage"
            });
            $('.ourMeth').on('click', function () {
                location.href = "/home/ourMethodology"
            });
            $('.serv').on('click', function () {
                location.href = "/home/ourServices"
            });
            $('.docu').on('click', function () {
                location.href = "/home/documents"
            });
            $('.cntUS').on('click', function () {
                location.href = "/home/contactUs"
            });
        });

    </script>



    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>