

<?php $__env->startSection('content'); ?>
    <link rel="stylesheet" type="text/css" href="/css/OurServices/services.css" />
    <!--Desktop-->
    <div class="servicesDesktop">
        <div class="section">
            <div class="row">
                <div class="parallax-container">
                    <div class="parallax">
                        <img  src="/images/home/Services2.jpg">
                    </div>
                    <h1 class="header1" style="color:white;margin-left: 950px;margin-top: 50vh;text-shadow: 5px 3px 1px #000000;">What we do</h1>
                </div>
            </div>
        </div>
        <!--First Section Desktop-->
        <div class="section center-align"style="margin-left: 5em;margin-right: 2em;">
            <div class="row left-align">
                <div class="col s3" id="">
                    <div class="standard-left-buttons homepage">
                        <h6 class="standard-left-button-headers ">Home</h6>
                    </div>
                    <div class="standard-left-buttons ourpeople">
                        <h6 class="standard-left-button-headers ">Engeli Team</h6>
                    </div>
                    <div class="standard-left-buttons ourMeth">
                        <h6 class="standard-left-button-headers ">How we do it</h6>
                    </div>
                    <div class="standard-left-buttons serv">
                        <h6 class="standard-left-button-headers ">What we do</h6>
                    </div>
                    <div class="standard-left-buttons docu">
                        <h6 class="standard-left-button-headers ">Library</h6>
                    </div>
                    <div class="standard-left-buttons cntUS">
                        <h6 class="standard-left-button-headers ">Get in touch</h6>
                    </div>
                </div>

                <div class="col s1"></div>
                <div class="col s8">
                    <div class="row center-align">
                        <h6 style="color: #9d5821;font-size: 2em;"><strong>Services:</strong></h6>
                        <h6>What do we do?</h6>
                        <p class="text-justify">Engeli has a strong focus on the productive aspect of the economy (Manufacturing) where job and wealth creation opportunities are more prevalent due to value-chain beneficiation, technology use and innovation. Engeli is positioned to function at the Institutional Support level (Meso) by assisting existing Business Support Organisations (BSO’s) in delivering business development services (BDS) to a regional or sector specific grouping of businesses. Engeli will also assist in conceptualising, designing and implementing new BSO’s that deliver to specific target markets. This will include the establishment of Sectoral Business Incubators that have a focus on manufacturing, innovation and technology.</p>
                        <div>&nbsp;</div>
                        <p>Engeli remains focussed on addressing the primary needs of Small Enterprises, with the core thrust of Engeli’s efforts being aligned to addressing three fundamental issues and deficits that plague Small Enterprises (SE’s). These are;</p>
                        <div>
                            <ul>
                                <li><b>Access to Markets&nbsp;</b></li>
                                <li><b>Access to Finance&nbsp;</b></li>
                                <li><b>Access to high level business support and mentorship&nbsp;</b></li>
                            </ul>
                        </div>
                        <div>Engeli are of the opinion that this focus, in conjunction with a manufacturing/technology/innovation bias, is necessary to grow and sustain Small Enterprises, in support of wealth and much needed job creation. The holistic approach is indicated in the diagram below.</div>
                        <div>&nbsp;</div>
                        
                        <div>&nbsp;</div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        

                        
                    </div>
                </div>
            </div>
        </div>
        <!--Second Section-->
        <div class="row"style="margin-left: 550px;margin-top: -50px">
            <p style="color: saddlebrown"><b>Click on the blocks to view more info</b></p>
            <div class="row">
                <div style="position:relative;">
                    <div class="waves-effect waves-light  modal-trigger hoverable"href="#modal1" style="float:left; width: 300px; height:100px; background-color:saddlebrown;border-radius: 20px;margin-right: 2em">
                        <h6 class="center-align"style="color: white;margin-top: 5vh">Supply Chain Development & Localisation</h6>
                    </div>
                    <div class="waves-effect waves-light  modal-trigger hoverable"href="#modal2" style="float:left; width: 300px; height:100px; background-color:grey;border-radius: 20px;">
                        <h6 class="center-align"style="color: white;margin-top: 5vh">Transformation Solutions</h6>

                    </div>
                </div>
            </div>
            <div class="row">
                <div  style="position:relative;">
                    <div class="waves-effect waves-light  modal-trigger hoverable"href="#modal3"  style="float:left; width: 300px; height:100px; background-color:grey;border-radius: 20px;margin-right: 2em">
                        <h6 class="center-align"style="color: white;margin-top: 5vh">Business Incubation</h6>
                    </div>
                    <div class="waves-effect waves-light  modal-trigger hoverable"href="#" style="float:left; width: 300px; height:100px; background-color:saddlebrown;border-radius: 20px">
                        <h6 class="center-align"style="color: white;margin-top: 5vh">Loan Management</h6>
                    </div>
                </div>
            </div>
            <div class="row">
                <div style="position:relative;">
                    <div class="waves-effect waves-light  modal-trigger hoverable"href="#"  style="float:left; width: 300px; height:100px; background-color:saddlebrown;border-radius: 20px;margin-right: 2em">
                        <h6 class="center-align"style="color: white;margin-top: 5vh">Fund Management</h6>
                    </div>
                    <div class="waves-effect waves-light  modal-trigger hoverable"href="#" style="float:left; width: 300px; height:100px; background-color:grey;border-radius: 20px">
                        <h6 class="center-align"style="color: white;margin-top: 5vh">Private Equity Solution</h6>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Structure 1 -->
        <div id="modal1" class="modal">
            <div class="modal-content">
                <h6 style="color: #9d5821;font-size: 2em;">Supply Chain Development</h6>
                <h6>Introduction</h6>
                <p><strong>Engeli provides an integrated Supply Chain Development service to assist companies in optimising their Cost, Quality and Delivery (CQD) imperatives together with the BBBEE ratings of their suppliers. Engeli operates across a wide variety of sectors and have developed a niche in terms of supporting businesses operating in the manufacturing/technology and innovation space to support the productive (value adding) sector of the economy.</strong></p>
                <h6>Operational Methodology</h6>
                <p><strong>Engeli provides end to end development services which include:</strong></p>
                <ul>
                    <li>• Detailed analysis of the supplier database and performance metrics of the individual suppliers&nbsp;</li>
                    <li>• Identification of leveraged supply chain interventions which will provide maximum return&nbsp;</li>
                    <li>• Design of a comprehensive supplier development programme&nbsp;</li>
                    <li>• Identify specific suppliers who require business development services&nbsp;</li>
                    <li>• Comprehensive business assessment of the targeted suppliers and the identification of appropriate interventions to improve the suppliers Cost, Quality and Delivery parameters&nbsp;</li>
                    <li>• Allocation of mentors to individual suppliers&nbsp;</li>
                    <li>• Implementation of supplier business improvement interventions through direct business support or via Incubators/Accelerators (physical or virtual business support)&nbsp;</li>
                    <li>• Overall management of the Supplier Development Programme&nbsp;</li>
                    <li>• Supplier Transformation (improvement of the BBBEE credentials of suppliers) Supply localisation (sourcing imported inputs locally)&nbsp;</li>
                    <li>• Identification, assessment and development/support of new BBBEE suppliers&nbsp;</li>
                    <li>• Leveraging of government/donor financial incentives to support supplier development&nbsp;</li>
                </ul>
                <h6>BBBEE Benefits to the Corporate</h6>
                <p><strong>The BBBEE benefits of supporting 51% “Black Owned” suppliers will apply to the element of Enterprise and Supplier Development (ESD) including the sub elements of:</strong></p>
                <p>• Preferential Procurement <br>• Enterprise Development <br>• Supplier Development</p>
                <p><strong><img style="vertical-align: middle;" title="Click to download PDF" src="http://engeli.co.za/files/4414/4135/1648/pdf.png" alt="" width="50" height="41"><a href="http://engeli.co.za/downloads/EngeliSupplyChainDevelopment.pdf" target="_blank"></a></strong></p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!-- Modal Structure 2 -->
        <div id="modal2" class="modal">
            <div class="modal-content">
                <h6 style="color: #9d5821;font-size: 2em;">Value added B-BBEE Solution</h6>
                <h6>Introduction</h6>
                <p><strong>Engeli provides holistic business solutions to companies that: </strong></p>
                <ul>
                    <li>• Translate to value added BBBEE interventions</li>
                    <li>• Contribute positively to their bottom line</li>
                    <li>• Foster Economic Transformation</li>
                </ul>
                <h6>The Engeli Process</h6>
                <p><strong>Our consulting services that assist in optimizing your company’s BBBEE score card include the following: </strong></p>
                <ul>
                    <li>• Overall company BBBEE strategy development for all elements of the BBBEE Score card (taking cognisance of the company’s broad business strategy, market focus and the competitiveness of the environment within which the company operates)</li>
                    <li>• Collection and analysis of current BBBEE information resulting in a preliminary BBBEE Score</li>
                    <li>• Propose cost effective and relevant solutions on all elements to obtain the desired BBBEE score</li>
                    <li>• Provision of BBBEE tools to assist with the implementation of the BBBEE Programmes</li>
                    <li>• Build capacity within your company to manage BBBEE processes</li>
                    <li>• Undertake BBBEE Pre-audits</li>
                    <li>• Compilation of the BBBEE verification file for the Verification Agency</li>
                    <li>• Handle disputes with the BBBEE Verification Agency, where required</li>
                    <li>• Assist your company with migration to the updated BBBEE Codes of Good Practice</li>
                </ul>
                <h6>Solutions</h6>
                <p>Engeli’s services go beyond the “normal” consulting route – we develop and implement business specific BBBEE interventions in the elements of Ownership, Skills Development, Enterprise and Supplier Development and Socio Economic Development.</p>

            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!-- Modal Structure 3 -->
        <div id="modal3" class="modal">
            <div class="modal-content">
                <h6 style="color: #9d5821;font-size: 2em;">Business Incubation Services</h6>
                <h6>Introduction</h6>
                <p><strong>Engeli boasts more than 30 years collective business incubator management and incubation process experience – we have developed a business incubation model which pivots upon the following 3 core developmental levers v</strong>iz.</p>
                <p>&nbsp;<img title="Evaluate, Enhance, Advance" src="http://engeli.co.za/files/8814/4135/2167/enhanceadvance.png" alt="Evaluate, Enhanace, Advance" width="690" height="45"></p>
                <h6>Operational Methodology</h6>
                <p>With this in mind, Engeli has plotted a sound incubation model comprised of pre-incubation (Evaluate), incubation (Enhance) and post incubation (Advance) processes. Engeli uses a stringent incubation selection process together with intensive
                    business and entrepreneurial development programmes to ensure optimal incubation impact. We emphasise the importance of
                    holistic business development, ensuring that the core focus is targeted towards the development of the entrepreneur within
                    the context of their business opportunity so as to ensure that should the business be unsuccessful, the entrepreneur still
                    emerges with the requisite skills to start another venture.</p>
                <h6>Benefits</h6>
                <ul>
                    <li>Engeli has modelled its incubation programme to be recognised as an enterprise and supplier development solution with
                        100% of a measured entity’s spend claimed as enterprise and supplier development contribution with the aim of optimising
                        their B-BBEE scorecard points.</li>
                    <li>The New Venture Creation qualification at NQF Level 4 is embedded in the incubation model as the formal curriculum for the
                        ENHANCE incubation phase.</li>
                    <li>Engeli has developed a blueprint for incubation replication and are replicating the model in various sectors.</li>
                </ul>
                <p><img style="vertical-align: middle;" title="Click to download PDF" src="http://engeli.co.za/files/4414/4135/1648/pdf.png" alt="" width="50" height="41"><strong><a href="http://engeli.co.za/downloads/EngeliBusinessIncubationServices.pdf" target="_blank"></a></strong></p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!-- Modal Structure 4 -->
        <div id="modal4" class="modal">s
            <div class="modal-content">
                <h6 style="color: #9d5821;font-size: 2em;">Training and Skills Development</h6>
                <h6>Introduction</h6>
                <p><strong>Engeli’s training and skills development team, <span style="color: #b06110;">LEAP Entrepreneurial Development Pty Ltd</span>, a Seta accredited training provider, is a leading implementer of Enterprise and Skills Development solutions.</strong></p>
                <h6>Methodology</h6>
                <p><strong>The Engeli training and skills development programme design follows a 3 pronged approach underpinned by a strong mentoring / coaching programme and can be depicted as:</strong></p>
                <p><img title="Engeli Methodology" src="http://engeli.co.za/files/5914/4135/3237/trainingflow.png" alt="" width="690" height="180"></p>
                <p><strong>Some of the Engeli training and skills development offerings cover bothaccredited and non-accredited programmes including</strong>:</p>
                <p><img title="Engeli Training Skills" src="http://engeli.co.za/files/2414/4135/3241/trainingtable.png" alt="" width="690" height="285"></p>
                <p>Engeli focuses on the development of both the entrepreneur and the enterprise and have thus built a significant portfolio of training offerings. We also remain agile in responding to specific market needs and have the competencies to develop tailor-made material and programmes for Enterprise, Supplier and Socio-Economic Development.</p>
                <p><img style="vertical-align: middle;" title="Click to download PDF" src="http://engeli.co.za/files/4414/4135/1648/pdf.png" alt="" width="50" height="41"><strong><a href="http://engeli.co.za/downloads/EngeliTrainingSkillsDevelopment.pdf" target="_blank">Click to download pdf</a></strong></p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!-- Modal Structure 5 -->
        <div id="modal5" class="modal">
            <div class="modal-content">
                <h6 style="color: #9d5821;font-size: 2em;">Corporate Finance, Private Equity &amp; Venture Capital</h6>
                <h6>Introduction</h6>
                <p>Engeli can assist business owners with achieving their BBBEE strategy objectives by providing suitable and cost effective interventions applicable to ownership, enterprise and supplier development, procurement and socio economic development.</p>
                <h6>Solutions&nbsp;</h6>
                <ul>
                    <li>Enterprise Development and Supplier Development Loan Fund</li>
                    <li>Strategic Private Equity investments</li>
                    <li>Innovative Supplier Development interventions</li>
                    <li>Triple bottom line Enterprise and Supplier Development interventions</li>
                    <li>Flexible and cost effective BBBEE ownership solutions</li>
                </ul>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!-- Modal Structure 6 -->
        <div id="modal6" class="modal">
            <div class="modal-content">
                <h6 style="color: #9d5821;font-size: 1em;">Our Offerings includes:&nbsp;</h6>

                <div class="row">
                    <div class="col s6">
                        <ul>
                            <li>• Business Analysis and Assessment  </li>
                            <li>• Entrepreneurial profiling&nbsp;</li>
                            <li>• Private sector development&nbsp;</li>
                            <li>• Localisation&nbsp</li>
                            <li>• Design and roll out of business support centres&nbsp;</li>
                            <li>• Design and implementation of Business Incubators&nbsp;</li>
                            <li>• Development, accreditation and delivering of training programmes&nbsp;</li>
                        </ul>
                    </div>

                    <div class="col s6">
                        <ul>
                            <li>• BBBEE consulting&nbsp;</li>
                            <li>• ED Strategy formulation and implementation&nbsp;</li>
                            <li>• Market Development&nbsp;</li>
                            <li>• Finance Raising&nbsp;</li>
                            <li>• Product and process innovation&nbsp;</li>
                            <li>• Sector studies and optimization&nbsp;</li>
                        </ul>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>

    </div>
    <!--Mobile-->
    <div class="servicesMobile">
        <div class="section">
            <div class="row">
                <div class="parallax-container">
                    <div class="parallax">
                        <img src="/images/home/Services2.jpg"style="width: 600px;height: 00px;">
                    </div>
                    <h1 class="header1" style="color:white;margin-left: 180px;margin-top: 70vh;text-shadow: 5px 3px 1px #000000;font-size: 2em;">What we do</h1>
                </div>
            </div>
        </div>
        <!--First Section Desktop-->
        <div class="section"style="margin-left: 2em;margin-right: 2em;">
            <div class="row">
                <div class="col s12 center-align">
                    <div class="row">
                        <h6 style="color: saddlebrown;font-size: 2em;"><b>Services</b></h6>
                        <h6><strong>What do we do?</strong></h6>
                        <div>Engeli has a strong focus on the productive aspect of the economy (Manufacturing) where job and wealth creation opportunities are more prevalent due to value-chain beneficiation, technology use and innovation. Engeli is positioned to function at the Institutional Support level (Meso) by assisting existing Business Support Organisations (BSO’s) in delivering business development services (BDS) to a regional or sector specific grouping of businesses. Engeli will also assist in conceptualising, designing and implementing new BSO’s that deliver to specific target markets. This will include the establishment of Sectoral Business Incubators that have a focus on manufacturing, innovation and technology.</div>
                        <div>&nbsp;</div>
                        <div>Engeli remains focussed on addressing the primary needs of Small Enterprises, with the core thrust of Engeli’s efforts being aligned to addressing three fundamental issues and deficits that plague Small Enterprises (SE’s). These are;</div>
                        <div>
                            <ul>
                                <li>Access to Markets&nbsp;</li>
                                <li>Access to Finance&nbsp;</li>
                                <li>Access to high level business support and mentorship&nbsp;</li>
                            </ul>
                        </div>
                        <div>Engeli are of the opinion that this focus, in conjunction with a manufacturing/technology/innovation bias, is necessary to grow and sustain Small Enterprises, in support of wealth and much needed job creation. The holistic approach is indicated in the diagram below.</div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        

                        
                    </div>
                </div>
            </div>
        </div>
        <!--Second Section-->
        <p style="font-style: italic;margin-left: 100px;">(Click to view more info)</p>
        <div class="row"style="margin-left: 30px;">
            <div class="row">
                <div style="position:relative;">
                    <div class="waves-effect waves-light  modal-trigger hoverable"href="#modal20" style="float:left; width: 300px; height:100px; background-color:saddlebrown;border-radius: 20px">
                        <h6 class="center-align"style="color: white;margin-top: 5vh">Supply Chain Development & Localisation</h6>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="waves-effect waves-light  modal-trigger hoverable"href="#modal21" style="float:left; width: 300px; height:100px; background-color:grey;border-radius: 20px">
                    <h6 class="center-align"style="color: white;margin-top: 5vh">Transformation Solutions</h6>

                </div>
            </div>
            <div class="row">
                <div  style="position:relative;">
                    <div class="waves-effect waves-light  modal-trigger hoverable"href="#modal22"  style="float:left; width: 300px; height:100px; background-color:saddlebrown;border-radius: 20px">
                        <h6 class="center-align"style="color: white;margin-top: 5vh">Business Incubation</h6>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="waves-effect waves-light  modal-trigger hoverable"href="" style="float:left; width: 300px; height:100px; background-color:gray;border-radius: 20px">
                    <h6 class="center-align"style="color: white;margin-top: 5vh">Loan Management</h6>
                </div>
            </div>
            <div class="row">
                <div style="position:relative;">
                    <div class="waves-effect waves-light  modal-trigger hoverable"href="#" style="float:left; width: 300px; height:100px; background-color:saddlebrown;border-radius: 20px">
                        <h6 class="center-align"style="color: white;margin-top: 5vh">Fund Management</h6>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="waves-effect waves-light  modal-trigger hoverable"href="#" style="float:left; width: 300px; height:100px; background-color:grey;border-radius: 20px">
                    <h6 class="center-align"style="color: white;margin-top: 5vh">Private Equity Solution</h6>
                </div>
            </div>
        </div>

        <!-- Modal Structure 1 -->
        <div id="modal20" class="modal">
            <div class="modal-content">
                <h6 style="color: #9d5821;font-size: 2em;">Supply Chain Development</h6>
                <h6>Introduction</h6>
                <p><strong>Engeli provides an integrated Supply Chain Development service to assist companies in optimising their Cost, Quality and Delivery (CQD) imperatives together with the BBBEE ratings of their suppliers. Engeli operates across a wide variety of sectors and have developed a niche in terms of supporting businesses operating in the manufacturing/technology and innovation space to support the productive (value adding) sector of the economy.</strong></p>
                <h6>Operational Methodology</h6>
                <p><strong>Engeli provides end to end development services which include:</strong></p>
                <ul>
                    <li>Detailed analysis of the supplier database and performance metrics of the individual suppliers&nbsp;</li>
                    <li>Identification of leveraged supply chain interventions which will provide maximum return&nbsp;</li>
                    <li>Design of a comprehensive supplier development programme&nbsp;</li>
                    <li>Identify specific suppliers who require business development services&nbsp;</li>
                    <li>Comprehensive business assessment of the targeted suppliers and the identification of appropriate interventions to improve the suppliers Cost, Quality and Delivery parameters&nbsp;</li>
                    <li>Allocation of mentors to individual suppliers&nbsp;</li>
                    <li>Implementation of supplier business improvement interventions through direct business support or via Incubators/Accelerators (physical or virtual business support)&nbsp;</li>
                    <li>Overall management of the Supplier Development Programme&nbsp;</li>
                    <li>Supplier Transformation (improvement of the BBBEE credentials of suppliers) Supply localisation (sourcing imported inputs locally)&nbsp;</li>
                    <li>Identification, assessment and development/support of new BBBEE suppliers&nbsp;</li>
                    <li>Leveraging of government/donor financial incentives to support supplier development&nbsp;</li>
                </ul>
                <h6>BBBEE Benefits to the Corporate</h6>
                <p><strong>The BBBEE benefits of supporting 51% “Black Owned” suppliers will apply to the element of Enterprise and Supplier Development (ESD) including the sub elements of:</strong></p>
                <p>• Preferential Procurement <br>• Enterprise Development <br>• Supplier Development</p>
                <p><strong><img style="vertical-align: middle;" title="Click to download PDF" src="http://engeli.co.za/files/4414/4135/1648/pdf.png" alt="" width="50" height="41"><a href="http://engeli.co.za/downloads/EngeliSupplyChainDevelopment.pdf" target="_blank">Click to download pdf</a></strong></p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!-- Modal Structure 2 -->
        <div id="modal21" class="modal">
            <div class="modal-content">
                <h6 style="color: #9d5821;font-size: 2em;">Value added BBB-EE Solution</h6>
                <h6>Introduction</h6>
                <p><strong>Engeli provides holistic business solutions to companies that: </strong></p>
                <ul>
                    <li>Translate to value added BBBEE interventions</li>
                    <li>Contribute positively to their bottom line</li>
                    <li>Foster Economic Transformation</li>
                </ul>
                <h6>The Engeli Process</h6>
                <p><strong>Our consulting services that assist in optimizing your company’s BBBEE scorecard include the following: </strong></p>
                <ul>
                    <li>Overall company BBBEE strategy development for all elements of the BBBEE Score card (taking cognisance of the company’s broad business strategy, market focus and the competitiveness of the environment within which the company operates)</li>
                    <li>Collection and analysis of current BBBEE information resulting in a preliminary BBBEE Score</li>
                    <li>Propose cost effective and relevant solutions on all elements to obtain the desired BBBEE score</li>
                    <li>Provision of BBBEE tools to assist with the implementation of the BBBEE Programmes</li>
                    <li>Build capacity within your company to manage BBBEE processes</li>
                    <li>Undertake BBBEE Pre-audits</li>
                    <li>Compilation of the BBBEE verification file for the Verification Agency</li>
                    <li>Handle disputes with the BBBEE Verification Agency, where required</li>
                    <li>Assist your company with migration to the updated BBBEE Codes of Good Practice</li>
                </ul>
                <h6>Solutions</h6>
                <p>Engeli’s services go beyond the “normal” consulting route – we develop and implement business specific BBBEE interventions in the elements of Ownership, Skills Development, Enterprise and Supplier Development and Socio Economic Development.</p>

            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!-- Modal Structure 3 -->
        <div id="modal22" class="modal">
            <div class="modal-content">
                <h6 style="color: #9d5821;font-size: 2em;">Business Incubation Services</h6>
                <h6>Introduction</h6>
                <p><strong>Engeli boasts more than 30 years collective business incubator management and incubation process experience – we have developed a business incubation model which pivots upon the following 3 core developmental levers v</strong>iz.</p>
                <p>&nbsp;<img title="Evaluate, Enhance, Advance" src="http://engeli.co.za/files/8814/4135/2167/enhanceadvance.png" alt="Evaluate, Enhanace, Advance" width="690" height="45"></p>
                <h6>Operational Methodology</h6>
                <p>With this in mind, Engeli has plotted a sound incubation model comprised of pre-incubation (Evaluate), incubation (Enhance
                    ) and post incubation (Advance) processes. Engeli uses a stringent incubation selection process together with intensive
                    business and entrepreneurial development programmes to ensure optimal incubation impact. We emphasise the importance of
                    holistic business development, ensuring that the core focus is targeted towards the development of the entrepreneur within
                    the context of their business opportunity so as to ensure that should the business be unsuccessful, the entrepreneur still
                    emerges with the requisite skills to start another venture.</p>
                <h6>Benefits</h6>
                <ul>
                    <li>Engeli has modelled its incubation programme to be recognised as an enterprise and supplier development solution with
                        100% of a measured entity’s spend claimed as enterprise and supplier development contribution with the aim of optimising
                        their B-BBEE scorecard points</li>
                    <li>The New Venture Creation qualification at NQF Level 4 is embedded in the incubation model as the formal curriculum for the
                        ENHANCE incubation phase</li>
                    <li>Engeli has developed a blueprint for incubation replication and are replicating the model in various sectors</li>
                </ul>
                <p><img style="vertical-align: middle;" title="Click to download PDF" src="http://engeli.co.za/files/4414/4135/1648/pdf.png" alt="" width="50" height="41"><strong><a href="http://engeli.co.za/downloads/EngeliBusinessIncubationServices.pdf" target="_blank">Click to download pdf</a></strong></p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!-- Modal Structure 4 -->
        <div id="modal23" class="modal">
            <div class="modal-content">
                <h6 style="color: #9d5821;font-size: 2em;">Training and Skills Development</h6>
                <h6>Introduction</h6>
                <p><strong>Engeli’s training and skills development team, <span style="color: #b06110;">LEAP Entrepreneurial Development Pty Ltd</span>, a Seta accredited training provider, is a leading implementer of Enterprise and Skills Development solutions.</strong></p>
                <h6>Methodology</h6>
                <p><strong>The Engeli training and skills development programme design follows a 3 pronged approach underpinned by a strong mentoring / coaching programme and can be depicted as:</strong></p>
                <p><img title="Engeli Methodology" src="http://engeli.co.za/files/5914/4135/3237/trainingflow.png" alt="" width="690" height="180"></p>
                <p><strong>Some of the Engeli training and skills development offerings cover bothaccredited and non-accredited programmes including</strong>:</p>
                <p><img title="Engeli Training Skills" src="http://engeli.co.za/files/2414/4135/3241/trainingtable.png" alt="" width="690" height="285"></p>
                <p>Engeli focuses on the development of both the entrepreneur and the enterprise and have thus built a significant portfolio of training offerings. We also remain agile in responding to specific market needs and have the competencies to develop tailor-made material and programmes for Enterprise, Supplier and Socio-Economic Development.</p>
                <p><img style="vertical-align: middle;" title="Click to download PDF" src="http://engeli.co.za/files/4414/4135/1648/pdf.png" alt="" width="50" height="41"><strong><a href="http://engeli.co.za/downloads/EngeliTrainingSkillsDevelopment.pdf" target="_blank">Click to download pdf</a></strong></p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!-- Modal Structure 5 -->
        <div id="modal24" class="modal">
            <div class="modal-content">
                <h6 style="color: #9d5821;font-size: 2em;">Corporate Finance, Private Equity &amp; Venture Capital</h6>
                <h6>Introduction</h6>
                <p>Engeli can assist business owners with achieving their BBBEE strategy objectives by providing suitable and cost effective interventions applicable to ownership, enterprise and supplier development, procurement and socio economic development.</p>
                <h6>Solutions&nbsp;</h6>
                <ul>
                    <li>Enterprise Development and Supplier Development Loan Fund</li>
                    <li>Strategic Private Equity investments</li>
                    <li>Innovative Supplier Development interventions</li>
                    <li>Triple bottom line Enterprise and Supplier Development interventions</li>
                    <li>Flexible and cost effective BBBEE ownership solutions</li>
                </ul>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>
        <!-- Modal Structure 6 -->
        <div id="modal25" class="modal">
            <div class="modal-content">
                <h6 style="color: #9d5821;font-size: 1em;">Our Offerings includes:&nbsp;</h6>

                <div class="row">
                    <div class="col s6">
                        <ul>
                            <li>• Business Analysis and Assessment  </li>
                            <li>• Entrepreneurial profiling&nbsp;</li>
                            <li>• Private sector development&nbsp;</li>
                            <li>• Localisation&nbsp</li>
                            <li>• Design and roll out of business support centres&nbsp;</li>
                            <li>• Design and implementation of Business Incubators&nbsp;</li>
                            <li>• Development, accreditation and delivering of training programmes&nbsp;</li>
                        </ul>
                    </div>

                    <div class="col s6">
                        <ul>
                            <li>• BBBEE consulting&nbsp;</li>
                            <li>• ED Strategy formulation and implementation&nbsp;</li>
                            <li>• Market Development&nbsp;</li>
                            <li>• Finance Raising&nbsp;</li>
                            <li>• Product and process innovation&nbsp;</li>
                            <li>• Sector studies and optimization&nbsp;</li>
                        </ul>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Close</a>
            </div>
        </div>

    </div>


    <script>
        $(document).ready(function(){
            $('.modal').modal();
        });

        $('.homepage').on('click', function () {
            location.href = '/';
        });
        $('.ourpeople').on('click', function () {
            location.href = '/home/engeliTeam';
        });
        $('.ourMeth').on('click', function () {
            location.href = '/home/ourMethodology';
        });
        $('.docu').on('click', function () {
            location.href = '/home/documents';
        });
        $('.cntUS').on('click', function () {
            location.href = '/home/contactUs';
        });

        $('.ourservices').on('click', function () {
            $('.services').show();
            $('.corporate').hide();
            $('.values').hide();
            $('.business').hide();
            $('.training').hide();
            $('.supplyChain').hide();
        });
        $('.corporateFinance').on('click', function () {
            $('.services').hide();
            $('.corporate').show();
            $('.values').hide();
            $('.business').hide();
            $('.training').hide();
            $('.supplyChain').hide();
        });
        $('.valueAdded').on('click', function () {
            $('.services').hide();
            $('.corporate').hide();
            $('.values').show();
            $('.business').hide();
            $('.training').hide();
            $('.supplyChain').hide();
        });
        $('.businessInc').on('click', function () {
            $('.services').hide();
            $('.corporate').hide();
            $('.values').hide();
            $('.business').show();
            $('.training').hide();
            $('.supplyChain').hide();
        });
        $('.trainingSkills').on('click', function () {
            $('.services').hide();
            $('.corporate').hide();
            $('.values').hide();
            $('.business').hide();
            $('.training').show();
            $('.supplyChain').hide();
        });
        $('.supply').on('click', function () {
            $('.services').hide();
            $('.corporate').hide();
            $('.values').hide();
            $('.business').hide();
            $('.training').hide();
            $('.supplyChain').show();
        });
    </script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views//home/ourServices.blade.php ENDPATH**/ ?>