

<?php $__env->startSection('content'); ?>
    <link rel="stylesheet" href="/css/home/home.css"/>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        
    </head>
    <br>
    <br>


    <!--Desktop Display-->
    <div class="row" id="homeDesktop">
        <div class="row center" id="Tags" style="smargin-bottom: 0px!important;">
            <div class="col s2 About" id="" style="cursor:pointer;border-right: 1px solid white;">
                <h4 class="section align-content-center " id="AboutTxt"><b>About us</b></h4>
            </div>
            <div class="col s2 Team" id="" style="cursor:pointer;border-right: 1px solid white;">
                <h4 class="section align-content-center" id="TeamTxt"><b>Our Team</b></h4>
            </div>
            <div class="col s2 Services" id="" style="cursor:pointer;border-right: 1px solid white;">
                <h4 class="section align-content-center " id="ServicesTxt"><b>What we do</b></h4>
            </div>
            <div class="col s2 metho" id="" style="cursor:pointer;border-right: 1px solid white;">
                <h4 class="section align-content-center " id="methoTxt"><b>How we do it</b></h4>
            </div>
            <div class="col s2 library" id="" style="cursor:pointer;border-right: 1px solid white;">
                <h4 class="section align-content-center " id="libraryTxt"><b>Library</b></h4>
            </div>
            <div class="col s2 contact" id="" style="cursor:pointer;border-right: 1px solid white;">
                <h4 class="section align-content-center " id="contactTxt"><b>Contact us</b></h4>
            </div>
        </div>

        <div class="section center-align" style="margin-top: 0;">
            <div class="row">
                <div class="row" style="margin-right: 2em;margin-left: 2em;">
                    <h6 style="color: #9d5821;font-size: 2em;"><b>Our Origin</b></h6>
                    <p>Engeli,&nbsp;a name derived from the word <em>Engele</em> (German for Angel), is the name given
                        to a range of mountains in the Eastern Cape. It was this range of mountains looked upon by
                        Oliver Reginald Tambo during his formative years and the remaining years of his life following
                        his exile.</p>
                    <p>"Looking out from my home, the site of it commanded a wide view of the terrain as it swept from
                        the vicinity of my home and stretched away as far as the eye could see – the panorama bordered
                        on a high range of mountains that we could see from our home. The Engeli Mountains were a huge
                        wall that rolls in the distance to mark the end of a very broken landscape, landscape of great
                        variety and, looking back now, I would say of great beauty? But the nagging question was, what
                        lay beyond the Engeli Mountains? Just exactly what was there? How far
                        would one be able to walk over the mountains to Egoli, Johannesburg? What sort of world would it
                        be? What did it conceal from my view?" </p>
                    <p>I saw two worlds. The one in the vicinity of my home? This was my world. I understood it from my
                        mother’s rondavel? I was part of this world. There was obviously another one beyond the Engeli
                        Mountains.”<br> <strong>OLIVER TAMBO, BEYOND THE ENGELI MOUNTAINS by Lulu Callinicos </strong>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!--Mobile Display-->
    <div class="row" id="homeMobile">
        <div class="container">
            <div class="carousel" id="myCarousel">
                <a class="carousel-item" style="width: 100%;height:100%" href="/home/aboutpage">
                    <div class="about" alt="First Slide"></div>
                    <div class="txt" id="aboutTxt"><b>About Us</b></div>
                </a>
                <a class="carousel-item" style="width:100%;" href="/home/engeliTeam">
                    <div class="team" alt="Second Slide"></div>
                    <text class="txt" id="teamTxt"><b>Our Team</b></text>
                </a>
                <a class="carousel-item" style="width:100%;" href="/home/ourMethodology">
                    <div class="methodology" alt="Third Slide"></div>
                    <text class="txt" id="methodologyTxt"><b>Our Methodology</b></text>
                </a>
                <a class="carousel-item" style="width:100%;" href="/home/ourServices">
                    <div class="services" alt="Fourth Slide"></div>
                    <text class="txt" id="servicesTxt"><b>Our Services</b></text>
                </a>
                <a class="carousel-item" style="width:100%;" href="/home/documents">
                    <div class="doc" alt="Fifth Slide"></div>
                    <text class="txt" id="documentsTxt"><b>Library</b></text>
                </a>
                <a class="carousel-item" style="width:100%;" href="/home/contactUs">
                    <div class="contact" alt="Sixth Slide"></div>
                    <text class="txt" id="contactTxt"><b>Get in touch</b></text>
                </a>
            </div>
        </div>

        <div class="section center-align" style="margin-right: 2em;margin-left: 2em;">
            <div class="row ">
                <div class="row col s12">
                    <h6 style="color: #9d5821;font-size: 2em;"><b>Our Origin</b></h6>
                    <p>Engeli,&nbsp;a name derived from the word <em>Engele</em> (German for Angel), is the name given
                        to a range of mountains in the Eastern Cape. It was this range of mountains looked upon by
                        Oliver Reginald Tambo during his formative years and the remaining years of his life following
                        his exile. </p>
                    <p>Looking out from my home, the site of it commanded a wide view of the terrain as it swept from
                        the vicinity of my home and stretched away as far as the eye could see – the panorama bordered
                        on a high range of mountains that we could see from our home. The Engeli Mountains were a huge
                        wall that rolls in the distance to mark the end of a very broken landscape, landscape of great
                        variety and, looking back now, I would say of great beauty? But the nagging question was, what
                        lay beyond the Engeli Mountains? Just exactly what was there? How far
                        would one be able to walk over the mountains to Egoli, Johannesburg? What sort of world would it
                        be? What did it conceal from my view? </p>
                    <p>I saw two worlds. The one in the vicinity of my home? This was my world. I understood it from my
                        mother’s rondavel? I was part of this world. There was obviously another one beyond the Engeli
                        Mountains.”<br> <strong>OLIVER TAMBO, BEYOND THE ENGELI MOUNTAINS by Lulu Callinicos </strong>
                    </p>
                </div>
            </div>
        </div>
    </div>


    

    <script>
        $(document).ready(function () {
            $('.About').on('click', function () {
                location.href = "/home/aboutpage"
            });
            $('.Team').on('click', function () {
                location.href = "/home/engeliTeam"
            });
            $('.metho').on('click', function () {
                location.href = "/home/ourMethodology"
            });
            $('.Services').on('click', function () {
                location.href = "/home/ourServices"
            });
            $('.library').on('click', function () {
                location.href = "/home/documents"
            });
            $('.contact').on('click', function () {
                location.href = "/home/contactUs"
            });
        });

    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views/welcome.blade.php ENDPATH**/ ?>