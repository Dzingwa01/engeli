<?php $__env->startSection('content'); ?>
    <br />
    <br />
    <br />
    <h6 style="color: #9d5821;padding-left: 600px;font-size: 2em;">Engeli Team</h6>
    <br />

    <div class="row" style="margin-right: 2em;margin-left: 2em;">
        <div class="col s12 m4">
            <div class="card horizontal hoverable">
                <div class="card-image">
                    <img style="width: 110%;height:100%;" src="/images/staff/ricardo.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p style="color: saddlebrown">Dr Ricardo Dames</p>
                        <p>BSc. and MBA; Doctorate in Business Incubation, 18 years experience
                            in SME development. Managed COMSEC(Incubator),ILO Master Trainer on SIYB Program & Social Entrepreneurship.</p>
                   <br />
                    <br />
                        <br />
                        <br />

                    </div>
                </div>
            </div>
        </div>


        <div class="col s12 m4">
            <div class="card horizontal hoverable">
                <div class="card-image">
                    <img   style="width: 100%;height:100%;" src="/images/staff/wayne.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p style="color: saddlebrown">Wayne Oosthuizen</p>
                        <p>Msc (ITFM-Warwick University),MDip Tech (Elec Eng) 21 years Enterprise Development experience including the
                            National Manufacturing Advisory Centre (NAMAC). Last 8 years involved in the design and management of technology/innovation business incubators.</p>
                    <br />
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12 m4">
            <div class="card horizontal hoverable">
                <div class="card-image">
                    <img src="/images/staff/barry.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p style="color: saddlebrown">Barry Wiseman</p>
                        <p>B.Com, LIAB :27 years Corporate Banking (FNB) ,8 years self employed where the assisted business owners with
                            capital raising & business development services.</p>
                        <br />
                        <br />
                        <br />
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="row" style="margin-left: 2em;margin-right: 2em;">
        <div class="col s12 m4">
            <div class="card horizontal hoverable">
                <div class="card-image">
                    <img   style="width: 100%;height:100%;" src="/images/staff/alfred.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p style="color: saddlebrown">Alfred da Costa</p>
                        <p>Serial entrepreneur, Ex CEO and subsequently appointed president of the Port Elizabeth Regional Chamber of Commerce
                            and Industry. Served on a number of JSE listed entities such as Bidvest. CVurrently serves as an independent
                            non-executive director on numerous boards.</p>
                        <br />
                        <br />
                        <br />
                    </div>
                </div>
            </div>
        </div>


        <div class="col s12 m4">
            <div class="card horizontal hoverable">
                <div class="card-image">
                    <img style="width: 110%;height:100%;" src="/images/staff/aadil.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p style="color: saddlebrown">Aadil Kahaar</p>
                        <p>B.Com Acc (NMU) , PG Management Accounting (CIMA). Experienced as analyst of SANAS accredited B-BBEE Verification
                            agency ,later started his own consultancy and now assists with cost effective B-BBEE solutions. He also serves
                            on various boards.</p>
                        <br />
                        <br />
                        <br />
                        <br />
                    </div>
                </div>
            </div>
        </div>


        <div class="col s12 m4">
            <div class="card horizontal hoverable">
                <div class="card-image">
                    <img style="width: 110%;height:100%;" src="/images/staff/trevor.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p style="color: saddlebrown">Trevor Hayter</p>
                        <p>B Com ,FIFM management,CAIB : Banking ,Financial Markets,Risk Management . Trevor has a backgroung in Banking ,
                            International trade and Financial Services. Serial entrepreneur for past 25 years and founding members of various
                            companies including listed company IQUAD ,in partneship with the  PSG Group.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row" style="margin-left: 2em;margin-right: 2em;">
        <div class="col s12 m4">
            <div class="card horizontal hoverable">
                <div class="card-image">
                    <img style="width: 110%;height:100%;" src="/images/staff/kenneth.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p style="color: saddlebrown">Kenneth Jooste</p>
                        <p>B.Com, LLB : Previous experience includes Corporate Governance,Compliances and Legal, HR and Financial Management.</p>
                        <br />
                        <p>&nbsp;</p>
                    </div>
                </div>
            </div>
        </div>


        <div class="col s12 m4">
            <div class="card horizontal hoverable">
                <div class="card-image">
                    <img style="width: 150%;height:100%;" src="/images/staff/lutho.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p style="color: saddlebrown">Lutho Gwili</p>
                        <p>Dip in Banking, PG Dii (Ecom) Mcom(Ecom) ,B-BBEE Consultant. Experiences as SME Advisor & Business Skills Trainer</p>
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                        <br />
                    </div>
                </div>
            </div>
            <br />
            <br />
        </div>

        <div class="col s12 m4">
            <div class="card horizontal hoverable">
                <div class="card-image">
                    <img src="/images/staff/wilfred.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p style="color: saddlebrown">Wilfred Nyiki</p>
                        <p>Pg Dip in Small Enterprise Consulting, majoring in Co-op Development : 20 years experience in SME Development Construction SMME specialist.</p>
                        <br />
                </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-left: 2em;margin-right: 2em;">
        <div class="col s12 m4">
            <div class="card horizontal hoverable">
                <div class="card-image">
                    <img style="height:347px" src="/images/staff/lemo.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p style="color: saddlebrown">Lemogang Gaotshetse</p>
                        <p>B.Com , SAIFM : Previous experience includes Corporate Banking,Corporate Foreign Exchange Risk Management and Investment Management.</p>
                        <br />
                        <br />
                        <br />
                        <br />

                    </div>
                </div>

            </div>
        </div>


        <div class="col s12 m4">
            <div class="card horizontal hoverable">
                <div class="card-image">
                    <img src="/images/staff/chumani.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p style="color: saddlebrown">Chumani Maqina</p>
                        <p>BMus, MBA : Experienced Management Professional within SME and Business Development enviroments. Also successfully
                            managed own procurement solutions business.</p>
                        <br />
                        <BR />
                    </div>
                </div>
            </div>
        </div>


        <div class="col s12 m4">
            <div class="card horizontal hoverable">
                <div class="card-image">
                    <img src="/images/staff/bronwynne.jpg">
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p style="color: saddlebrown">Bronwynne Boswell</p>
                        <p>B Com :12 years experience in Business Skills Development and B-BBEE consulting. Expertise incude development of
                            skills and compliance from B-BBEE perspective.She uses strategic solutions to ensure that companies achieve
                            optimal scores for all elements of the B-BBEE..</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>