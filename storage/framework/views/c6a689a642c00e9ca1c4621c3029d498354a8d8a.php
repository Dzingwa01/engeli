<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>


    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">



    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>


</head>
<body>

<div id="app">
    <div class="navbar-fixed">
        <nav class=""style="background: saddlebrown;height: 120px;">
            <div class="nav-wrapper">
                <a  href="/" class="brand-logo">
                    <img style="width:270px;height: 120px;" src="/images/home/engeliLog.png" class="engeliLogo"/>
                </a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                    <li><a class="nav-tab" href="/"><i class="material-icons left"></i>Home</a></li>
                    <li><a class="nav-tab" href="/home/aboutpage"><i class="material-icons left"></i>About</a></li>
                    <li><a class="nav-tab" href="/home/engeliTeam"><i class="material-icons left"></i>Team</a></li>
                    <li><a class="nav-tab" href="/home/ourMethodology"><i class="material-icons left"></i>Our Methodology</a></li>
                    <li><a class="nav-tab" href="/home/ourServices"><i class="material-icons left"></i>Services</a></li>
                    <li><a class="nav-tab" href="/home/documents"><i class="material-icons left"></i>Documents</a></li>
                    <li><a class="nav-tab" href="/home/contactUs"><i class="material-icons left"></i>Contact</a></li>
                </ul>
            </div>
            <div class="row grey darken-2 " style="height: 6px;">
            </div>
        </nav>
    </div>
    <ul class="sidenav" id="mobile-demo">
        <li><a class="nav-tab" href="/"><i class="material-icons left">business</i>Home</a></li>
        <li><a class="nav-tab" href="/home/aboutpage"><i class="material-icons left">lightbulb_outline</i>About</a></li>
        <li><a class="nav-tab" href="/home/engeliTeam"><i class="material-icons left">people</i>Team</a></li>
        <li><a class="nav-tab" href="/home/ourMethodology"><i class="material-icons left">work</i>Our Methodology</a></li>
        <li><a class="nav-tab" href="/home/ourServices"><i class="material-icons left">more</i>Services</a></li>
        <li><a class="nav-tab" href="/home/documents"><i class="material-icons left">attachments</i>Documents</a></li>
        <li><a class="nav-tab" href="/home/contactUs"><i class="material-icons left">local_phone</i>Contact</a></li>
    </ul>

    <div class="container-fluid">
        <?php echo $__env->yieldContent('content'); ?>
    </div>

    <footer class="page-footer "style="background: saddlebrown">
        <div class="container-fluid">
            <div class="row" style="margin-left: 400px;">
                <td  id="footer">PO Box 21406, Port Elizabeth, 6000  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Telephone : 0861 364 354 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;fax : 0861 55 55 33&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email : info@engeli.co.za</td>

            </div>
        </div>
        <br />
        <br />
    </footer>
</div>
<style>



    .nav-tab {
        font-weight: bolder !important;

    }
    @media  screen and (max-width: 767px) {
        nav a{
            color:black!important;
        }
        .propella-logo{
            height: 57px!important;

        }
    }


</style>




<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>
    $(document).ready(function () {
//        M.AutoInit();
        $('.parallax').parallax();
        $('.sidenav').sidenav();
        $('.dropdown-trigger-cus').dropdown();
        $('.dropdown-trigger-cus-3').dropdown();
        $('.dropdown-trigger-c').dropdown();
        $('select').formSelect();
        $('.carousel').carousel({
            fullWidth: true,
            indicators: true
        });


    });
</script>
<?php echo $__env->yieldPushContent('custom-scripts'); ?>
</body>
</html>
