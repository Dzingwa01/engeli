

<?php $__env->startSection('content'); ?>
    <br>
    <br>
    <br>
    <div class="card"style="width:800px;margin: 0 auto">
        <br>
        <div class="row">
            <div class="col s4">
                <img style="width:150px;height: 100px;margin-left: 630px;" src="/images/home/logoo.png" class="engeliLogo"/>
            </div>
            <div class="col s4">
                <h6 style="margin-left: 10px;font-size: 2em;color: #9d5821"><strong>Enquiry</strong></h6>
            </div>
        </div>
        <div class="row"style="margin-left: 2em;margin-right: 4em;">
            <div class="row">
                <input id="enquiry_id" disabled hidden value="<?php echo e($enquiry->id); ?>">
                <div class="col s4">
                    <h6><?php echo e($enquiry->name); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo e($enquiry->surname); ?></h6>
                </div>
            </div>
            <div class="row">
                <div class="col s4">
                    <h6>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php echo e($enquiry->email); ?></h6>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <h6>Cell number &nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;: <?php echo e($enquiry->cell_number); ?></h6>
                </div>
            </div>
            <div class="row">
                <div class="col s4">
                    <h6>Message &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </h6>
                </div>
                <div class="card col s4" style="width: 470px;height: 170px;font-family: Arial">
                    <br>
                    <p style="margin-left: 1em;margin-right: 1em;"><?php echo e($enquiry->message); ?></p>
                </div>
            </div>
            <div class="input-field col m6"style="width: 500px">
                <select id="status">
                    <option value="">Select Status</option>
                    <option value="New">New</option>
                    <option value="Awaiting Response">Awaiting Response</option>
                    <option value="Resolved">Resolved</option>
                </select>
                <label>Status</label>
                <br />
            </div>
        </div>
        <div class="row" style="text-align: center;margin-left: 450px">
            <a class="waves-effect waves-light btn" id="submit-status">Submit</a>
        </div>
        <br>
    </div>
<br>

    <script>
        $(document).ready(function(){
            $('select').formSelect();

            $('#submit-status').on('click', function () {
                let formData = new FormData();
                formData.append('status', $('#status').val());
                formData.append('enquiry_id', $('#enquiry_id').val());

                let url = "<?php echo e(route('status.store')); ?>";
                $.ajax({
                    url:url,
                    data: formData,
                    type: 'post',
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        //window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
    </script>

    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views//home/enquiry-show.blade.php ENDPATH**/ ?>