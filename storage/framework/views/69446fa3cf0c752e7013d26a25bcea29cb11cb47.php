<?php $__env->startSection('content'); ?>
    <link rel="stylesheet" type="text/css" href="/css/contact/contact.css" />
<br />
    <br />
    <br />
    <div class="section center-align">
        <div class="row left-align">
            <div class="col s4" id="">
                <div class="standard-left-buttons homepage">
                    <h6 class="standard-left-button-headers ">Home</h6>
                </div>
                <div class="standard-left-buttons abt">
                    <h6 class="standard-left-button-headers ">About Us</h6>
                </div>
                <div class="standard-left-buttons ourpeople">
                    <h6 class="standard-left-button-headers ">Engeli Team</h6>
                </div>
                <div class="standard-left-buttons ourMeth">
                    <h6 class="standard-left-button-headers ">Our Methodology</h6>
                </div>
                <div class="standard-left-buttons serv">
                    <h6 class="standard-left-button-headers ">Services</h6>
                </div>
                <div class="standard-left-buttons docu">
                    <h6 class="standard-left-button-headers ">Documents</h6>
                </div>
                <div class="standard-left-buttons cntUS">
                    <h6 class="standard-left-button-headers ">Contact Us</h6>
                </div>
            </div>

            <div class="col s1"></div>
            <div class="col s6">
                <h6 style="color: #9d5821;font-size: 2em;">Contact Us:&nbsp;</h6>
                <div id="HTMLBlock410" class="HTMLBlock">
                    <iframe style="width:700px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3308.908521344357!2d25.62724661521281!3d-33.969190680628586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1e7ad2904dd018ff%3A0xdbe8085bd20c11dd!2s1+Humewood+Rd%2C+South+End%2C+Port+Elizabeth%2C+6001!5e0!3m2!1sen!2sza!4v1502865449848" width="340" height="300" frameborder="0" style="border:0" allowfullscreen="">
                    </iframe>
                </div>
                <div class="">
                    <div class="section">
                        <div class="layoutArea">
                            <div class="column">
                                <p>Ground Floor, Baysuites<br>1a Humewood Road,<br>South End<br>Port Elizabeth&nbsp;</p>
                                <p><strong>Tel: &nbsp; &nbsp; </strong><span>0861 ENGELI or 0861 364 354<br></span><strong>Fax: &nbsp; &nbsp;</strong>0861 55 55 33<br><strong>Email:&nbsp;<a href="mailto:info@engeli.co.za">info@engeli.co.za</a></strong>&nbsp;</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.homepage').on('click', function () {
                location.href = "/"
            });
            $('.ourpeople').on('click', function () {
                location.href = "/home/engeliTeam"
            });
            $('.abt').on('click', function () {
                location.href = "/home/aboutpage"
            });
            $('.ourMeth').on('click', function () {
                location.href = "/home/ourMethodology"
            });
            $('.serv').on('click', function () {
                location.href = "/home/ourServices"
            });
            $('.docu').on('click', function () {
                location.href = "/home/documents"
            });
            $('.cntUS').on('click', function () {
                location.href = "/home/contactUs"
            });
        });

    </script>



    </div><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p></div>
   <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>