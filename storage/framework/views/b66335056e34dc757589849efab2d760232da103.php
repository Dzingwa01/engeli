

<?php $__env->startSection('content'); ?>
    <link rel="stylesheet" type="text/css" href="/css/about/about.css" />
    <!--Top image Desktop-->
    <div class="section center-align topImageDesktop ">
        <div class="row">
            <img src="/images/TagsImages/Engele strip.png" id="TopAboutImage">
        </div>
    </div>

    <!--Desktop-->
    <div class="section center-align" id="aboutDesktop" style="margin-left: 5em;margin-right: 2em;">
        <div class="row left-align">
            <div class="col s3" id="">
                <div class="standard-left-buttons homepage">
                    <h6 class="standard-left-button-headers ">Home</h6>
                </div>
                <div class="standard-left-buttons ourpeople">
                    <h6 class="standard-left-button-headers ">Engeli Team</h6>
                </div>
                <div class="standard-left-buttons ourMeth">
                    <h6 class="standard-left-button-headers ">How we do it</h6>
                </div>
                <div class="standard-left-buttons serv">
                    <h6 class="standard-left-button-headers ">What we do</h6>
                </div>
                <div class="standard-left-buttons docu">
                    <h6 class="standard-left-button-headers ">Library</h6>
                </div>
                <div class="standard-left-buttons cntUS">
                    <h6 class="standard-left-button-headers ">Get in touch</h6>
                </div>
            </div>

            <div class="col s1"></div>
            <div class="col s8">
                <div class="row center-align">
                    <h6 style="color: #9d5821;font-size: 2em;"><strong>About Us</strong></h6>
                    <p>Engeli Enterprise Development (Pty) Ltd Engeli was established in 2011 by a group
                            of Enterprise Development Specialists who has
                            extensive experience, both locally and abroad. The team consists of Engineers, Business Support Specialist, Lawyers
                            and Accountants.&nbsp;&nbsp;</p>

                    <p>Engeli is a 51% black and 46% black women owned business focusing on supplier development
                            (transformation and localisation), business incubation and skills development.&nbsp;</p>

                    <p>Engeli has also established an Employee Empowerment Trust for the benefit of their black owned women staff
                            which has a direct interest in Engeli Enterprise Development (Pty) Ltd.&nbsp;</p>

                    <p>Engeli is a verified Black Fund Manager trading under FSCA (Financial Sector Conduct Authority) licence number 48139.&nbsp;</p>


                    <h6 style="color: #9d5821;font-size: 2em;"><strong>Our purpose :</strong></h6>
                    <p>We believe that equipping entrepreneurs and enterprises with our innovative business solutions will contribute&nbsp;
                        towards robust economic development. We focus on addressing the primary needs of Small Enterprises, with the core thrust of Engeli’s
                        efforts being aligned to addressing three fundamental issues and deficits that plague Small Enterprises (SE’s). These are:&nbsp;</p>
                    <ul>
                        <li><b>• Access to Markets&nbsp;</b></li>
                        <li><b>• Access to Finance&nbsp;</b></li>
                        <li><b>• Access to high level business support and mentorship&nbsp;</b></li>
                    </ul>


                    <p>The Engeli team has been working within the business development environment for many years now and are appropriately skilled and experienced in
                        both the technological and business aspects of enterprise development and support to provide the required technical
                        assistance to small businesses.&nbsp;</p>
                    <p>Engeli are of the opinion that a synergistic approach is necessary to grow and sustain Small Enterprises, in support of wealth and job creation.</p>
                </div>
            </div>
        </div>
    </div>

    <!--Top image Mobile-->
    <div class="section center-align topImageMobile">
        <div class="row">
            <div class="">
                <div class="">
                    <img src="/images/home/Engele strip.png"style="width:380px;height: 400px">
                </div>
            </div>
        </div>
    </div>

    <!--Mobile-->
    <div class="section" id="aboutMobile" style="margin-left: 2em;margin-right: 2em;">
        <div class="row">
            <div class="col s12">
                <div class="row">
                    <h6 style="color: #9d5821;font-size: 2em;"><strong>About Us</strong></h6>

                    <p>Engeli Enterprise Development(Pty) Ltd Engeli was established in 2011 by a group
                            of Enterprise Development Specialists who has
                            extensive experience, both locally and abroad. The team consists of Engineers, Business Support Specialist, Lawyers
                            and Accountants.&nbsp;</p>

                    <p>Engeli is a 51% black owned and 46% black women owned business focusing on supplier development
                            (transformation and localisation), business incubation and skills development.&nbsp;</p>

                    <p>Engeli has also established an Employee Empowerment Trust for the benefit of their black owned women staff
                            which has a direct interest in Engeli Enterprise Development (Pty) Ltd.&nbsp;</p>

                    <p>Engeli is a verified Black Fund Manager trading under FSCA (Financial Sector Conduct Authority) licence number 48139.&nbsp;</p>


                    <h6 style="color: #9d5821;font-size: 2em;"><strong>Our purpose :</strong></h6>
                    <p>We believe that equipping entrepreneurs and enterprises with our innovative business solutions will contribute&nbsp;
                        towards robust economic development. We focus on addressing the primary needs of Small Enterprises, with the core thrust of Engeli’s
                        efforts being aligned to addressing three fundamental issues and deficits that plague Small Enterprises (SE’s). These are;&nbsp;</p>
                    <ul>
                        <li>• Access to Markets&nbsp;</li>
                        <li>• Access to Finance&nbsp;</li>
                        <li>• Access to high level business support and mentorship&nbsp;</li>
                    </ul>


                    <p>The Engeli team has been working within the business development environment for many years now and are appropriately skilled and experienced in
                        both the technological and business aspects of enterprise development and support to provide the required technical
                        assistance to small businesses.&nbsp;</p>
                    <p>Engeli are of the opinion that a synergistic approach is necessary to grow and sustain Small Enterprises, in support of wealth and job creation.</p>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready(function () {
            $('.homepage').on('click', function () {
                location.href = "/"
            });
            $('.ourpeople').on('click', function () {
                location.href = "/home/engeliTeam"
            });
            $('.ourMeth').on('click', function () {
                location.href = "/home/ourMethodology"
            });
            $('.serv').on('click', function () {
                location.href = "/home/ourServices"
            });
            $('.docu').on('click', function () {
                location.href = "/home/documents"
            });
            $('.cntUS').on('click', function () {
                location.href = "/home/contactUs"
            });
        });

    </script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views//home/aboutpage.blade.php ENDPATH**/ ?>