<?php $__env->startSection('content'); ?>
    <link rel="stylesheet" href="/css/home/home.css"/>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        
    </head>
    <!--Desktop Display-->
    <div class="row" id="homeDesktop">
        <div class="row" id="Tags" style="margin-bottom: 0px!important;">
            <a class="col s2 about" href="/home/aboutpage" style="cursor:pointer">
                <div class="txt" id="aboutTxt"><b>About Us</b></div>
            </a>
            <div class="col s2 team" id="" style="cursor:pointer">
                <text class="txt" id="teamTxt"><b>Our Team</b></text>
            </div>
            <div class="col s2 methodology" id="" style="cursor:pointer">
                <text class="txt" id="methodologyTxt"><b>Our Methodology</b></text>
            </div>
            <div class="col s2 services" id="" style="cursor:pointer">
                <text class="txt" id="servicesTxt"><b>Our Services</b></text>

            </div>
            <div class="col s2 doc" id="" style="cursor:pointer">
                <text class="txt" id="documentsTxt"><b>Documents</b></text>
            </div>
            <div class="col s2 contact" id="" style="cursor:pointer">
                <text class="txt"id="contactTxt"><b>Contact us</b></text>
            </div>
        </div>


        <div class="section center-align">
            <div class="row " >
                <div class="col s4"style="margin-left:2em;margin-right: 2em; ">
                    <p><img title="Evaluate Enhance Advance" src="http://engeli.co.za/files/3614/4126/1575/evaluate_enhance_advance.jpg" alt="Evaluate Enhance Advance" width="690" height="420"> </p>

                  </div>
                <div class="col s1"></div>
                <div class="row col s4"style="margin-left: 400px;">
                    <h6 style="color: #9d5821;font-size: 2em;">OUR ORIGIN</h6>
                    <p ><strong>Engeli,&nbsp;a name derived from the word <em>Engele</em> (German for Angel), is the name given to a range of mountains in the Eastern Cape. It was this range of mountains looked upon by Oliver Reginald Tambo during his formative years and the remaining years of his life following his exile. </strong></p>
                    <p>Looking out from my home, the site of it commanded a wide view of the terrain as it swept from the vicinity of my home and stretched away as far as the eye could see – the panorama bordered on a high range of mountains that we could see from our home. The Engeli Mountains were a huge wall that rolls in the distance to mark the end of a very broken landscape, landscape of great variety and, looking back now, I would say of great beauty? But the nagging question was, what lay beyond the Engeli Mountains? Just exactly what was there? How far
                        would one be able to walk over the mountains to Egoli, Johannesburg? What sort of world would it be? What did it conceal from my view? </p>
                    <p>I saw two worlds. The one in the vicinity of my home? This was my world. I understood it from my mother’s rondavel? I was part of this world. There was obviously another one beyond the Engeli Mountains.”<br> <strong>OLIVER TAMBO, BEYOND THE ENGELI MOUNTAINS by Lulu Callinicos </strong></p>
                </div>
            </div>
        </div>
    </div>



    <script>
        $(document).ready(function () {
            $('.about').on('click', function () {
                location.href = "/home/aboutpage"
            });
            $('.team').on('click', function () {
                location.href = "/home/engeliTeam"
            });
            $('.methodology').on('click', function () {
                location.href = "/home/ourMethodology"
            });
            $('.services').on('click', function () {
                location.href = "/home/ourServices"
            });
            $('.doc').on('click', function () {
                location.href = "/home/documents"
            });
            $('.contact').on('click', function () {
                location.href = "/home/contactUs"
            });
        });

    </script>
 <?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>