

<?php $__env->startSection('content'); ?>
    <br>
    <br>
    <br>
    <br>

    <div class="row">
        <input id="id" value="<?php echo e($library->id); ?>" type="text" class="validate" hidden>

        <div class=""style="margin-left: 2em;margin-right: 2em;">
            <p style="font-size: 2em;"><?php echo e($library->title); ?></p>
            <p><?php echo e($library->description); ?></p>
            <div class="row" style="margin-left:2em;margin-right: 2em;">
                <?php if(isset($library->images)): ?>
                    <?php $__currentLoopData = $library->images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="col 12 ">
                            <div class="card-image">
                                <img src="<?php echo e('/storage/'.$upload->image_url); ?>" style="width:100px; height:100px;">
                            </div>
                            <div class="card-content">
                                <p><?php echo e($library->description); ?></p>
                            </div>

                            <div class="card-action">
                                <a href="<?php echo e('/storage/'.$upload->image_url); ?>" download>Download Image</a>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </div>
            <div class="row" style="margin-left: 10px">
                <div class="col s4">
                    <?php if(isset($library->pdfs)): ?>
                        <?php $__currentLoopData = $library->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <a href="<?php echo e($upload->pdf_url); ?>" download>Download PDF</a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col s4">
                    <?php if(isset($library->videos)): ?>
                        <?php $__currentLoopData = $library->videos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <iframe width="150" height="150" src="<?php echo e($upload->video_url); ?>" frameborder="0" allowfullscreen></iframe>
                            <a href="<?php echo e($upload->video_url); ?>" target="_blank">Watch this video</a>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                </div>
            </div>
            <br>
        </div>
            
                
                
                
                
            


        </div>
    </div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views//home/show-uploaded-libraries.blade.php ENDPATH**/ ?>