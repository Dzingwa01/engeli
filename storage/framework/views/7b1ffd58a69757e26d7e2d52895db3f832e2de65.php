

<?php $__env->startSection('content'); ?>
    <link rel="stylesheet" type="text/css" href="/css/methodology/methodology.css" />
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <!--Desktop-->
    <div class="section center-align" id="ourMethoDesktop">
        <div class="row left-align"style="margin-left: 5em;">
            <div class="col s3" id="">
                <div class="standard-left-buttons homepage">
                    <h6 class="standard-left-button-headers ">Home</h6>
                </div>
                <div class="standard-left-buttons abt">
                    <h6 class="standard-left-button-headers ">About Us</h6>
                </div>
                <div class="standard-left-buttons ourpeople">
                    <h6 class="standard-left-button-headers ">Engeli Team</h6>
                </div>
                <div class="standard-left-buttons ourMeth">
                    <h6 class="standard-left-button-headers ">How we do it</h6>
                </div>
                <div class="standard-left-buttons serv">
                    <h6 class="standard-left-button-headers ">What we do</h6>
                </div>
                <div class="standard-left-buttons docu">
                    <h6 class="standard-left-button-headers ">Library</h6>
                </div>
                <div class="standard-left-buttons cntUS">
                    <h6 class="standard-left-button-headers ">Get in touch</h6>
                </div>
            </div>

            <div class="col s1"></div>
            <div class="col s8 center-align">
                <h6 style="font-size: 2em;color: #9d5821;"><strong>How we do it</strong></h6>
                <p style="margin-right: 1em;">Engeli signifies the quest to go beyond that which seems impenetrable; to create a business that knows no barriers in the pursuit of excellence for the success of all stakeholders.</p>
                <div class="row">
                    <p>&nbsp;<img class="materialboxed" title="Our Methodology" src="/images/MethodologyEdited.png" width="680" height="643"></p>
                </div>
            </div>
        </div>
    </div>

    <!--Mobile-->
    <div class="section" id="ourMethoMobile">
        <div class="row"style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <br>
                <h6 style="font-size: 2em;color: #9d5821;"><strong>How we do it</strong></h6>
                <p ><span><strong>Engeli signifies the quest to go beyond that which seems impenetrable; to create a business that knows no barriers in the pursuit of excellence for the success of all stakeholders.</strong></span></p>
                <div class="row">
                    <p>&nbsp;<img class="materialboxed" title="Our Methodology" src="http://engeli.co.za/files/6814/4126/4433/methodology_org.jpg" alt="Our Methodology" width="300" height="300"></p>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.materialboxed').materialbox();

            $('.homepage').on('click', function () {
                location.href = "/"
            });
            $('.ourpeople').on('click', function () {
                location.href = "/home/engeliTeam"
            });
            $('.abt').on('click', function () {
                location.href = "/home/aboutpage"
            });
            $('.ourMeth').on('click', function () {
                location.href = "/home/ourMethodology"
            });
            $('.serv').on('click', function () {
                location.href = "/home/ourServices"
            });
            $('.docu').on('click', function () {
                location.href = "/home/documents"
            });
            $('.cntUS').on('click', function () {
                location.href = "/home/contactUs"
            });
        });

    </script>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views//home/ourMethodology.blade.php ENDPATH**/ ?>