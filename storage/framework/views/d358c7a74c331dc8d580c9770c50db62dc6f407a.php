

<?php $__env->startSection('content'); ?>
    <link rel="stylesheet" type="text/css" href="/css/team/team.css">
    <br/>
    <br/>
    <br>
    <br>



    <style>
        .container {
            position: relative;
            width: 20%;
            margin-left: 40px;
        }

        .image {
            opacity: 1;
            display: block;
            width: 100%;
            height: auto;
            transition: .5s ease;
            backface-visibility: hidden;
        }

        .middle {
            transition: .5s ease;
            opacity: 0;
            position: absolute;
            top: 50%;
            left: 200%;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            text-align: center;
        }

        .container:hover .image {
            opacity: 0.3;
        }

        .container:hover .middle {
            opacity: 1;
        }

        .text {
            backdrop-filter: blur(10px);
            color: black;
            font-size: 1em;
            padding: 16px 32px;
            width: 300px;
        }
    </style>
    </head>
    <body>

    <!--Desktop-->
    <div class="desktopTeam">
        <br>
        <h6 style="color: #9d5821;padding-left: 680px;font-size: 2em;"><strong>Engeli Team</strong></h6>
        <br>
        <div class="row">
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/RicardoEdited.png" alt="Avatar" class="image" style="width:400%;">
                    <div class="middle">
                        <div class="text"><b>Dr Ricardo Dames</b></div>
                        <div class="text">BSc. and MBA; PhD: Business Incubation, 18 years experience
                            in SME development. Managed COMSEC(Incubator), ILO SIYB and Social Enterprise Master Trainer.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/WyneEdited.png" alt="Avatar" class="image" style="width:400%;height: 200%;">
                    <div class="middle">
                        <div class="text"><b>Wayne Oosthuizen</b></div>
                        <div class="text">Msc (ITFM-Warwick University), MDip Tech (Elec Eng) 21 years Enterprise
                            Development experience including the
                            National Manufacturing Advisory Centre (NAMAC). Last 8 years involved in the design and
                            management of technology/innovation business incubators.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/BrownEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Bronwynne Boswell</b></div>
                        <div class="text">B Com: 12 years experience in Business Skills Development and B-BBEE
                            Consulting. Expertise incude development of
                            skills and compliance from B-BBEE perspective. She uses strategic solutions to ensure that
                            companies achieve
                            optimal scores for all elements of the B-BBEE.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/BarryEdited.png" alt="Avatar" class="image" style="width:400%;height: 200%;">
                    <div class="middle">
                        <div class="text"><b>Barry Wiseman</b></div>
                        <div class="text">B.Com, LIAB: 27 years Corporate Banking (FNB), 8 years self employed where he
                            assisted business owners with
                            capital raising & business development services.
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/ReneEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Rene Howlett </b></div>
                        <div class="text">B-BBEE MDP, Member of ABP with recognition as EEP.
                                Experience as an Analyst, Technical Signatory of SANAS Accredited B-BBEE Verification agency. Assists with cost effective B-BBEE solution, SETA Submissions, EE compliance.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/JohnEdited.png" alt="Avatar" class="image" style="width:400%;height: 200%;">
                    <div class="middle">
                        <div class="text"><b>John Rankin</b></div>
                        <div class="text">BPsych (Registered Counsellor): 14 Years’ experience in Transformation and Development work.
                            Individual, Youth, Community and Business advancement experience through bespoke solutions. Previously worked at a
                            SANAS accredited B-BBEE Verification Agency and as a B-BBEE Consultant. Currently fills role of Business Development
                            Manager and B-BBEE Specialist.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/FarradEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Farhaad Sain</b></div>
                        <div class="text">B.Com Business Management and Economics (NMU), B.Com Economics (Hons) (NMU). Work experience abroad at a Fortune 500 Global Financial Institution.
                            Former years’ experience in local operation retail banking.
                            Experienced as an analyst at a SANAS accredited B-BBEE Verification Agency.
                        </div>
                    </div>
                </div>
            </div>

            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/NontuthuzeloEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Nontuthuzelo "Pinky" Samela</b></div>
                        <div class="text">Started her career at M Secure cleaning services where she completed various training that specialises in
                            cleaning and Hygiene.  Pinky showed great potential and Engeli offered her a position as the Receptionist.  Pinky currently
                            assists a few of the group companies and manages the reception area and its associated tasks.
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/KennethEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Kenneth Jooste</b></div>
                        <div class="text">B.Com, LLB: Previous experience includes Corporate Governance, Compliances and
                            Legal, HR and Financial Management.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/TrevorEdited.png" alt="Avatar" class="image" style="width:400%;height: 200%;">
                    <div class="middle">
                        <div class="text"><b>Trevor Hayter</b></div>
                        <div class="text">B Com, FIFM, CAIB : Banking, Financial Markets, Risk Management. Trevor has a
                            background in Banking, International trade and Financial
                            Services. Entrepreneur for past 25 years and founding member of various companies including
                            listed company IQUAD, in partnership with the PSG Group.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/Sivu2.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Sivuyisiwe Msebi</b></div>
                        <div class="text">Completed the Certificate in the Theory of Accounting (CTA) in 2012 and went onto KPMG to complete her learnership as a trainee accountant.
                            During her learnership at KPMG, she passed both SAICA qualifying examinations and registered with SAICA as a chartered accountant.
                            Prior to joining Engeli Enterprise Development, she worked at PwC as a manager in the Service Delivery Centre."

                        </div>
                    </div>
                </div>
            </div>

            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/LuthoEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Lutho Gwili</b></div>
                        <div class="text">Dip (Banking), PGDip (Applied Economics), MCom (Economics). B-BBEE Consultant. Experienced as an SME Advisor and Business Skills Trainer
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/ChumaniEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Chumani Magina</b></div>
                        <div class="text">BMus, MBA : Experienced Management Professional within SME and Business
                            Development enviroments. Also successfully
                            managed own procurement solutions business.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/AaadilEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Aadil Kahaar</b></div>
                        <div class="text">B.Com Acc (NMU) , PG Management Accounting (CIMA). Experienced as analyst of
                            SANAS accredited B-BBEE Verification
                            agency ,later started his own consultancy and now assists with cost effective B-BBEE
                            solutions. He also serves
                            on various boards.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/LiezelEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Liezel Hayter</b></div>
                        <div class="text">Liezel spent 3 years with a wealth management company and 4 years with a boutique financial services
                            company, as personal assistant to the respective CEOs. Liezel then joined Engeli as an Executive Assistant,
                            but she also plays a leading role in the administration of the Supplier and
                            Enterprise Development Fund managed by Engeli, as well as assisting the Financial Director with the
                            preparation of the Group’s Financials.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/AlfredEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Alfred da Costa</b></div>
                        <div class="text">Entrepreneur, Ex CEO and appointed president of the Port Elizabeth Regional
                            Chamber of Commerce
                            and Industry. Served on a number of JSE listed entities such as Bidvest. Cuurrently serves
                            as an independent
                            non-executive director on numerous boards.
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <div class="row">
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/LemoEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Lemogang Gaotshetse</b></div>
                        <div class="text">B.Com , SAIFM : Previous experience includes Corporate Banking,Corporate
                            Foreign Exchange Risk Management and Investment Management.
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <!--Mobile-->
    <div class="mobileTeam">
        <br>
        <h2 style="font-size: 2em;color: #9d5821;margin-left: 110px"><strong>Engeli Team</strong></h2>
        <p style="margin-left: 70px;font-style: italic;">Click on the image to view more info.</p>
        <div class="row">
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/RicardoEdited.png" alt="Avatar" class="image" style="width:470%;">
                    <div class="middle">
                        <div class="text"><b>Dr Ricardo Dames</b></div>
                        <div class="text">BSc. and MBA; PhD: Business Incubation , 18 years experience
                            in SME development. Managed COMSEC(Incubator),ILO SIYB and Social Enterprise Master Trainer.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/WyneEdited.png" alt="Avatar" class="image" style="width:400%;height: 200%;">
                    <div class="middle">
                        <div class="text"><b>Wayne Oosthuizen</b></div>
                        <div class="text">Msc (ITFM-Warwick University),MDip Tech (Elec Eng) 21 years Enterprise
                            Development experience including the
                            National Manufacturing Advisory Centre (NAMAC). Last 8 years involved in the design and
                            management of technology/innovation business incubators.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/BrownEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Bronwynne Boswell</b></div>
                        <div class="text">B Com :12 years experience in Business Skills Development and B-BBEE
                            consulting. Expertise incude development of
                            skills and compliance from B-BBEE perspective.She uses strategic solutions to ensure that
                            companies achieve
                            optimal scores for all elements of the B-BBEE.
                        </div>
                    </div>
                </div>
            </div>

            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/AlfredEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Alfred da Costa</b></div>
                        <div class="text">Entrepreneur, Ex CEO and appointed president of the Port Elizabeth Regional
                            Chamber of Commerce
                            and Industry. Served on a number of JSE listed entities such as Bidvest. CVurrently serves
                            as an independent
                            non-executive director on numerous boards.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/ReneEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Rene Howlett </b></div>
                        <div class="text">B-BBEE MDP,Member of ABP with recognition as EEP.
                            Experience as an Analysis Technical Signatory of SANAS Accredited B-BBEE Verification agency. Assists with cost effective B-BBEE solution, SETA Submissions, EE compliance.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/JohnEdited.png" alt="Avatar" class="image" style="width:400%;height: 200%;">
                    <div class="middle">
                        <div class="text"><b>John Rankin</b></div>
                        <div class="text">BPsych (Registered Counsellor): 14 Years’ experience in Transformation and Development work.
                            Individual, Youth, Community and Business advancement experience through bespoke solutions. Previously worked at a
                            SANAS accredited B-BBEE Verification Agency and as a B-BBEE Consultant. Currently fills role of Business Development
                            Manager and B-BBEE Specialist.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/FarradEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Farhaad Sain</b></div>
                        <div class="text">B.Com Business Management and Economics (NMU), B.Com Economics (Hons) (NMU)
                            Work experience abroad at a Fortune 500 Global Financial Institution.
                            Former years’ experience in local operation retail banking.
                            Experienced as an analyst at a SANAS accredited B-BBEE Verification Agency.
                        </div>
                    </div>
                </div>
            </div>

            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/NontuthuzeloEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Nontuthuzelo "Pinky" Samela</b></div>
                        <div class="text">Started her career at M Secure cleaning services where she completed various training that specialises in
                            leaning and Hygiene.  Pinky showed great potential and Engeli offered her a position as the Receptionist.  Pinky currently
                            assists a few of the group companies and manages the reception area and its associated tasks.
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/KennethEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Kenneth Jooste</b></div>
                        <div class="text">B.Com, LLB : Previous experience includes Corporate Governance,Compliances and
                            Legal, HR and Financial Management.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/TrevorEdited.png" alt="Avatar" class="image" style="width:400%;height: 200%;">
                    <div class="middle">
                        <div class="text"><b>Trevor Hayter</b></div>
                        <div class="text">B Com, FIFM, CAIB : Banking, Financial Markets,Risk Management. Trevor has a
                            background in Banking,International trade and Financial
                            Services. Entrepreneur for past 25 years and founding member of various companies including
                            listed company IQUAD, in partnership with the PSG Group.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/Sivu2.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Sivuyisiwe Msebi</b></div>
                        <div class="text">Completed the Certificate in the Theory of Accounting (CTA) in 2012 and went onto KPMG to complete her learnership as a trainee accountant.
                            During her learnership at KPMG, she passed both SAICA qualifying examinations and registered with SAICA as a chartered accountant.
                            Prior to joining Engeli Enterprise Development, she worked at PwC as a manager in the Service Delivery Centre."

                        </div>
                    </div>
                </div>
            </div>

            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/LuthoEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Lutho Gwili</b></div>
                        <div class="text">Dip (Banking), PGDip (Applied Economics), MCom (Economics). B-BBEE Consultant. Experience as an SME Advisor and Business Skills Trainer
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/BarryEdited.png" alt="Avatar" class="image" style="width:400%;height: 200%;">
                    <div class="middle">
                        <div class="text"><b>Barry Wiseman</b></div>
                        <div class="text">B.Com, LIAB :27 years Corporate Banking (FNB) ,8 years self employed where the
                            assisted business owners with
                            capital raising & business development services.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/ChumaniEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Chumani Magina</b></div>
                        <div class="text">BMus, MBA : Experienced Management Professional within SME and Business
                            Development enviroments. Also successfully
                            managed own procurement solutions business.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/AaadilEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Aadil Kahaar</b></div>
                        <div class="text">B.Com Acc (NMU) , PG Management Accounting (CIMA). Experienced as analyst of
                            SANAS accredited B-BBEE Verification
                            agency ,later started his own consultancy and now assists with cost effective B-BBEE
                            solutions. He also serves
                            on various boards.
                        </div>
                    </div>
                </div>
            </div>
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/LiezelEdited.png" alt="Avatar" class="image" style="width:400%">
                    <div class="middle">
                        <div class="text"><b>Liezel Hayter</b></div>
                        <div class="text">Liezel spent 3 years with a wealth management company and 4 years with a boutique financial services
                            company, as personal assistant to the respective CEOs. Liezel then joined Engeli as an Executive Assistant,
                            but she also plays a leading role in the administration of the Supplier and
                            Enterprise Development Fund managed by Engeli, as well as assisting the Financial Director with the
                            preparation of the Group’s Financials.
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col m3">
                <div class="container">
                    <img src="/images/staff/LemoEdited.png" alt="Avatar" class="image" style="width:470%">
                    <div class="middle">
                        <div class="text"><b>Lemogang Gaotshetse</b></div>
                        <div class="text">B.Com , SAIFM : Previous experience includes Corporate Banking,Corporate
                            Foreign Exchange Risk Management and Investment Management.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views//home/engeliTeam.blade.php ENDPATH**/ ?>