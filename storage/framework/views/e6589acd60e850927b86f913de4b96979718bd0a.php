

<?php $__env->startSection('content'); ?>
    <head>
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    </head>
    <link rel="stylesheet" type="text/css" href="/css/documents/documents.css"/>
    <br>
    <br>
    <br>
    <br>
    <br>

    <div class="section center-align desktopDocuments " style="margin-left: 5em;margin-right: 2em;">
        <div class="row left-align">
            <div class="col s3">
                <div class="standard-left-buttons supplyChain ">
                    <h6 class="standard-left-button-headers ">Supply Chain Development & Localization</h6>
                </div>
                <div class="standard-left-buttons transformation ">
                    <h6 class="standard-left-button-headers ">Transformation Solutions</h6>
                </div>
                <div class="standard-left-buttons businessIncubation ">
                    <h6 class="standard-left-button-headers ">Business Incubation</h6>
                </div>
                <div class="standard-left-buttons loanManagement ">
                    <h6 class="standard-left-button-headers ">Loan Management</h6>
                </div>
                <div class="standard-left-buttons fundManagement ">
                    <h6 class="standard-left-button-headers ">Fund Management</h6>
                </div>
            </div>
            <div class="col s1"></div>

            <div class="col s8 left-align">
                <h6 style="color: #9d5821;font-size: 2em;"><strong>Library</strong></h6>
                <hr class="line1">
                <p>The following documents are available for download:&nbsp;</p>
                <!--Supply Chain Development-->
                <div class="" id="supply">
                    <?php for($i = 0; $i < count($supply_chain_library); $i++): ?>
                        <?php if($i == 0): ?>
                            <h6 style="color: saddlebrown;font-size: 2em">Supply Chain Development & Localization</h6>
                        <?php endif; ?>
                        <div class="">
                            <input value="<?php echo e($supply_chain_library[$i]->id); ?>" disabled hidden class="document-id">
                            <div class=" ">
                                <?php if(isset($supply_chain_library[$i]->excel)): ?>
                                    <?php $__currentLoopData = $supply_chain_library[$i]->excel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p><?php echo e($supply_chain_library[$i]->description); ?></p>
                                        <a href="<?php echo e('/download-file-excel/'.$upload->id); ?>"><?php echo e($supply_chain_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <?php if(isset($supply_chain_library[$i]->pdfs)): ?>
                                    <?php $__currentLoopData = $supply_chain_library[$i]->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p><?php echo e($supply_chain_library[$i]->description); ?></p>
                                            <a href="<?php echo e('/download-file-pdf/'.$upload->id); ?>"><?php echo e($supply_chain_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>

                <!--Transformation Solution-->
                <div class="" id="transformationSolution">
                    <?php for($i = 0; $i < count($transformation_solution_library); $i++): ?>
                        <?php if($i == 0): ?>
                            <h6 style="color: saddlebrown;font-size: 2em;">Transformation Solutions</h6>
                        <?php endif; ?>
                        <div class="">
                            <input value="<?php echo e($transformation_solution_library[$i]->id); ?>" disabled hidden
                                   class="document-id">
                            <div class=" ">
                                <?php if(isset($transformation_solution_library[$i]->excel)): ?>
                                    <?php $__currentLoopData = $transformation_solution_library[$i]->excel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p><?php echo e($transformation_solution_library[$i]->description); ?></p>
                                        <a href="<?php echo e('/download-file-excel/'.$upload->id); ?>"><?php echo e($transformation_solution_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <?php if(isset($transformation_solution_library[$i]->pdfs)): ?>
                                    <?php $__currentLoopData = $transformation_solution_library[$i]->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($transformation_solution_library[$i]->description); ?></p>
                                            <a href="<?php echo e('/download-file-pdf/'.$upload->id); ?>"><?php echo e($transformation_solution_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>

                <!--Business Incubation-->
                <div class="" id="incubation">
                    <?php for($i = 0; $i < count($business_incubation_library); $i++): ?>
                        <?php if($i == 0): ?>
                            <h6 style="color: saddlebrown;font-size: 2em;">Business Incubation</h6>
                        <?php endif; ?>
                        <div class="">
                            <input value="<?php echo e($business_incubation_library[$i]->id); ?>" disabled hidden class="document-id">
                            <div class=" ">
                                <?php if(isset($business_incubation_library[$i]->excel)): ?>
                                    <?php $__currentLoopData = $business_incubation_library[$i]->excel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p><?php echo e($business_incubation_library[$i]->description); ?></p>
                                        <a href="<?php echo e('/download-file-excel/'.$upload->id); ?>"><?php echo e($business_incubation_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <?php if(isset($business_incubation_library[$i]->pdfs)): ?>
                                    <?php $__currentLoopData = $business_incubation_library[$i]->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($business_incubation_library[$i]->description); ?></p>
                                            <a href="<?php echo e('/download-file-pdf/'.$upload->id); ?>"><?php echo e($business_incubation_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>

                <!--Loan Management-->
                <div class="" id="loan">
                    <?php for($i = 0; $i < count($loan_management_library); $i++): ?>
                        <?php if($i == 0): ?>
                            <h6 style="color: saddlebrown;font-size: 2em">Loan Management</h6>
                        <?php endif; ?>
                        <div class="">
                            <input value="<?php echo e($loan_management_library[$i]->id); ?>" disabled hidden class="document-id">
                            <div class=" ">
                                <?php if(isset($loan_management_library[$i]->excel)): ?>
                                    <?php $__currentLoopData = $loan_management_library[$i]->excel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p><?php echo e($loan_management_library[$i]->description); ?></p>
                                        <a href="<?php echo e('/download-file-excel/'.$upload->id); ?>"><?php echo e($loan_management_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <?php if(isset($loan_management_library[$i]->pdfs)): ?>
                                    <?php $__currentLoopData = $loan_management_library[$i]->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($loan_management_library[$i]->description); ?></p>
                                            <a href="<?php echo e('/download-file-pdf/'.$upload->id); ?>"><?php echo e($loan_management_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>

                <!--Fund Management-->
                <div class="" id="fund">
                    <?php for($i = 0; $i < count($fund_management_library); $i++): ?>
                        <?php if($i == 0): ?>
                            <h6 style="color: saddlebrown;font-size: 2em">Fund Management</h6>
                        <?php endif; ?>
                        <div class="">
                            <input value="<?php echo e($fund_management_library[$i]->id); ?>" disabled hidden class="document-id">
                            <div class=" ">
                                <?php if(isset($fund_management_library[$i]->excel)): ?>
                                    <?php $__currentLoopData = $fund_management_library[$i]->excel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p><?php echo e($fund_management_library[$i]->description); ?></p>
                                        <a href="<?php echo e('/download-file-excel/'.$upload->id); ?>"><?php echo e($fund_management_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <?php if(isset($fund_management_library[$i]->pdfs)): ?>
                                    <?php $__currentLoopData = $fund_management_library[$i]->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($fund_management_library[$i]->description); ?></p>
                                            <a href="<?php echo e('/download-file-pdf/'.$upload->id); ?>"><?php echo e($fund_management_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>

                <!--Private Equity Solution-->
                <div class="" id="equity">
                    <?php for($i = 0; $i < count($private_equity_library); $i++): ?>
                        <?php if($i == 0): ?>
                            <h6 style="color: saddlebrown;font-size: 2em">Private Equity Solution</h6>
                        <?php endif; ?>
                        <div class="">
                            <input value="<?php echo e($private_equity_library[$i]->id); ?>" disabled hidden class="document-id">
                            <div class=" ">
                                <?php if(isset($private_equity_library[$i]->excel)): ?>
                                    <?php $__currentLoopData = $private_equity_library[$i]->excel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p><?php echo e($private_equity_library[$i]->description); ?></p>
                                        <a href="<?php echo e('/download-file-excel/'.$upload->id); ?>"><?php echo e($private_equity_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <?php if(isset($private_equity_library[$i]->pdfs)): ?>
                                    <?php $__currentLoopData = $private_equity_library[$i]->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($private_equity_library[$i]->description); ?></p>
                                            <a href="<?php echo e('/download-file-pdf/'.$upload->id); ?>"><?php echo e($private_equity_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>

    <!--Mobile-->
    <div class="section desktopMobile" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s7 ">
                <h6 style="color: #9d5821;font-size: 2em;"><strong>Library</strong></h6>
                <hr class="line1">
                <p>The following documents are available for download:&nbsp;</p>

                <!--Supply Chain Development-->
                <div class="" id="supply">
                    <?php for($i = 0; $i < count($supply_chain_library); $i++): ?>
                        <?php if($i == 0): ?>
                            <h6 style="color: saddlebrown;">Supply Chain Development & Localization</h6>
                        <?php endif; ?>
                        <div class="">
                            <input value="<?php echo e($supply_chain_library[$i]->id); ?>" disabled hidden class="document-id">
                            <div class=" ">
                                <?php if(isset($supply_chain_library[$i]->excel)): ?>
                                    <?php $__currentLoopData = $supply_chain_library[$i]->excel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p><?php echo e($supply_chain_library[$i]->description); ?></p>
                                        <a href="<?php echo e('/download-file-excel/'.$upload->id); ?>"><?php echo e($supply_chain_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <?php if(isset($supply_chain_library[$i]->pdfs)): ?>
                                    <?php $__currentLoopData = $supply_chain_library[$i]->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($supply_chain_library[$i]->description); ?></p>
                                            <a href="<?php echo e('/download-file-pdf/'.$upload->id); ?>"><?php echo e($supply_chain_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>

                <!--Transformation Solution-->
                <div class="" id="transformationSolution">
                    <?php for($i = 0; $i < count($transformation_solution_library); $i++): ?>
                        <?php if($i == 0): ?>
                            <h6 style="color: saddlebrown;">Transformation Solution</h6>
                        <?php endif; ?>
                        <div class="">
                            <input value="<?php echo e($transformation_solution_library[$i]->id); ?>" disabled hidden
                                   class="document-id">
                            <div class=" ">
                                <?php if(isset($transformation_solution_library[$i]->excel)): ?>
                                    <?php $__currentLoopData = $transformation_solution_library[$i]->excel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p><?php echo e($transformation_solution_library[$i]->description); ?></p>
                                        <a href="<?php echo e('/download-file-excel/'.$upload->id); ?>"><?php echo e($transformation_solution_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <?php if(isset($transformation_solution_library[$i]->pdfs)): ?>
                                    <?php $__currentLoopData = $transformation_solution_library[$i]->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($transformation_solution_library[$i]->description); ?></p>
                                            <a href="<?php echo e('/download-file-pdf/'.$upload->id); ?>"><?php echo e($transformation_solution_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>

                <!--Business Incubation-->
                <div class="" id="incubation">
                    <?php for($i = 0; $i < count($business_incubation_library); $i++): ?>
                        <?php if($i == 0): ?>
                            <h6 style="color: saddlebrown;">Business Incubation</h6>
                        <?php endif; ?>
                        <div class="">
                            <input value="<?php echo e($business_incubation_library[$i]->id); ?>" disabled hidden class="document-id">
                            <div class=" ">
                                <?php if(isset($business_incubation_library[$i]->excel)): ?>
                                    <?php $__currentLoopData = $business_incubation_library[$i]->excel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p><?php echo e($business_incubation_library[$i]->description); ?></p>
                                        <a href="<?php echo e('/download-file-excel/'.$upload->id); ?>"><?php echo e($business_incubation_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <?php if(isset($business_incubation_library[$i]->pdfs)): ?>
                                    <?php $__currentLoopData = $business_incubation_library[$i]->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($business_incubation_library[$i]->description); ?></p>
                                            <a href="<?php echo e('/download-file-pdf/'.$upload->id); ?>"><?php echo e($business_incubation_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>

                <!--Loan Management-->
                <div class="" id="loan">
                    <?php for($i = 0; $i < count($loan_management_library); $i++): ?>
                        <?php if($i == 0): ?>
                            <h6 style="color: saddlebrown;">Loan Management</h6>
                        <?php endif; ?>
                        <div class="">
                            <input value="<?php echo e($loan_management_library[$i]->id); ?>" disabled hidden class="document-id">
                            <div class=" ">
                                <?php if(isset($loan_management_library[$i]->excel)): ?>
                                    <?php $__currentLoopData = $loan_management_library[$i]->excel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p><?php echo e($loan_management_library[$i]->description); ?></p>
                                        <a href="<?php echo e('/download-file-excel/'.$upload->id); ?>"><?php echo e($loan_management_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <?php if(isset($loan_management_library[$i]->pdfs)): ?>
                                    <?php $__currentLoopData = $loan_management_library[$i]->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($loan_management_library[$i]->description); ?></p>
                                            <a href="<?php echo e('/download-file-pdf/'.$upload->id); ?>"><?php echo e($loan_management_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>

                <!--Fund Management-->
                <div class="" id="fund">
                    <?php for($i = 0; $i < count($fund_management_library); $i++): ?>
                        <?php if($i == 0): ?>
                            <h6 style="color: saddlebrown;">Fund Management</h6>
                        <?php endif; ?>
                        <div class="">
                            <input value="<?php echo e($fund_management_library[$i]->id); ?>" disabled hidden class="document-id">
                            <div class=" ">
                                <?php if(isset($fund_management_library[$i]->excel)): ?>
                                    <?php $__currentLoopData = $fund_management_library[$i]->excel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p><?php echo e($fund_management_library[$i]->description); ?></p>
                                        <a href="<?php echo e('/download-file-excel/'.$upload->id); ?>"><?php echo e($fund_management_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <?php if(isset($fund_management_library[$i]->pdfs)): ?>
                                    <?php $__currentLoopData = $fund_management_library[$i]->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($fund_management_library[$i]->description); ?></p>
                                            <a href="<?php echo e('/download-file-pdf/'.$upload->id); ?>"><?php echo e($fund_management_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>

                <!--Private Equity Solution-->
                <div class="" id="equity">
                    <?php for($i = 0; $i < count($private_equity_library); $i++): ?>
                        <?php if($i == 0): ?>
                            <h6 style="color: saddlebrown;">Private Equity Solution</h6>
                        <?php endif; ?>
                        <div class="">
                            <input value="<?php echo e($private_equity_library[$i]->id); ?>" disabled hidden class="document-id">
                            <div class=" ">
                                <?php if(isset($private_equity_library[$i]->excel)): ?>
                                    <?php $__currentLoopData = $private_equity_library[$i]->excel; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <p><?php echo e($private_equity_library[$i]->description); ?></p>
                                        <a href="<?php echo e('/download-file-excel/'.$upload->id); ?>"><?php echo e($private_equity_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <?php if(isset($private_equity_library[$i]->pdfs)): ?>
                                    <?php $__currentLoopData = $private_equity_library[$i]->pdfs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $upload): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <p><?php echo e($private_equity_library[$i]->description); ?></p>
                                            <a href="<?php echo e('/download-file-pdf/'.$upload->id); ?>"><?php echo e($private_equity_library[$i]->title); ?></a>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </div>



    <script>
        $(document).ready(function () {
            $('.supplyChain').on('click', function () {
                location.href = '#supply';
            });
            $('.transformation').on('click', function () {
                location.href = '#transformationSolution';
            });

            $('.privateEquity').on('click', function () {
                location.href = '#equity';
            });
            $('.businessIncubation').on('click', function () {
                location.href = '#incubation';
            });
            $('.loanManagement').on('click', function () {
                location.href = '#loan';
            });
            $('.fundManagement').on('click', function () {
                location.href = '#fund';
            });
            $('.privateEquity').on('click', function () {
                location.href = '#euity';
            });

            $('.documents').each(function (obj) {

                $(this).on('click', function () {
                    let document_id = $(this).find('.document-id').val();

                    location.href = '/home/show-uploaded-libraries/' + document_id;
                });
            });

        });

    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views//home/documents.blade.php ENDPATH**/ ?>