

<?php $__env->startSection('content'); ?>
    <head>
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
    </head>
    <!--Desktop-->
    <div clas="section">
        <div class="card hoverable center" style="width: 800px; margin-left: 350px;">
            <form class="col s12" id="add-user" method="post">
                <br />
                <h2 style="font-size: 2em;font-family: Arial;">Edit User</h2>
                <br />
                <input id="user_id" value="<?php echo e($user->id); ?>" hidden>
                <?php echo csrf_field(); ?>
                <div class="row"style="margin-left: 95px">
                    <div class="input-field col m5">
                        <input id="name" type="text" value="<?php echo e($user->name); ?>" class="validate">
                        <label for="name">Name</label>
                    </div>
                    <div class="input-field col m5">
                        <input id="surname" value="<?php echo e($user->surname); ?>" type="text" class="validate">
                        <label for="surname">Surname</label>
                    </div>
                </div>
                <div class="row"style="margin-left: 95px">
                    <div class="input-field col m5">
                        <input id="email" value="<?php echo e($user->email); ?>" type="email" class="validate">
                        <label for="email">Email</label>
                    </div>
                    <div class="input-field col m5">
                        <input id="contact_number" value="<?php echo e($user->contact_number); ?>" type="tel" class="validate">
                        <label for="contact_number">Contact Number</label>
                    </div>
                </div>
                <div class="row"style="margin-left: 95px">
                    <div class="input-field col m5">
                        <textarea id="address" value="<?php echo e($user->address); ?>" class="materialize-textarea"></textarea>
                        <label for="address">Address</label>
                    </div>
                    <div class="input-field col m5">
                        <select id="role_id">
                            <?php $__currentLoopData = $roles; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $role): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($role->id); ?>"><?php echo e($role->display_name); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                        <label>System Role</label>
                    </div>
                </div>

                <button class="btn waves-effect waves-light"style="margin-left:300px;" id="save-user" name="action">Submit
                    <i class="material-icons right">send</i>
                </button>
                <br />
                <br />

            </form>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();

            $('#add-user').on('submit', function (e) {

                e.preventDefault();
                let formData = new FormData();
                formData.append('name', $('#name').val());
                formData.append('surname', $('#surname').val());
                formData.append('email', $('#email').val());
                formData.append('contact_number', $('#contact_number').val());
                formData.append('role_id', $('#role_id').val());
                formData.append('address', $('#address').val());
                console.log(formData);

                let url = '/update-edit-user/'+ '<?php echo e($user->id); ?>';

                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        console.log("success", response);
                        alert(response.message);
                        //window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        console.log("error", message);
                        let errors = response.responseJSON.errors;

                        for (var error in   errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);

                    }
                });
            });
        });
    </script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin-layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views//users/edit-user.blade.php ENDPATH**/ ?>