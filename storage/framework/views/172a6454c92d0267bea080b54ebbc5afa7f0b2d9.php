

<?php $__env->startSection('content'); ?>
    <div class="container">
        <h1 class="mb-2 text-center">Contact Us</h1>

        <?php if(session('message')): ?>
            <div class='alert alert-success'>
                <?php echo e(session('message')); ?>

            </div>
        <?php endif; ?>

        <div class="col-12 col-md-6">
            <form class="form-horizontal" method="POST" action="/contact">
                <?php echo e(csrf_field()); ?>

                <div class="form-group">
                    <label for="Name">Name: </label>
                    <input type="text" class="form-control" id="name" placeholder="Your name" name="name" required>
                </div>

                <div class="form-group">
                    <label for="email">Email: </label>
                    <input type="text" class="form-control" id="email" placeholder="john@example.com" name="email" required>
                </div>

                <div class="form-group">
                    <label for="message">message: </label>
                    <textarea type="text" class="form-control luna-message" id="message" placeholder="Type your messages here" name="message" required></textarea>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary" value="Send">Send</button>
                </div>
            </form>
        </div>
    </div>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/vagrant/code/engeli-sinazo/resources/views//home/contact.blade.php ENDPATH**/ ?>