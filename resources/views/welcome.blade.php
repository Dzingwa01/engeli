@extends('layouts.app')

@section('content')
    <link rel="stylesheet" href="/css/home/home.css"/>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">--}}
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        {{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>--}}
    </head>
    <br>
    <br>


    <!--Desktop Display-->

    <div class="row" id="homeDesktop">
        <br>
        <br>
        <p class="center-align" style="font-size: 1.2em;color: grey">To get an update on the COVID-19 Corona Virus in South Africa, visit the following <a href="https://www.sacoronavirus.co.za/" target="_blank"><span style="color: red">link</span></a></p>
        <div id="ns-slider">
            <!--    Start: Buttons-->
            <button class="ns-slide-btn control_next"><i class="fa fa-chevron-right"></i></button>
            <button class="ns-slide-btn control_prev"><i class="fa fa-chevron-left"></i></button>
            <!--    End: Buttons-->
            <!--    Start: Images-->
            <ul class="image_slider_ul">
                <li>
                    <div class="bgimage" style="background-image: url('/images/First slide.jpg')">
                        <div class="ns-slider-text-area">
                            <p class="ns-slide-text"><b>Evaluate + Enhance + Advance</b></p>
                        </div>

                    </div>
                </li>

                <li>
                    <div class="bgimage" style="background-image: url('/images/Building wealth.jpg')">
                        <div class="ns-slider-text-area">
                            <p class="ns-slide-text"><b>Building Wealth One Company At A Time</b></p>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="bgimage" style="background-image: url('/images/slide 3.jpg')">
                        <div class="ns-slider-text-area">
                            <p class="ns-slide-text"><b>Unlocking New Opportunities + Your Potential</b></p>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="bgimage" style="background-image: url('/images/Strenghting.jpg')">
                        <div class="ns-slider-text-area">
                            <p class="ns-slide-text"><b>Strengthening Supply Chains for Growth</b></p>
                        </div>
                    </div>
                </li>
            </ul>
            <!--    End: Images-->
            <!--    Start: Indicators-->
            <div class="ns-indicator_con">
                <ul class="ns-indicator"></ul>
            </div>
            <!--    End Indicators-->
        </div>
        <div class="section center-align">
            <div class="row">
                <div class="row" style="margin-right: 2em;margin-left: 2em;">
                    <h6 style="color: #cd4300;font-size: 2em;"><b>Our Origin</b></h6>
                    <p>Engeli,&nbsp;a name derived from the word <em>Engele</em> (German for Angel), is the name given
                        to a range of mountains in the Eastern Cape. It was this range of mountains looked upon by
                        Oliver Reginald Tambo during his formative years and the remaining years of his life following
                        his exile.</p>
                    <p>"Looking out from my home, the site of it commanded a wide view of the terrain as it swept from
                        the vicinity of my home and stretched away as far as the eye could see – the panorama bordered
                        on a high range of mountains that we could see from our home. The Engeli Mountains were a huge
                        wall that rolls in the distance to mark the end of a very broken landscape, landscape of great
                        variety and, looking back now, I would say of great beauty? But the nagging question was, what
                        lay beyond the Engeli Mountains? Just exactly what was there? How far
                        would one be able to walk over the mountains to Egoli, Johannesburg? What sort of world would it
                        be? What did it conceal from my view?" </p>
                    <p>I saw two worlds. The one in the vicinity of my home? This was my world. I understood it from my
                        mother’s rondavel? I was part of this world. There was obviously another one beyond the Engeli
                        Mountains.”<br> <strong>OLIVER TAMBO, BEYOND THE ENGELI MOUNTAINS by Lulu Callinicos. </strong>
                    </p>
                </div>
            </div>
        </div>
    </div>


    <!--Mobile Display-->
    <div class="row" id="homeMobile">
        <br>
        <br>
        <br>
        <p class="center-align" style="font-size: 1.2em;color: grey">To get an update on the COVID-19 Corona Virus in South Africa, visit the following <a href="https://www.sacoronavirus.co.za/" target="_blank">  <span style="color: red">link</span></a></p>


        <div class="container">
            <div class="carousel" id="myCarousel">
                <a class="carousel-item" style="width: 100%;height:100%">
                    <div class="about" alt="First Slide"></div>
                    <div class="txt" id="aboutTxt"><b>Evaluate + Enhance + Advance</b></div>
                </a>
                <a class="carousel-item" style="width:100%;">
                    <div class="team" alt="Second Slide"></div>
                    <text class="txt" id="teamTxt"><b>Building Wealth One Company At A Time</b></text>
                </a>
                <a class="carousel-item" style="width:100%;">
                    <div class="methodology" alt="Third Slide"></div>
                    <text class="txt" id="methodologyTxt"><b>Unlocking New Opportunities + Your Potential</b></text>
                </a>
                <a class="carousel-item" style="width:100%;">
                    <div class="services" alt="Fourth Slide"></div>
                    <text class="txt" id="servicesTxt"><b>Strengthening Supply Chains for Growth</b></text>
                </a>
            </div>
        </div>

        <div class="section center-align" style="margin-right: 2em;margin-left: 2em;">
            <div class="row ">
                <div class="row col s12">
                    <h6 style="color: #9d5821;font-size: 2em;"><b>Our Origin</b></h6>
                    <p>Engeli,&nbsp;a name derived from the word <em>Engele</em> (German for Angel), is the name given
                        to a range of mountains in the Eastern Cape. It was this range of mountains looked upon by
                        Oliver Reginald Tambo during his formative years and the remaining years of his life following
                        his exile. </p>
                    <p>Looking out from my home, the site of it commanded a wide view of the terrain as it swept from
                        the vicinity of my home and stretched away as far as the eye could see – the panorama bordered
                        on a high range of mountains that we could see from our home. The Engeli Mountains were a huge
                        wall that rolls in the distance to mark the end of a very broken landscape, landscape of great
                        variety and, looking back now, I would say of great beauty? But the nagging question was, what
                        lay beyond the Engeli Mountains? Just exactly what was there? How far
                        would one be able to walk over the mountains to Egoli, Johannesburg? What sort of world would it
                        be? What did it conceal from my view? </p>
                    <p>I saw two worlds. The one in the vicinity of my home? This was my world. I understood it from my
                        mother’s rondavel? I was part of this world. There was obviously another one beyond the Engeli
                        Mountains.”<br> <strong>OLIVER TAMBO, BEYOND THE ENGELI MOUNTAINS by Lulu Callinicos. </strong>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <style>
        .ns-hover-card-title {
            padding-bottom: 100px;
            max-width: 450px;
        }
        .ns-hover-card-title strong {
            font-size: 50px;
            color: #e6382a;
        }
        @media screen and (max-width: 767px) {
            .ns-hover-card-title {
                padding-bottom: 40px;
            }
            .ns-hover-card-title strong {
                font-size: 25px;
            }
        }

        /*NS Image Slider with Text */
        /*  Overall Conatiner*/
        #ns-slider {
            position: relative;
            /* margin: 20px auto 0 auto; */
            border-radius: 4px;
            width: 100%;
            height: 550px;
            overflow: hidden;
        }

        #ns-slider ul.image_slider_ul {
            position: relative;
            margin: 0;
            padding: 0;
            width: 400%;
            height: 100%;
            list-style: none;
        }

        #ns-slider ul.image_slider_ul li {
            position: relative;
            display: block;
            float: left;
            margin: 0;
            padding: 0;
            width: 25%;
            height: 100%;
        }

        #ns-slider ul.image_slider_ul li .bgimage {
            width: 100%;
            height: 100%;
            position: relative;
            /* background-color: #333; */
            background-position: 100% 0%;
            background-size: cover;
            background-repeat: no-repeat;
            /* -webkit-filter: blur(2px); */
        }

        /*  Indicators*/

        #ns-slider .ns-indicator_con {
            width: 100%;
            height: auto;
            position: absolute;
            display: flex;
            align-items: center;
            justify-content: center;
            padding: 10px 0;
            bottom: 0;
        }

        #ns-slider .ns-indicator_con ul.ns-indicator {
            width: 100%;
            height: auto;
            position: relative;
            text-align: center;
        }

        #ns-slider .ns-indicator_con ul.ns-indicator li {
            position: relative;
            list-style: none;
            width: 20px;
            height: 4px;
            background-color: #fff;
            /* border: 1px solid #fff; */
            display: inline-block;
            margin: 0 3px;
            border-radius: 1.5px;
            transition: 800ms ease-in-out;
        }

        #ns-slider .ns-indicator_con ul.ns-indicator li.active {
            background-color: #f2b206;
        }

        /*  Buttons*/

        .ns-slide-btn.control_prev,
        .ns-slide-btn.control_next {
            position: absolute;
            /* top: 42%; */
            z-index: 999;
            display: block;
            width: 7%;
            height: 100%;
            background: transparent;
            /* background: rgba(0,0,0,0.1); */
            color: #eee;
            text-decoration: none;
            font-weight: 600;
            font-size: 18px;
            border: none;
            outline: none;
            /* opacity: 0.8; */
            cursor: pointer;
        }

        ns-slide-btn.control_prev:hover,
        .ns-slide-btn.control_next:hover {
            opacity: 1;
            -webkit-transition: all 0.2s ease;
            /* background-color: #fff; */
            color: #fff;
        }

        .ns-slide-btn.control_prev {
            border-radius: 0 4px 4px 0;
        }

        .ns-slide-btn.control_next {
            right: 0;
            border-radius: 4px 0 0 4px;
        }

        .ns-slider-text-area {
            max-width: 100%;
            position: absolute;
            color: #fff;
            top: 200px;
            left: 200px;
        }
        .ns-slide-title {
            line-height: 1.2;
            font-weight: 700;
            font-size: 65px;
            color: #fff;
        }
        .ns-slide-text {
            font-size: 45px;
            color: #cd4300;
            font-style: italic;
            text-shadow: 5px 3px 1px #404040;
        }
        @media screen and (max-width: 768px) {
            .ns-slider-text-area {
                max-width: 100%;
                top: 130px;
                left: 50px;
            }
            .ns-slide-title {
                font-size: 50px;
            }
            .ns-slide-text {
                display: none;
            }
        }
        @media screen and (max-width: 480px) {
            .ns-slider-text-area {
                max-width: 100%;
                top: 130px;
                left: 50px;
            }
            .ns-slide-title {
                font-size: 38px;
            }
        }

    </style>

    {{--<script src="https://cdnjs.cloudflare.com/ajax/materialize/1.0.0-beta/js/materialize.js"></script>--}}

    <script>

        $(document).ready(function () {
            var slideCount = $("#ns-slider ul.image_slider_ul li").length;
            var slideWidth = $("#ns-slider ul.image_slider_ul li").width();
            var slideHeight = $("#ns-slider ul.image_slider_ul li").height();
            var sliderUlWidth = slideCount * slideWidth;

            $("#ns-slider ul.image_slider_ul").css({ marginLeft: -slideWidth });

            $("#ns-slider ul.image_slider_ul li:last-child").prependTo(
                "#ns-slider ul.image_slider_ul"
            );

            function moveLeft() {
                $("#ns-slider ul.image_slider_ul").animate(
                    {
                        left: +slideWidth
                    },
                    1500,
                    function () {
                        $("#ns-slider ul.image_slider_ul li:last-child").prependTo(
                            "#ns-slider ul.image_slider_ul"
                        );
                        $("#ns-slider ul.image_slider_ul").css("left", "");
                    }
                );
            }

            function moveRight() {
                $("#ns-slider ul.image_slider_ul").animate(
                    {
                        left: -slideWidth
                    },
                    1500,
                    function () {
                        $("#ns-slider ul.image_slider_ul li:first-child").appendTo(
                            "#ns-slider ul.image_slider_ul"
                        );
                        $("#ns-slider ul.image_slider_ul").css("left", "");
                    }
                );
            }

            var navDots = [];

            for (var i = 0; i < slideCount; i++) {
                navDots[i] = '<li currentSlide="' + i + '"></li>';
                $(".ns-indicator").append(navDots[i]);
            }

            var count = 0;
            $("ul.ns-indicator li").eq(count).addClass("active");

            slideCountforindicators = slideCount - 1;
            $(".ns-slide-btn.control_prev").click(function () {
                moveLeft();

                $("ul.ns-indicator li").eq(count).removeClass("active");
                count--;
                if (count < 0) {
                    count = slideCountforindicators;
                }

                $("ul.ns-indicator li").eq(count).addClass("active");
            });

            $(".ns-slide-btn.control_next").click(function () {
                moveRight();

                $("ul.ns-indicator li").eq(count).removeClass("active");
                count++;
                if (count > slideCountforindicators) {
                    count = 0;
                }

                $("ul.ns-indicator li").eq(count).addClass("active");
            });

            //   Automatic Slider

            setInterval(function () {
                if ($("#ns-slider").is(":hover")) {
                } else {
                    moveRight();
                    $("ul.ns-indicator li").eq(count).removeClass("active");
                    count++;
                    if (count > slideCountforindicators) {
                        count = 0;
                    }

                    $("ul.ns-indicator li").eq(count).addClass("active");
                }
            }, 9000);

        });

    </script>
@endsection

