@extends('layouts.clerk-layout')

@section('content')
    <br>
    <br>
    <br>
    <div class="card"style="width:700px;margin:  0 auto;">
        <br>
        <div class="row">
            <div class="col s4">
                <img style="width:150px;height: 100px;margin-left: 530px;" src="/images/home/logoo.png" class="engeliLogo"/>
            </div>
            <div class="col s4">
                <h6 style="margin-left: 10px;font-size: 2em;color: #9d5821"><strong>Uploads</strong></h6>
            </div>
        </div>
        <div class="row"style="margin-left: 10px;">
            <div class="col s4">
                <h6 style="font-size: 2em;">{{$library->title}}</h6>
            </div>
        </div>
        <div class="row"style="margin-left: 10px">
            <div class="col s4">
                <p>{{$library->description}}</p>
            </div>
        </div>
        <div class="row"style="margin-left: 10px">
            <div class="col s4">
                @if(isset($library->image_url)?$library->image_url:'Not set')
                    <h6>Uploaded Image:</h6>
                    <img src="{{$library->image_url}}" style="width:650px; height:200px;">
                    <a href="{{$library->image_url}}" download>Download Image</a>
                    @if($library->image_url == 'Not set')
                    <p>No image has been uploaded.</p>
                    @endif
                @endif
            </div>
        </div>
        <div class="row"style="margin-left: 10px">
            <div class="col s4">
                @if(isset($library->pdf_url)?$library->pdf_url:'Not set')
                    <h6>PDF File</h6>
                    <a href="{{$library->pdf_url}}" download>Download PDF</a>
                    @if($library->pdf_url == 'Not set')
                        <p>No pdf file uploaded</p>
                    @endif
                @endif
            </div>
        </div>
        <div class="row"style="margin-left: 10px">
            <div class="col s4">
                @if(isset($library->pdf_url)?$library->video_url:'Not set')
                    <h6>Video</h6>
                    <a href="{{$library->video_url}}" download>Download PDF</a>
                    @if($library->video_url == 'Not set')
                        <p>No video uploaded</p>
                    @endif
                @endif
            </div>
        </div>
        <br>
    </div>
    <br>
    <br>

    @endsection