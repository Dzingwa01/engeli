@extends('layouts.clerk-layout')
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
@section('content')
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="card"style="width:800px;margin: 0 auto;">
        <br>
        <form class="col s12">
            <h6 style="font-size: 2em;margin-left: 300px;color: #9d5821"><strong>Upload File</strong></h6>
            <div class="row"style="margin-right: 2em;margin-left: 2em;">
                <div class="input-field col s6">
                    <input id="title" type="text" class="validate">
                    <label for="title">Title</label>
                </div>
                <div class="input-field col s6">
                    <input id="description" type="text" class="validate">
                    <label for="description">Description</label>
                </div>
            </div>
            <div class="row"style="margin-left: 2em;margin-right: 2em;">
                <div class="col m6">
                    <div class="file-field input-field" style="bottom:0px!important;">
                        <div class="btn">
                            <span>Upload Image</span>
                            <input id="image_url" type="file" name="image_url" multiple>
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path" type="text">
                        </div>
                    </div>
                </div>
                <div class="col m6">
                    <div class="file-field input-field" style="bottom:0px!important;">
                        <div class="btn">
                            <span>Upload Video</span>
                            <input id="video_url" type="file" name="video_url">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path" type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row"style="margin-left: 2em;margin-right: 2em;">
                <div class="col m6">
                    <div class="file-field input-field" style="bottom:0px!important;">
                        <div class="btn">
                            <span>Upload PDF</span>
                            <input id="pdf_url" type="file" name="pdf_url">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path"
                                   type="text">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-left: 600px;">
                <a class="waves-effect waves-light btn" id="upload-submit-button">Submit</a>
            </div>
            <br>
            <br>
        </form>
    </div>



    <script>
        $(document).ready(function () {

            $('#upload-submit-button').on('click', function () {

                let formData = new FormData();
                formData.append('title', $('#title').val());
                formData.append('description' ,$('#description').val());

                jQuery.each(jQuery('#image_url')[0].files, function (i, file) {
                    formData.append('image_url', file);
                });
                jQuery.each(jQuery('#pdf_url')[0].files, function (i, file) {
                    formData.append('pdf_url', file);
                });
                jQuery.each(jQuery('#video_url')[0].files, function (i, file) {
                    formData.append('video_url', file);
                });

                console.log(formData);
                let url = "{{route('library.store')}}";
                $.ajax({
                    url:url,
                    data: formData,
                    type: 'post',
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                    },


                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

        });

    </script>

    @endsection