@extends('layouts.app')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <link rel="stylesheet" type="text/css" href="/css/host/host.css" />

    <div class="card" id="hostRegistrationDesktop" style="margin-top: 20vh;width: 700px;margin-left: 400px">
        <div class="row" style="margin-left: 3em;margin-right: 3em">
            <br>
            <h5 style="margin-left: 100px;color: grey"><b>Host Employer Registration form</b></h5>
            <div class="input-field col s6">
                <input id="company_name" type="text" class="validate"  required>
                <label for="company_name">Company Name</label>
            </div>
            <div class="input-field col s6">
                <input id="company_reg_number" type="text" class="validate" required>
                <label for="company_reg_number">Company Registration Number</label>
            </div>
        </div>
        <div class="row" style="margin-left: 3em;margin-right: 3em">
            <div class="input-field col s6">
                <input id="physical_address" type="text" class="validate" required>
                <label for="physical_address">Physical Address</label>
            </div>
            <div class="input-field col s6">
                <input id="contact_name" type="text" class="validate" required>
                <label for="contact_name">Contact name</label>
            </div>
        </div>
        <div class="row" style="margin-left: 3em;margin-right: 3em">
            <div class="input-field col s6">
                <input id="contact_email" type="text" class="validate" required>
                <label for="contact_email">Contact email</label>
            </div>
            <div class="input-field col s6">
                <input id="contact_number" type="text" class="validate" required>
                <label for="contact_number" >Contact number</label>
            </div>
        </div>
        <div class="row" style="margin-left: 3em;margin-right: 3em">
            <div class="input-field col s6">
                <input id="industry" type="text" class="validate" required>
                <label for="industry">Industry</label>
            </div>
            <div class="input-field col s6">
                <input id="number_of_candidates" type="text" class="validate" required>
                <label for="number_of_candidates" >Number of candidates</label>
            </div>
        </div>
        <div class="input-field" style="margin-left: 4em;margin-right: 4em">
            <textarea id="message" placeholder="Message" rows="4" cols="50"></textarea>
        </div>
        <div class="row" style="margin-left: 300px;">
            <a class="waves-effect waves-light btn" style="background-color: grey"
               id="host-employee-submit-button-desktop">Submit</a>
        </div>
        <br>
    </div>

    <div class="card" id="hostRegistrationMobile" style="margin-top: 20vh;width: 300px;margin-left:2em">
        <div class="row" style="margin-left: 3em;margin-right: 3em">
            <br>
            <h6 style="color: grey"><b>Host Employer Registration</b></h6>
        </div>
        <div class="row">
            <div class="input-field col s8" style="margin-left: 3em">
                <input id="company_name" type="text" class="validate"  required>
                <label for="company_name">Company Name</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s8" style="margin-left: 3em">
                <input id="company_reg_number" type="text" class="validate" required>
                <label for="company_reg_number">Company Registration Number</label>
            </div>
        </div>
        <div class="row" >
            <div class="input-field col s8" style="margin-left: 3em">
                <input id="physical_address" type="text" class="validate" required>
                <label for="physical_address">Physical Address</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s8" style="margin-left: 3em">
                <input id="contact_name" type="text" class="validate" required>
                <label for="contact_name">Contact name</label>
            </div>
        </div>

        <div class="row">
            <div class="input-field col s8" style="margin-left: 3em">
                <input id="contact_email" type="text" class="validate" required>
                <label for="contact_email">Contact email</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s8" style="margin-left: 3em">
                <input id="contact_number" type="text" class="validate" required>
                <label for="contact_number" >Contact number</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s8" style="margin-left: 3em">
                <input id="industry" type="text" class="validate" required>
                <label for="industry">Industry</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s8" style="margin-left: 3em">
                <input id="number_of_candidates" type="text" class="validate" required>
                <label for="number_of_candidates" >Number of candidates</label>
            </div>
        </div>
        <div class="input-field" style="margin-left: 4em;margin-right: 4em">
            <textarea id="message" placeholder="Message" rows="4" cols="50"></textarea>
        </div>
        <div class="row" style="margin-left: 100px;">
            <a class="waves-effect waves-light btn" style="background-color: grey"
               id="host-employee-submit-button-mobile">Submit</a>
        </div>
        <br>
    </div>

    <script>
        $(document).ready(function () {
            $('#host-employee-submit-button-desktop').on('click', function () {
                let formData = new FormData();
                formData.append('company_name', $('#company_name').val());
                formData.append('company_reg_number', $('#company_reg_number').val());
                formData.append('physical_address', $('#physical_address').val());
                formData.append('contact_name', $('#contact_name').val());
                formData.append('contact_email', $('#contact_email').val());
                formData.append('contact_number', $('#contact_number').val());
                formData.append('number_of_candidates', $('#number_of_candidates').val());
                formData.append('industry', $('#industry').val());
                formData.append('category_id', $('#category_id').val());
                formData.append('message', $('#message').val());


                $.ajax({
                    url: '{{route('store-host-employee-registration')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            $('#host-employee-submit-button-mobile').on('click', function () {
                let formData = new FormData();
                formData.append('company_name', $('#company_name').val());
                formData.append('company_reg_number', $('#company_reg_number').val());
                formData.append('physical_address', $('#physical_address').val());
                formData.append('contact_name', $('#contact_name').val());
                formData.append('contact_email', $('#contact_email').val());
                formData.append('contact_number', $('#contact_number').val());
                formData.append('number_of_candidates', $('#number_of_candidates').val());
                formData.append('industry', $('#industry').val());
                formData.append('category_id', $('#category_id').val());
                formData.append('message', $('#message').val());


                $.ajax({
                    url: '{{route('store-host-employee-registration')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
    </script>
    @endsection
