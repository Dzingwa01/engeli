@extends('layouts.admin-layout')

@section('content')
    <br>
    <div class="container-fluid">
        <div class="" style="margin-left: 10em">
            <br>
            <div class="row">
                <h6 style="text-align: center;font-size: 2em;color: #9d5821"><strong>Uploads</strong></h6>
            </div>
            <div class="row" style="margin-left:2em;margin-right: 2em;">
                <h6 style="text-align:center;font-size: 1.3em;">{{$library->title}}</h6>
                <br>
                <h6>{{$library->category}}</h6>
                @if(isset($library->excel))
                    @foreach($library->excel as $upload)
                        <a href="{{$upload->excel_sheet}}">{{$library->title}}</a>
                    @endforeach
                @endif
            </div>
            <div class="row" style="margin-left: 10px">
                <div class="col s4">
                    @if(isset($library->pdfs))
                        @foreach($library->pdfs as $upload)
                            <a href="{{$upload->pdf_url}}">{{$library->title}}</a>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="row" style="margin-left: 10px">
                <div class="col s4">
                    @if(isset($library->videos))
                        @foreach($library->videos as $upload)
                            <a href="{{$upload->video_url}}">{{$library->title}}</a>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="row" style="margin-left: 10px">
                <div class="col s4">
                    @if(isset($library->images))
                        @foreach($library->images as $upload)
                            <img src="{{$upload->image_url}}" width="400" height="300">
                        @endforeach
                    @endif
                </div>
            </div>
            <br>
        </div>
    </div>
@endsection