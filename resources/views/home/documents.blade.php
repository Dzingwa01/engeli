@extends('layouts.app')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <link rel="stylesheet" type="text/css" href="/css/documents/documents.css"/>
    <br>
    <div class="row center-align" style="margin-top: 12vh;">
        <h6 style="color: #cd4300;font-size: 2em;top: 5vh"><b>Library</b></h6>
        <p style="color: #cd4300;font-size: 1.5em">The following documents are available for download</p>
    </div>
    <div class="section center-align desktopDocuments">
        <div class="row" style="margin-left: 10em">
            <!--Supply Chain Development-->
            <div class="z-depth-4 col s5" id="supply">
                <i class="material-icons medium">archive</i>
                <h6 style="color: #cd4300;">Supply Chain Development & Localization</h6>
            @for($i = 0; $i < count($supply_chain_library); $i++)
                    @if($i == 0)
                    @endif
                    <div class="">
                        <input value="{{$supply_chain_library[$i]->id}}" disabled hidden class="document-id">
                        <div class=" ">
                            @if(isset($supply_chain_library[$i]->excel))
                                @foreach($supply_chain_library[$i]->excel as $upload)
                                    <p>{{$supply_chain_library[$i]->description}}</p>
                                    <a href="{{'/download-file-excel/'.$upload->id}}">{{$supply_chain_library[$i]->title}}</a>
                                @endforeach
                            @endif
                            @if(isset($supply_chain_library[$i]->pdfs))
                                @foreach($supply_chain_library[$i]->pdfs as $upload)
                                    <p>{{$supply_chain_library[$i]->description}}</p>
                                    <a href="{{'/download-file-pdf/'.$upload->id}}">{{$supply_chain_library[$i]->title}}</a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endfor
                <br>
            </div>
            <!--Transformation Solution-->
            <div class="z-depth-4 col s5" id="transformationSolution" style="margin-left: 2em">
                <i class="material-icons medium">archive</i>
                <h6 style="color: #cd4300;">Transformation Solutions</h6>
            @for($i = 0; $i < count($transformation_solution_library); $i++)
                    @if($i == 0)
                    @endif
                    <div class="">
                        <input value="{{$transformation_solution_library[$i]->id}}" disabled hidden
                               class="document-id">
                        <div class=" ">
                            @if(isset($transformation_solution_library[$i]->excel))
                                @foreach($transformation_solution_library[$i]->excel as $upload)
                                    <p>{{$transformation_solution_library[$i]->description}}</p>
                                    <a href="{{'/download-file-excel/'.$upload->id}}">{{$transformation_solution_library[$i]->title}}</a>
                                @endforeach
                            @endif
                            @if(isset($transformation_solution_library[$i]->pdfs))
                                @foreach($transformation_solution_library[$i]->pdfs as $upload)
                                    <p>{{$transformation_solution_library[$i]->description}}</p>
                                    <a href="{{'/download-file-pdf/'.$upload->id}}">{{$transformation_solution_library[$i]->title}}</a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endfor
                <br>
            </div>
        </div>
        <div class="row" style="margin-left: 10em">
            <!--Loan Management-->
            <div class="z-depth-4 col s5" id="loan">
                <i class="material-icons medium">archive</i>
                <h6 style="color: #cd4300;">Loan Management</h6>
            @for($i = 0; $i < count($loan_management_library); $i++)
                    @if($i == 0)
                    @endif
                    <div class="">
                        <input value="{{$loan_management_library[$i]->id}}" disabled hidden class="document-id">
                        <div class=" ">
                            @if(isset($loan_management_library[$i]->excel))
                                @foreach($loan_management_library[$i]->excel as $upload)
                                    <p>{{$loan_management_library[$i]->description}}</p>
                                    <a href="{{'/download-file-excel/'.$upload->id}}">{{$loan_management_library[$i]->title}}</a>
                                @endforeach
                            @endif
                            @if(isset($loan_management_library[$i]->pdfs))
                                @foreach($loan_management_library[$i]->pdfs as $upload)
                                    <p>{{$loan_management_library[$i]->description}}</p>
                                    <a href="{{'/download-file-pdf/'.$upload->id}}">{{$loan_management_library[$i]->title}}</a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endfor
                <br>
            </div>
            <!--Fund Management-->
            <div class="z-depth-4 col s5" id="loan" style="margin-left: 2em">
                <i class="material-icons medium">archive</i>
                <h6 style="color: #cd4300;">Fund Management</h6>
            @for($i = 0; $i < count($fund_management); $i++)
                    @if($i == 0)
                    @endif
                    <div class="">
                        <input value="{{$fund_management[$i]->id}}" disabled hidden class="document-id">
                        <div class=" ">
                            @if(isset($fund_management[$i]->excel))
                                @foreach($fund_management[$i]->excel as $upload)
                                    <p>{{$fund_management[$i]->description}}</p>
                                    <a href="{{'/download-file-excel/'.$upload->id}}">{{$fund_management[$i]->title}}</a>
                                @endforeach
                            @endif
                            @if(isset($fund_management[$i]->pdfs))
                                @foreach($fund_management[$i]->pdfs as $upload)
                                    <p>{{$fund_management[$i]->description}}</p>
                                    <a href="{{'/download-file-pdf/'.$upload->id}}">{{$fund_management[$i]->title}}</a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endfor
                <br>
            </div>
        </div>
        <div class="row" style="margin-left: 10em">
            <!--Business Incubation-->
            <div class="z-depth-4 col s5" id="incubation">
                <i class="material-icons medium">archive</i>
                <h6 style="color: #cd4300;">Business Incubation</h6>
            @for($i = 0; $i < count($business_incubation_library); $i++)
                    @if($i == 0)
                    @endif
                    <div class="">
                        <input value="{{$business_incubation_library[$i]->id}}" disabled hidden class="document-id">
                        <div class=" ">
                            @if(isset($business_incubation_library[$i]->excel))
                                @foreach($business_incubation_library[$i]->excel as $upload)
                                    <p>{{$business_incubation_library[$i]->description}}</p>
                                    <a href="{{'/download-file-excel/'.$upload->id}}">{{$business_incubation_library[$i]->title}}</a>
                                @endforeach
                            @endif
                            @if(isset($business_incubation_library[$i]->pdfs))
                                @foreach($business_incubation_library[$i]->pdfs as $upload)
                                    <p>{{$business_incubation_library[$i]->description}}</p>
                                    <a href="{{'/download-file-pdf/'.$upload->id}}">{{$business_incubation_library[$i]->title}}</a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endfor
                <br>
            </div>
            <!--Fund Management-->
            <div class="z-depth-4 col s5" id="fund" style="margin-left: 2em">
                <i class="material-icons medium">archive</i>
                <h6 style="color: #cd4300;">Human Capital Solutions</h6>
            @for($i = 0; $i < count($fund_management_library); $i++)
                    @if($i == 0)
                    @endif
                    <div class="">
                        <input value="{{$fund_management_library[$i]->id}}" disabled hidden class="document-id">
                        <div class=" ">
                            @if(isset($fund_management_library[$i]->excel))
                                @foreach($fund_management_library[$i]->excel as $upload)
                                    <p>{{$fund_management_library[$i]->description}}</p>
                                    <a href="{{'/download-file-excel/'.$upload->id}}">{{$fund_management_library[$i]->title}}</a>
                                @endforeach
                            @endif
                            @if(isset($fund_management_library[$i]->pdfs))
                                @foreach($fund_management_library[$i]->pdfs as $upload)
                                    <p>{{$fund_management_library[$i]->description}}</p>
                                    <a href="{{'/download-file-pdf/'.$upload->id}}">{{$fund_management_library[$i]->title}}</a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endfor
                <br>
            </div>
        </div>
        <div class="row" style="margin-left: 10em">
            <!--Private Equity Solution-->
            <div class="z-depth-4 col s5" id="equity">
                <i class="material-icons medium">archive</i>
                <h6 style="color: #cd4300;">Private Equity Solution</h6>
            @for($i = 0; $i < count($private_equity_library); $i++)
                    @if($i == 0)
                    @endif
                    <div class="">
                        <input value="{{$private_equity_library[$i]->id}}" disabled hidden class="document-id">
                        <div class=" ">
                            @if(isset($private_equity_library[$i]->excel))
                                @foreach($private_equity_library[$i]->excel as $upload)
                                    <p>{{$private_equity_library[$i]->description}}</p>
                                    <a href="{{'/download-file-excel/'.$upload->id}}">{{$private_equity_library[$i]->title}}</a>
                                @endforeach
                            @endif
                            @if(isset($private_equity_library[$i]->pdfs))
                                @foreach($private_equity_library[$i]->pdfs as $upload)
                                    <p>{{$private_equity_library[$i]->description}}</p>
                                    <a href="{{'/download-file-pdf/'.$upload->id}}">{{$private_equity_library[$i]->title}}</a>
                                @endforeach
                            @endif
                        </div>
                    </div>
                @endfor
                <br>
            </div>
        </div>
    </div>

    <!--Mobile-->
    <div class="section desktopMobile" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s7 ">
                <!--Supply Chain Development-->
                <div class="" id="supply">
                    @for($i = 0; $i < count($supply_chain_library); $i++)
                        @if($i == 0)
                            <h6 style="color: saddlebrown;">Supply Chain Development & Localization</h6>
                        @endif
                        <div class="">
                            <input value="{{$supply_chain_library[$i]->id}}" disabled hidden class="document-id">
                            <div class=" ">
                                @if(isset($supply_chain_library[$i]->excel))
                                    @foreach($supply_chain_library[$i]->excel as $upload)
                                        <p>{{$supply_chain_library[$i]->description}}</p>
                                        <a href="{{'/download-file-excel/'.$upload->id}}">{{$supply_chain_library[$i]->title}}</a>
                                    @endforeach
                                @endif
                                @if(isset($supply_chain_library[$i]->pdfs))
                                    @foreach($supply_chain_library[$i]->pdfs as $upload)
                                            <p>{{$supply_chain_library[$i]->description}}</p>
                                            <a href="{{'/download-file-pdf/'.$upload->id}}">{{$supply_chain_library[$i]->title}}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endfor
                </div>

                <!--Transformation Solution-->
                <div class="" id="transformationSolution">
                    @for($i = 0; $i < count($transformation_solution_library); $i++)
                        @if($i == 0)
                            <h6 style="color: saddlebrown;">Transformation Solution</h6>
                        @endif
                        <div class="">
                            <input value="{{$transformation_solution_library[$i]->id}}" disabled hidden
                                   class="document-id">
                            <div class=" ">
                                @if(isset($transformation_solution_library[$i]->excel))
                                    @foreach($transformation_solution_library[$i]->excel as $upload)
                                        <p>{{$transformation_solution_library[$i]->description}}</p>
                                        <a href="{{'/download-file-excel/'.$upload->id}}">{{$transformation_solution_library[$i]->title}}</a>
                                    @endforeach
                                @endif
                                @if(isset($transformation_solution_library[$i]->pdfs))
                                    @foreach($transformation_solution_library[$i]->pdfs as $upload)
                                            <p>{{$transformation_solution_library[$i]->description}}</p>
                                            <a href="{{'/download-file-pdf/'.$upload->id}}">{{$transformation_solution_library[$i]->title}}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endfor
                </div>

                <!--Business Incubation-->
                <div class="" id="incubation">
                    @for($i = 0; $i < count($business_incubation_library); $i++)
                        @if($i == 0)
                            <h6 style="color: saddlebrown;">Business Incubation</h6>
                        @endif
                        <div class="">
                            <input value="{{$business_incubation_library[$i]->id}}" disabled hidden class="document-id">
                            <div class=" ">
                                @if(isset($business_incubation_library[$i]->excel))
                                    @foreach($business_incubation_library[$i]->excel as $upload)
                                        <p>{{$business_incubation_library[$i]->description}}</p>
                                        <a href="{{'/download-file-excel/'.$upload->id}}">{{$business_incubation_library[$i]->title}}</a>
                                    @endforeach
                                @endif
                                @if(isset($business_incubation_library[$i]->pdfs))
                                    @foreach($business_incubation_library[$i]->pdfs as $upload)
                                            <p>{{$business_incubation_library[$i]->description}}</p>
                                            <a href="{{'/download-file-pdf/'.$upload->id}}">{{$business_incubation_library[$i]->title}}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endfor
                </div>

                <!--Loan Management-->
                <div class="" id="loan">
                    @for($i = 0; $i < count($loan_management_library); $i++)
                        @if($i == 0)
                            <h6 style="color: saddlebrown;">Loan Management</h6>
                        @endif
                        <div class="">
                            <input value="{{$loan_management_library[$i]->id}}" disabled hidden class="document-id">
                            <div class=" ">
                                @if(isset($loan_management_library[$i]->excel))
                                    @foreach($loan_management_library[$i]->excel as $upload)
                                        <p>{{$loan_management_library[$i]->description}}</p>
                                        <a href="{{'/download-file-excel/'.$upload->id}}">{{$loan_management_library[$i]->title}}</a>
                                    @endforeach
                                @endif
                                @if(isset($loan_management_library[$i]->pdfs))
                                    @foreach($loan_management_library[$i]->pdfs as $upload)
                                            <p>{{$loan_management_library[$i]->description}}</p>
                                            <a href="{{'/download-file-pdf/'.$upload->id}}">{{$loan_management_library[$i]->title}}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endfor
                </div>

                <!--Fund Management-->
                <div class="" id="funding">
                    @for($i = 0; $i < count($fund_management_library); $i++)
                        @if($i == 0)
                            <h6 style="color: saddlebrown;">Fund Management</h6>
                        @endif
                        <div class="">
                            <input value="{{$fund_management_library[$i]->id}}" disabled hidden class="document-id">
                            <div class=" ">
                                @if(isset($fund_management_library[$i]->excel))
                                    @foreach($fund_management_library[$i]->excel as $upload)
                                        <p>{{$fund_management_library[$i]->description}}</p>
                                        <a href="{{'/download-file-excel/'.$upload->id}}">{{$fund_management_library[$i]->title}}</a>
                                    @endforeach
                                @endif
                                @if(isset($fund_management_library[$i]->pdfs))
                                    @foreach($fund_management_library[$i]->pdfs as $upload)
                                            <p>{{$fund_management_library[$i]->description}}</p>
                                            <a href="{{'/download-file-pdf/'.$upload->id}}">{{$fund_management_library[$i]->title}}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endfor
                </div>

                <!--Private Equity Solution-->
                <div class="" id="equity">
                    @for($i = 0; $i < count($private_equity_library); $i++)
                        @if($i == 0)
                            <h6 style="color: saddlebrown;">Private Equity Solution</h6>
                        @endif
                        <div class="">
                            <input value="{{$private_equity_library[$i]->id}}" disabled hidden class="document-id">
                            <div class=" ">
                                @if(isset($private_equity_library[$i]->excel))
                                    @foreach($private_equity_library[$i]->excel as $upload)
                                        <p>{{$private_equity_library[$i]->description}}</p>
                                        <a href="{{'/download-file-excel/'.$upload->id}}">{{$private_equity_library[$i]->title}}</a>
                                    @endforeach
                                @endif
                                @if(isset($private_equity_library[$i]->pdfs))
                                    @foreach($private_equity_library[$i]->pdfs as $upload)
                                            <p>{{$private_equity_library[$i]->description}}</p>
                                            <a href="{{'/download-file-pdf/'.$upload->id}}">{{$private_equity_library[$i]->title}}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    @endfor
                </div>
            </div>
        </div>

    <style>
        hr.line1 {
            border: 1px solid #cd4300;
        }
        .material-icons{
            color: #cd4300;
        }

        .page-hero {
            height: 150px;
            overflow: hidden;
            position: relative;
            width: 100%;
        }

        .page-hero h1 {
            animation:
                    slideUp 0.75s .5s cubic-bezier(0.17,.88,.32,1.27) both,
                    fadeIn .25s .5s ease-in both;
            padding: 0 20px;
            position: absolute;
            text-align: center;
            text-shadow: 3px 3px 7px rgba(0,0,0,.61);
            top: 50%;
            width: 100%;
        }

        @keyframes slideUp {
            from {transform: translateY(200%);}
            to {transform:translateY(-50%);}
        }

        @keyframes fadeIn {
            from {opacity: 0;}
            to {opacity: 1;}
        }

    </style>


    <script>
        $(document).ready(function () {
            $('.supplyChain').on('click', function () {
                location.href = '#supply';
            });
            $('.transformation').on('click', function () {
                location.href = '#transformationSolution';
            });

            $('.privateEquity').on('click', function () {
                location.href = '#equity';
            });
            $('.businessIncubation').on('click', function () {
                location.href = '#incubation';
            });
            $('.loanManagement').on('click', function () {
                location.href = '#loan';
            });
            $('.lmanagement').on('click', function () {
                location.href = '#funding';
            });
            $('.fundManagement').on('click', function () {
                location.href = '#fund';
            });
            $('.privateEquity').on('click', function () {
                location.href = '#euity';
            });

            $('.documents').each(function (obj) {

                $(this).on('click', function () {
                    let document_id = $(this).find('.document-id').val();

                    location.href = '/home/show-uploaded-libraries/' + document_id;
                });
            });

        });

    </script>
@endsection
