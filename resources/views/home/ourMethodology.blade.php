@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/methodology/methodology.css" />
    <!--Desktop-->
    <div class="section center-align" id="ourMethoDesktop">
            <div class="col s8 center-align">
                <div class="row center-align" style="margin-top: 12vh;">
                    <h6 style="color: #cd4300;font-size: 2em;top: 5vh"><b>How we do it</b></h6>
                </div>                <p style="margin-right: 1em;">Engeli signifies the quest to go beyond that which seems impenetrable; to create a business that knows no barriers in the pursuit of excellence for the success of all stakeholders.</p>
                <div class="row" style="margin-left: 450px">
                    <p>&nbsp;<img class="materialboxed" title="Our Methodology" src="/images/MethodologyEdited.png" width="680" height="643"></p>
                </div>
            </div>
    </div>

    <!--Mobile-->
    <div class="section" id="ourMethoMobile">
        <div class="row"style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <br>
                <h6 style="font-size: 2em;color: #9d5821;"><strong>How we do it</strong></h6>
                <p ><span><strong>Engeli signifies the quest to go beyond that which seems impenetrable; to create a business that knows no barriers in the pursuit of excellence for the success of all stakeholders.</strong></span></p>
                <div class="row">
                    <p>&nbsp;<img class="materialboxed" title="Our Methodology" src="http://engeli.co.za/files/6814/4126/4433/methodology_org.jpg" alt="Our Methodology" width="300" height="300"></p>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.materialboxed').materialbox();

            $('.homepage').on('click', function () {
                location.href = "/"
            });
            $('.ourpeople').on('click', function () {
                location.href = "/home/engeliTeam"
            });
            $('.abt').on('click', function () {
                location.href = "/home/aboutpage"
            });
            $('.ourMeth').on('click', function () {
                location.href = "/home/ourMethodology"
            });
            $('.serv').on('click', function () {
                location.href = "/home/ourServices"
            });
            $('.docu').on('click', function () {
                location.href = "/home/documents"
            });
            $('.cntUS').on('click', function () {
                location.href = "/home/contactUs"
            });
        });

    </script>



@endsection
