@extends('layouts.app')

@section('content')
    <br>
    <br>
    <br>
    <br>

    <div class="row">
        <input id="id" value="{{$library->id}}" type="text" class="validate" hidden>

        <div class=""style="margin-left: 2em;margin-right: 2em;">
            <p style="font-size: 2em;">{{$library->title}}</p>
            <p>{{$library->description}}</p>
            <div class="row" style="margin-left:2em;margin-right: 2em;">
                @if(isset($library->images))
                    @foreach($library->images as $upload)
                        <div class="col 12 ">
                            <div class="card-image">
                                <img src="{{'/storage/'.$upload->image_url}}" style="width:100px; height:100px;">
                            </div>
                            <div class="card-content">
                                <p>{{$library->description}}</p>
                            </div>

                            <div class="card-action">
                                <a href="{{'/storage/'.$upload->image_url}}" download>Download Image</a>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            <div class="row" style="margin-left: 10px">
                <div class="col s4">
                    @if(isset($library->pdfs))
                        @foreach($library->pdfs as $upload)
                            <a href="{{$upload->pdf_url}}" download>Download PDF</a>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col s4">
                    @if(isset($library->videos))
                        @foreach($library->videos as $upload)
                            <iframe width="150" height="150" src="{{$upload->video_url}}" frameborder="0" allowfullscreen></iframe>
                            <a href="{{$upload->video_url}}" target="_blank">Watch this video</a>
                        @endforeach
                    @endif
                </div>
            </div>
            <br>
        </div>
            {{--<div class="row">--}}
                {{--<img src="/storage/{{isset($library->image_url)}}" style="width:250px; height:250px;">--}}
                {{--<a href="/storage/{{isset($library->image_url)}}" download="image">Download Image</a>--}}
                {{--<a href="/storage/{{isset($library->pdf_url)}}" download>Download PDF</a>--}}
                {{--<a href="/storage/{{isset($library->video_url)}}"> Watch this video</a>--}}
            {{--</div>--}}


        </div>
    </div>


@endsection