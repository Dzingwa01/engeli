@extends('layouts.admin-layout')

@section('content')
    <br>
    <br>
    <br>
    <div class="card"style="width:800px;margin: 0 auto">
        <br>
        <div class="row">
            <div class="col s4">
                <img style="width:150px;height: 100px;margin-left: 630px;" src="/images/home/logoo.png" class="engeliLogo"/>
            </div>
            <div class="col s4">
                <h6 style="margin-left: 10px;font-size: 2em;color: #9d5821"><strong>Enquiry</strong></h6>
            </div>
        </div>
        <div class="row"style="margin-left: 2em;margin-right: 4em;">
            <div class="row">
                <input id="enquiry_id" disabled hidden value="{{$enquiry->id}}">
                <div class="col s4">
                    <h6>{{$enquiry->name}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$enquiry->surname}}</h6>
                </div>
            </div>
            <div class="row">
                <div class="col s4">
                    <h6>Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;{{$enquiry->email}}</h6>
                </div>
            </div>
            <div class="row">
                <div class="col s12">
                    <h6>Cell number &nbsp;&nbsp;&nbsp;&nbsp&nbsp;&nbsp;: {{$enquiry->cell_number}}</h6>
                </div>
            </div>
            <div class="row">
                <div class="col s4">
                    <h6>Message &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </h6>
                </div>
                <div class="card col s4" style="width: 470px;height: 170px;font-family: Arial">
                    <br>
                    <p style="margin-left: 1em;margin-right: 1em;">{{$enquiry->message}}</p>
                </div>
            </div>
            <div class="input-field col m6"style="width: 500px">
                <select id="status">
                    <option value="">Select Status</option>
                    <option value="New">New</option>
                    <option value="Awaiting Response">Awaiting Response</option>
                    <option value="Resolved">Resolved</option>
                </select>
                <label>Status</label>
                <br />
            </div>
        </div>
        <div class="row" style="text-align: center;margin-left: 450px">
            <a class="waves-effect waves-light btn" id="submit-status">Submit</a>
        </div>
        <br>
    </div>
<br>

    <script>
        $(document).ready(function(){
            $('select').formSelect();

            $('#submit-status').on('click', function () {
                let formData = new FormData();
                formData.append('status', $('#status').val());
                formData.append('enquiry_id', $('#enquiry_id').val());

                let url = "{{route('status.store')}}";
                $.ajax({
                    url:url,
                    data: formData,
                    type: 'post',
                    processData: false,
                    contentType: false,
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                        //window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
    </script>

    @endsection