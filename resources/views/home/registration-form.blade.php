@extends('layouts.app')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <link rel="stylesheet" type="text/css" href="/css/youthRegistration/youthRegistration.css" />

    <div class="card z-depth-5" id="registrationDesktop" style="margin-top: 20vh;width: 700px;margin-left: 400px">
        <div class="row" style="margin-left: 3em;margin-right: 3em">
            <br>
            <h5 style="margin-left: 160px;color: grey"><b>Youth Registration form</b></h5>
            <div class="input-field col s6">
                <input id="first_name" type="text" class="validate" required>
                <label for="first_name">First Name</label>
            </div>
            <div class="input-field col s6">
                <input id="last_name" type="text" class="validate" required>
                <label for="last_name">Surname</label>
            </div>
        </div>
        <div class="row" style="margin-left: 3em;margin-right: 3em">
            <div class="input-field col l6 m6 s12">
                <select id="race" required>
                    <option value="" disabled selected>Choose Race</option>
                    <option value="Black">African</option>
                    <option value="White">White</option>
                    <option value="Indian">Indian</option>
                    <option value="Coloured">Coloured</option>
                    <option value="Other">Other</option>
                </select>
                <label for="race">Race</label>
            </div>
            <div class="input-field col l6 m6 s12">
                <select id="gender" required>
                    <option value="" disabled selected>Choose Gender</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
                <label for="gender">Gender</label>
            </div>
        </div>
        <div class="row" style="margin-left: 3em;margin-right: 3em">
            <div class="input-field col s6">
                <input id="physical_address" type="text" class="validate" required>
                <label for="physical_address">Physical Address</label>
            </div>
            <div class="input-field col s6">
                <input id="email_address" type="text" class="validate" required>
                <label for="email_address" data-error="incorrect email address">Email</label>
            </div>
        </div>
        <div class="row" style="margin-left: 3em;margin-right: 3em">
            <div class="input-field col s6">
                <input id="identity_number" type="text" class="validate" required>
                <label for="identity_number">Identity number</label>
            </div>
            <div class="input-field col s6">
                <input id="cell_number" type="text" class="validate" required>
                <label for="cell_number">Contact number</label>
            </div>
        </div>
        <div class="row" style="margin-left: 3em;margin-right: 3em">
            <div class="input-field col s6">
                <input id="highest_qualification" type="text" class="validate" required>
                <label for="highest_qualification">Highest Qualification</label>
            </div>
            <div class="row" style="margin-left: 100px">
                <div class="file-field input-field col s7">
                    <div class="btn grey">
                        <span>Attachments</span>
                        <input type="file" id="attachments">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text">
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-left: 300px;">
            <a class="waves-effect waves-light btn" style="background-color: grey"
               id="registration-submit-button-desktop">Submit</a>
        </div>
        <br>
    </div>

    <div class="card z-depth-5" id="registrationMobile" style="margin-top: 20vh;width: 300px;margin-left: 2em">
        <div class="row" style="margin-left: 3em;margin-right: 3em">
            <br>
            <h6 style="color: grey"><b>Youth Registration form</b></h6>
        </div>
        <div class="row">
            <div class="input-field col s8" style="margin-left: 3em;">
                <input id="first_name" type="text" class="validate" required>
                <label for="first_name">First Name</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s8" style="margin-right: 4em;margin-left: 3em">
                <input id="last_name" type="text" class="validate" required>
                <label for="last_name">Surname</label>
            </div>
        </div>
        <div class="row" style="margin-left: 3em;margin-right: 3em">
            <div class="input-field col l6 m6 s12">
                <select id="race" required>
                    <option value="" disabled selected>Choose Race</option>
                    <option value="Black">African</option>
                    <option value="White">White</option>
                    <option value="Indian">Indian</option>
                    <option value="Coloured">Coloured</option>
                    <option value="Other">Other</option>
                </select>
                <label for="race">Race</label>
            </div>
            <div class="input-field col l6 m6 s12">
                <select id="gender" required>
                    <option value="" disabled selected>Choose Gender</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
                <label for="gender">Gender</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s8" style="margin-left: 3em">
                <input id="email_address" type="text" class="validate" required>
                <label for="email_address" data-error="incorrect email address">Email</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s8" style="margin-left: 3em">
                <input id="physical_address" type="text" class="validate" required>
                <label for="physical_address">Physical Address</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s8" style="margin-left: 3em">
                <input id="identity_number" type="text" class="validate" required>
                <label for="identity_number">Identity number</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s8" style="margin-left: 3em">
                <input id="cell_number" type="text" class="validate" required>
                <label for="cell_number">Contact number</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s8" style="margin-left: 3em">
                <input id="highest_qualification" type="text" class="validate" required>
                <label for="highest_qualification">Highest Qualification</label>
            </div>
        </div>

        <div class="row">
            <div class="file-field input-field col s8" style="margin-left: 3em">
                <div class="btn grey">
                    <span>Attachments</span>
                    <input type="file" id="attachments">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
        </div>
        <div class="row" style="margin-left: 140px;">
            <a class="waves-effect waves-light btn" style="background-color: grey"
               id="registration-submit-button-mobile">Submit</a>
        </div>
        <br>
    </div>


    <script>
        $(document).ready(function () {
            $('#registration-submit-button-desktop').on('click', function () {
                let formData = new FormData();
                formData.append('first_name', $('#first_name').val());
                formData.append('last_name', $('#last_name').val());
                formData.append('race', $('#race').val());
                formData.append('gender', $('#gender').val());
                formData.append('physical_address', $('#physical_address').val());
                formData.append('email_address', $('#email_address').val());
                formData.append('identity_number', $('#identity_number').val());
                formData.append('cell_number', $('#cell_number').val());
                formData.append('category_id', $('#category_id').val());
                formData.append('highest_qualification', $('#highest_qualification').val());

                jQuery.each(jQuery('#attachments')[0].files, function (i, file) {
                    formData.append('attachments', file);
                });

                $.ajax({
                    url: '{{route('store-youth-registration')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });

            $('#registration-submit-button-mobile').on('click', function () {
                let formData = new FormData();
                formData.append('first_name', $('#first_name').val());
                formData.append('last_name', $('#last_name').val());
                formData.append('race', $('#race').val());
                formData.append('gender', $('#gender').val());
                formData.append('physical_address', $('#physical_address').val());
                formData.append('email_address', $('#email_address').val());
                formData.append('identity_number', $('#identity_number').val());
                formData.append('cell_number', $('#cell_number').val());
                formData.append('category_id', $('#category_id').val());
                formData.append('highest_qualification', $('#highest_qualification').val());

                jQuery.each(jQuery('#attachments')[0].files, function (i, file) {
                    formData.append('attachments', file);
                });

                $.ajax({
                    url: '{{route('store-youth-registration')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });
        });
    </script>
@endsection
