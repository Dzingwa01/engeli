@extends('layouts.app')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>
    </head>
    <link rel="stylesheet" type="text/css" href="/css/youth/youth.css" />

    <!--Youth Registration Form-->
    <div class="section" id="youthDesktop" style="margin-top: 5vh">
        <div class="row">
            <div class="parallax-container">
                <div class="parallax">
                    <img style="height: 220%;" src="/images/Engli new.jpg">
                </div>
                <h3 style="margin-left: 50px;margin-top: 40vh;color: #cd4300;text-shadow: 5px 3px 1px #404040;font-style: italic">
                    <div class="">
                        <span><b>The Youth Employment Service (YES)
                                Initiative</b></span><br>
                    </div>
                    </h3>
            </div>
            <div class="header1" style="color:white;width: 1300px;margin-left: 90px;">
                <div class="row" style="margin-right: 4em;margin-left: 5em;">
                    <div class="col s8">
                        <h5 class="vl" style="color: grey;text-transform: capitalize"><b>&nbsp;The Youth Employment Service (YES)
                                Initiative</b></h5>
                        <br>
                        <p style="color: grey">The Youth Employment Service (YES) Initiative is a business-driven initiative which
                            is breaking new ground by
                            pioneering a partnership with government and labour, in collectively tackling a national plan to build
                            economic
                            pathways for black youth. It aims to contribute towards building and strengthening our economy by
                            alleviating
                            youth unemployment, which is currently at 55.2% (Statistics South Africa, 2019). The target set is the
                            creation
                            of one million new jobs.</p>
                        <br>
                        <h5 class="vl" style="color: grey;text-transform: capitalize"><b>&nbsp;Application process</b></h5>
                        <br>
                        @foreach($registrationCategory as $category)
                            @if($category->category_name == 'Youth Candidate')
                                <div class="row">
                                    <div class="col s3">
                                        <img style="width: 150px"
                                             src="{{isset($category->category_image)?'/storage/'.$category->category_image:''}}">
                                    </div>
                                    <div class="col s5">
                                        <div class="vl">
                                            <p style="color: grey"> &nbsp;&nbsp;Click here to apply for :</p>
                                            <a style="color: #cd4300;font-style: italic" href="{{url('/youth-candidates/'.$category->id)}}">
                                                <p class="product-name">&nbsp;&nbsp;{{$category->category_name}}</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                        @foreach($registrationCategory as $category)
                            @if($category->category_name == 'Host Employer')
                                <div class="row">
                                    <div class="col s3">
                                        <img style="width: 150px"
                                             src="{{isset($category->category_image)?'/storage/'.$category->category_image:''}}">
                                    </div>
                                    <div class="col s5">
                                        <div class="vl">
                                            <p style="color: grey"> &nbsp;&nbsp;Click here to apply for :</p>
                                            <a style="color: #cd4300;font-style: italic" href="{{url('/host-employer-registration/'.$category->id)}}">
                                                <p class="product-name">&nbsp;&nbsp;{{$category->category_name}}</p>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <br>
                    <div class="col s3" style="margin-left: 4em;background-color: #cd4300">
                        <br>
                        <p style="color: white;font-style: italic">“YES recognises the critical role the youth play in shaping our
                            economy and our country,
                            yet six million young people are shut out of the economy. This is why we at YES seek out ground breaking
                            ways,
                            through innovation and technological best practice to create one million jobs for South Africa’s youth.
                            The only way to reduce inequality is to get us all into work, to build incomes and to invest
                            inclusively.
                            Now is the time to lend a hand.”</p>
                        <br>
                        <p style="color: white;font-style: italic;margin-left: 100px">~ Tashmia Ismail</p>
                        <p style="color: white;font-style: italic;margin-left: 100px">&nbsp;&nbsp;Yes4Youth CEO</p>
                    </div>
                </div>
            </div>
        </div>
        <h5 class="vl" style="color: grey;text-transform: capitalize;margin-left: 180px"><b>&nbsp;Testimonials</b></h5>
        <br>
        <h5 style="color: grey;margin-left: 180px"><b>YES = Road to Success</b></h5>
        <br>
        <iframe style="margin-left: 180px" width="700" height="400" src="https://www.youtube.com/embed/KGG5kb_TH-I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

    <div class="section" id="youthMobile" style="margin-top: 5vh;width: 360px">
        <div class="row">
            <div class="parallax-container">
                <div class="parallax">
                    <img style="height: 100%;" src="/images/Engli new.jpg">
                </div>
                <h5 style="margin-left: 50px;margin-top: 40vh;color: #cd4300;text-shadow: 5px 3px 1px #404040;font-style: italic">
                    <div class="">
                        <span><b>The Youth Employment Service (YES)
                                Initiative</b></span><br>
                    </div>
                </h5>
            </div>
            <div class="row col s12">
                <h5 class="vl" style="color: grey;text-transform: capitalize"><b>&nbsp;The Youth Employment Service (YES)
                        Initiative</b></h5>
                <br>
                <p style="color: grey">The Youth Employment Service (YES) Initiative is a business-driven initiative which
                    is breaking new ground by
                    pioneering a partnership with government and labour, in collectively tackling a national plan to build
                    economic
                    pathways for black youth. It aims to contribute towards building and strengthening our economy by
                    alleviating
                    youth unemployment, which is currently at 55.2% (Statistics South Africa, 2019). The target set is the
                    creation
                    of one million new jobs.</p>
                <br>
            </div>
            <div class="row" style="margin-left: 1em;margin-right: 1em">
                <div class="col s12" style="background-color: #cd4300">
                    <br>
                    <p style="color: white;font-style: italic">“YES recognises the critical role the youth play in shaping our
                        economy and our country,
                        yet six million young people are shut out of the economy. This is why we at YES seek out ground breaking
                        ways,
                        through innovation and technological best practice to create one million jobs for South Africa’s youth.
                        The only way to reduce inequality is to get us all into work, to build incomes and to invest
                        inclusively.
                        Now is the time to lend a hand.”</p>
                    <br>
                    <p style="color: white;font-style: italic;margin-left: 100px">~ Tashmia Ismail</p>
                    <p style="color: white;font-style: italic;margin-left: 100px">&nbsp;&nbsp;Yes4Youth CEO</p>
                </div>
            </div>
            <div class="col s12">
                <h5 class="vl" style="color: grey;text-transform: capitalize"><b>&nbsp;Application process</b></h5>

            @foreach($registrationCategory as $category)
                    @if($category->category_name == 'Youth Candidate')
                        <div class="row">
                            <div class="col s4">
                                <img style="width: 100px"
                                     src="{{isset($category->category_image)?'/storage/'.$category->category_image:''}}">
                            </div>
                            <div class="col s8">
                                <div class="vl">
                                    <p style="color: grey"> &nbsp;&nbsp;Click here to apply for :</p>
                                    <a style="color: #cd4300;font-style: italic" href="{{url('/youth-candidates/'.$category->id)}}">
                                        <p class="product-name">&nbsp;&nbsp;{{$category->category_name}}</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
                @foreach($registrationCategory as $category)
                    @if($category->category_name == 'Host Employer')
                        <div class="row">
                            <div class="col s4">
                                <img style="width: 100px"
                                     src="{{isset($category->category_image)?'/storage/'.$category->category_image:''}}">
                            </div>
                            <div class="col s8">
                                <div class="vl">
                                    <p style="color: grey"> &nbsp;&nbsp;Click here to apply for :</p>
                                    <a style="color: #cd4300;font-style: italic" href="{{url('/host-employer-registration/'.$category->id)}}">
                                        <p class="product-name">&nbsp;&nbsp;{{$category->category_name}}</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
        <h5 class="vl" style="color: grey;text-transform: capitalize;margin-left: 10px"><b>&nbsp;Testimonials</b></h5>
        <h5 style="color: grey;margin-left: 2em"><b>YES = Road to Success</b></h5>
        <br>
        <iframe style="margin-left: 15px" width="300" height="300" src="https://www.youtube.com/embed/KGG5kb_TH-I" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

        <br>
    </div>



    <style>
        @import url("https://fonts.googleapis.com/css?family=Raleway:400,400i,700");
        .changebox{
            margin: 0 5px;
            color: #cd4300;
            overflow: hidden;
            transition: .5s;
            white-space: nowrap;
        }
        .vl {
            border-left: 6px solid #cd4300;
            height: 50px;
        }
    </style>
    <script>
        $(document).ready(function () {
            var changebox = $(".changebox");

            var firstclone = changebox.children(":first").clone();
            changebox.append(firstclone);

            var fsstr = changebox.parent().css("font-size");
            fsstr = fsstr.slice(0,fsstr.indexOf("p"));
            var fs = parseInt(fsstr);

            changebox.css("height",changebox.parent().css("font-size") );
            ChangeSize(0);
            setInterval(Next,2000);

            function Next(){
                if( typeof Next.i == 'undefined' ) {
                    Next.i = 0;
                }
                Next.i++;
                if(Next.i == changebox.children("span").length){
                    Next.i = 1;
                    changebox.scrollTop(0);
                }
                changebox.animate({scrollTop: (fs*Next.i)+Next.i*5+3},500);
                setTimeout(function(){
                    ChangeSize(Next.i);
                },500);

            }

            function ChangeSize(i){
                var word = changebox.children("span").eq(i);
                var wordsize = word.css("width");
                changebox.css("width",wordsize);
            }


            $('.modal').modal();
        });

        function selectCategory(obj) {
            sessionStorage.setItem('category_id', obj.id);
            window.location = 'registration';
        }
    </script>
@endsection
