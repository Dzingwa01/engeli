@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/team/team.css">

    <div class="row center-align" style="margin-top: 12vh;">
        <h6 style="color: #cd4300;font-size: 2em;top: 5vh"><b>Engeli Team</b></h6>
    </div>

    <section id="team" style="margin-left: 2em;margin-right: 2em">
        {{--first row--}}
        <div class="card">
            <img src="/images/staff/RicardoEdited.png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Dr Ricardo Dames</h5>
                <p>BSc. and MBA; PhD: Business Incubation, 18 years experience
                    in SME development. Managed COMSEC (Incubator), ILO SIYB and Social Enterprise Master Trainer.</p>
            </div>
        </div>
        <div class="card">
            <img src="/images/NEW/Bronwynne-removebg-preview (1).png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Bronwynne Boswell</h5>
                <p>B Com: 12 years experience in Business Skills Development and B-BBEE
                    Consulting. Expertise includes development of
                    skills and compliance from B-BBEE perspective. She uses strategic solutions to ensure that
                    companies achieve
                    optimal scores for all elements of the B-BBEE.</p>
            </div>
        </div>
        <div class="card">
            <img src="/images/NEW/Wayne-removebg-preview.png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Wayne Oosthuizen</h5>
                <p>MSc (ITFM-Warwick University), MDip Tech (Elec Eng) 21 years Enterprise
                    Development experience including the
                    National Manufacturing Advisory Centre (NAMAC). Last 8 years involved in the design and
                    management of technology/innovation business incubators.</p>
            </div>
        </div>
       {{-- second row--}}
        <div class="card">
            <img src="/images/NEW/Barry-removebg-preview (1).png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Barry Wiseman</h5>
                <p>B.Com, LIAB: 27 years Corporate Banking (FNB), 8 years self employed where he
                    assisted business owners with
                    capital raising & business development services.</p>
            </div>
        </div>
        <div class="card">
            <img src="/images/NEW/Rene-removebg-preview.png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Rene Howlett</h5>
                <p>B-BBEE MDP, Member of ABP with recognition as EEP.
                    Experience as an Analyst, Technical Signatory of SANAS Accredited B-BBEE Verification Agency. Assists with cost effective B-BBEE solution, SETA Submissions, EE compliance.
                </p>
            </div>
        </div>
        <div class="card">
            <img src="/images/NEW/John-removebg-preview.png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>John Rankin</h5>
                <p>BPsych (Registered Counsellor): 14 Years’ experience in Transformation and Development work.
                    Individual, Youth, Community and Business advancement experience through bespoke solutions. Previously worked at a
                    SANAS accredited B-BBEE Verification Agency and as a B-BBEE Consultant. Currently fills role of Business Development
                    Manager and B-BBEE Specialist.</p>
            </div>
        </div>
        {{--third row--}}
        <div class="card">
            <img src="/images/NEW/Farhaad-removebg-preview (1).png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Farhaad Sain</h5>
                <p>B.Com Business Management and Economics (NMU), B.Com Economics (Hons) (NMU). Work experience abroad at a Fortune 500 Global Financial Institution.
                    Former years’ experience in local operation retail banking.
                    Experienced as an Analyst at a SANAS accredited B-BBEE Verification Agency.</p>
            </div>
        </div>
        <div class="card">
            <img src="/images/NEW/Nonthuthuzelo-removebg-preview.png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Nontuthuzelo "Pinky" Samela</h5>
                <p>Started her career at M Secure cleaning services where she completed various training that specialises in
                    Cleaning and Hygiene.  Pinky showed great potential and Engeli offered her a position as the Receptionist.  Pinky currently
                    assists a few of the group companies and manages the reception area and its associated tasks.
                </p>
            </div>
        </div>
        <div class="card">
            <img src="/images/staff/KennethEdited.png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Kenneth Jooste</h5>
                <p>B.Com, LLB: Previous experience includes Corporate Governance, Compliance and
                    Legal, HR and Financial Management.</p>
            </div>
        </div>
        {{--fourth row--}}
        <div class="card">
            <img src="/images/staff/IMG-20200110-WA0006-removebg-preview (2).png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Trevor Hayter</h5>
                <p>B Com, FIFM, CAIB: Banking, Financial Markets, Risk Management. Trevor has a
                    background in Banking, International trade and Financial
                    Services. Entrepreneur for past 25 years and founding member of various companies including
                    listed company IQUAD, in partnership with the PSG Group.</p>
            </div>
        </div>
        <div class="card">
            <img src="/images/NEW/Sivu-removebg-preview (1).png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Sivuyisiwe Msebi</h5>
                <p>Completed the Certificate in the Theory of Accounting (CTA) in 2012 and went onto KPMG to complete her learnership as a trainee accountant.
                    During her learnership at KPMG, she passed both SAICA qualifying examinations and registered with SAICA as a Chartered Accountant.
                    Prior to joining Engeli Enterprise Development, she worked at PwC as a manager in the Service Delivery Centre."
                </p>
            </div>
        </div>
        <div class="card">
            <img src="/images/NEW/Lutho-removebg-preview (1).png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Lutho Gwili</h5>
                <p>Dip (Banking), PGDip (Applied Economics), MCom (Economics). Youth Employment Services Coordinator.
                    Experienced as an SME Advisor and Business Skills Trainer
                </p>
            </div>
        </div>
        <!--fifth-->
        <div class="card">
            <img src="/images/staff/ChumaniEdited.png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Chumani Maqina</h5>
                <p>BMus, MBA: Experienced Management Professional within SME and Business
                    Development enviroments. Also successfully
                    managed own procurement solutions business.</p>
            </div>
        </div>
        <div class="card">
            <img src="/images/NEW/Liezel-removebg-preview.png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Liezel Hayter</h5>
                <p>Liezel spent 3 years with a wealth management company and 4 years with a boutique financial services
                    company, as personal assistant to the respective CEOs. Liezel then joined Engeli as an Executive Assistant,
                    but she also plays a leading role in the administration of the Supplier and
                    Enterprise Development Fund managed by Engeli, as well as assisting the Financial Director with the
                    preparation of the Group’s Financials.
                </p>
            </div>
        </div>

        <!--sixth-->
        <div class="card">
            <img src="/images/staff/AlfredEdited.png " style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Alfred da Costa</h5>
                <p>Entrepreneur, Ex CEO and appointed president of the Port Elizabeth Regional
                    Chamber of Commerce
                    and Industry. Served on a number of JSE listed entities such as Bidvest. Currently serves
                    as an independent
                    non-executive director on numerous boards.</p>
            </div>
        </div>
        <div class="card">
            <img src="/images/NEW/Donna-removebg-preview.png" style="height: 30vh;width: 50%;margin-left: 85px" alt="" />
            <div class="data">
                <h5>Donna Kramer</h5>
                <p>BA Hons Human Resources (NMU), BA Psych (NMU). 10 plus years’ experience as a Corporate HR
                    Generalist with extensive knowledge on Employment Equity, Skills Development and Recruitment & Selection.
                </p>
            </div>
        </div>
        <div class="card">
            <img src="/images/NEW/Yibanathi-removebg-preview.png" style="height: 30vh;width: 60%;margin-left: 80px" alt="" />
            <div class="data">
                <h5>Yibanathi Maqina</h5>
                <p>Dip Accountancy, BTECH Cost and management accounting, ILO SIYB trainer. Experienced
                    as an SME advisor as well as a business and personal development skills trainer. Currently fills the role of a skills development facilitator.

                </p>
            </div>
        </div>

    </section>

    <style>
        .page-hero {
            height: 140px;
            overflow: hidden;
            position: relative;
            width: 100%;
        }

        .page-hero h1 {
            animation:
                    slideUp 0.75s .5s cubic-bezier(0.17,.88,.32,1.27) both,
                    fadeIn .25s .5s ease-in both;
            padding: 0 20px;
            position: absolute;
            text-align: center;
            text-shadow: 3px 3px 7px rgba(0,0,0,.61);
            top: 50%;
            width: 100%;
        }

        @keyframes slideUp {
            from {transform: translateY(200%);}
            to {transform:translateY(-50%);}
        }

        @keyframes fadeIn {
            from {opacity: 0;}
            to {opacity: 1;}
        }

        h1{
            text-align:center;
            font-family:serif;
            font-size:3em;
            color:#cd4300
        }
        #team{
            display:flex;
            flex-wrap: wrap;
            flex-grow:3;
        }
        .card {
            width:33%;
            min-width: 250px;
            height: 400px;
            overflow: hidden;
            position: relative;
            margin:0 0.5% 1% 0;
            display:inline-block
        }
        .card:nth-of-type(3n){
            margin-right:0;
        }
        .card img {
            width: 100%;
            min-height:100%;
            transition:all 0.9s
        }
        .card:hover img{
            transform:scale(1.2,1.2)
        }
        .data {
            position: absolute;
            bottom: 5%;
            background: #fff;
            text-align:center;
            width: 90%;
            height: 80px;
            overflow: hidden;
            left: 0;
            right: 0;
            margin: 0 auto;
            padding: 10px 5%;
            box-sizing: border-box;
            opacity: 0.8;
            transition: all 0.4s
        }

        .card:hover .data {
            width: 100%;
            height: 100%;
            bottom: 0;
            opacity: 0.9;
            padding:90px 10%
        }

        .data h5 {
            margin: 0 0 5px 0;
            transition:color 0.4s
        }

        .data p {
            opacity: 0;
            text-align:justify;
            transition: all 0.2s
        }

        .card:hover .data p,.card:hover .data a {
            opacity: 1
        }

        .data a{
            color:#333;
            text-decoration:none;
            padding:20px;
            opacity:0
        }
        .data a:hover,.card:hover h5{
            color:#cd4300
        }

        @media (max-width:799px){
            .card {width:100%;margin-right:0}
        }



        /* follow me @nodws */
        .btn-twtr{
            color:#ddd;
            border:2px solid;
            border-radius:3px;
            text-decoration:none;
            display:inline-block;
            padding:5px 10px;
            font-family:sans-serif;
            font-weight:600;
            margin-top:100px;
            opacity:0.8
        }

    </style>
@endsection
