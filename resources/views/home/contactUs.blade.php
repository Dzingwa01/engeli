@extends('layouts.app')

@section('content')
    <link rel="stylesheet" type="text/css" href="/css/contact/contact.css"/>
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <br/>
    <br/>
    <br/>

    <style>
        .contact-wrap {
            padding: 40px 0;
            font-family: Arial;
        }

        .contact-wrap .contact-details {
            color: black;
            padding: 30px 25px 10px;
            margin-bottom: 40px;
        }

        .contact-wrap .contact-details h2 {
            text-transform: capitalize;
            margin: 0;
            font: normal 35px Arial;
            color: #333;
        }

        .contact-wrap .contact-other i, .contact-wrap .contact-other a {
            display: inline-block;
            margin-right: 10px;
            color: black;
        }

        .contact-wrap .contact-other.img {
            text-align: center;
        }

        .contact-wrap .contact-other.img img {
            max-width: 55%;
        }

        .contact-wrap .contact-other p {
            display: inline-block;
            margin: 0 0 10px;
            font-size: 15px
        }

        .contact-form button {
            background: #ff4081;
            border: none;
            border-radius: 2px;
            color: #fff;
            height: 36px;
            padding: 0px 15px;
            font-size: 14px;
            font-weight: 500;
            text-transform: uppercase;
            text-align: center;
            line-height: 36px;
            vertical-align: middle;
            font-family: Arial;
            margin-top: 20px;
        }

        .contact-form button i {
            color: #fff;
        }

        .contact-form i {
            color: #e97a7f;
        }

        .contact-form button[disabled] {
            background: #ddd;
            color: #757575;
        }

        #map {
            width: 100%;
            height: 400px;
        }

        @media (max-width: 600px) {
            #map {
                height: 200px;
                margin-bottom: 20px;
            }
        }
    </style>
    <br>
    <!--Desktop-->
    <div class="contact-wrap desktop">
        <div class="container">
            <div class="card contact-details clearfix z-depth-4 hoverable">
                <h2 style="font-family: Arial;font-size: 2em;color: saddlebrown"><strong>Our Office</strong></h2>
                <div class="row">
                    <div class="col s12 m5">
                        <div class="contact-other">
                            <p>7 Oakworth Road, South End, Port Elizabeth, 6001.</p>
                        </div>
                        <div class="contact-other">
                            <i class="material-icons dp48">call<span class="hide">hidden</span></i>
                            <p>0861 ENGELI or 0861 364 354</p>
                        </div>
                        <div class="contact-other">
                            <p class="black-text text-lighten-4">Follow us on Linkedin : </p>
                            <a href="https://www.linkedin.com/company/engeli-enterprise-development/?viewAsMember=true" target="_blank" class="white-text" href="https://" style="margin-right: 10px"><i class="fab fa-linkedin fa-2x"></i></a>
                        </div>
                    </div>
                    <div class="col s12 m4">
                        <div class="contact-other">
                            <i class="material-icons dp48">local_post_office<span class="hide">hidden</span></i>
                            <a href="">info@engeli.co.za</a>
                        </div>

                    </div>
                    <div class="col m3 hide-on-small-only">
                        <div class="contact-other img">
                            <img src="https://image.ibb.co/nHtaQv/placeholder.png" alt=""/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col m6 push-m6 s12">
                    <div id="HTMLBlock410" class="HTMLBlock">
                        <iframe style="width:520px;height:540px;"
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13235.449931524925!2d25.63019166828253!3d-33.97037390865128!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1e7ad29062e8535f%3A0x5e1e63b2b2e72c48!2s7%20Oakworth%20Rd%2C%20South%20End%2C%20Port%20Elizabeth%2C%206001!5e0!3m2!1sen!2sza!4v1575629299897!5m2!1sen!2sza"
                                width="440" height="300" frameborder="0" style="border:0" allowfullscreen="">
                        </iframe>
                    </div>
                </div>

                <!--Contact Us Form-->
                <div class="card col m6 pull-m6 s12 z-depth-4 hoverable" style="margin-top: 0">
                    <h2 style="font-family: Arial;font-size: 2em;color: saddlebrown;margin-left: 5em;"><strong>Contact
                            Us</strong></h2>
                    <div id="contact-desktop">
                        <div class="input-field">
                            <i class="material-icons prefix">person</i>
                            <input id="name" type="text" class="validate" required>
                            <label for="name" data-error="incomplete">First Name</label>
                        </div>
                        <div class="input-field">
                            <i class="material-icons prefix">person</i>
                            <input id="surname" type="text" class="validate" required>
                            <label for="surname" data-error="incomplete">Surname</label>
                        </div>
                        <div class="input-field">
                            <i class="material-icons prefix">phone</i>
                            <input id="cell_number" type="text" class="validate" required>
                            <label for="cell_number" data-error="incomplete">Cell number</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">email</i>
                            <input id="email" type="email" class="validate" required>
                            <label for="email" data-error="incorrect email address">Email</label>
                        </div>

                        <div class="input-field">
                            <i class="material-icons prefix">mode_edit</i>
                            <textarea id="message" class="materialize-textarea" required></textarea>
                            <label for="message" data-error="please write your message">Message</label>
                        </div>

                        <div>
                            <div class="row" style="margin-left: 40px;">
                                <a class="waves-effect waves-light btn" style="background-color: #5a6268"
                                   id="form-submit-button">Submit</a>
                            </div>
                        </div>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


    <!--Mobile-->
    <div class="contact-form-map mobile">
        <br>
        <br>
        <div class="row" style="margin: 0 auto;">
           <div class="row"style="margin-left: 2em">
               <h6 style="color: saddlebrown;font-size: 2em;"><b>Our Offices</b></h6>
               <p>Lower Ground Floor, Bay Suites, South End, Port Elizabeth.</p>
               <p>0861 ENGELI or 0861 364 354</p>
               <a href="mailto:info@engeli.co.za">info@engeli.co.za</a>
           </div>
            <div class="col m6 push-m6 s12" style="margin-left: 1em;margin-right: 2em;">
                <div id="HTMLBlock410" class="HTMLBlock">
                    <iframe style="width:310px;"
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3308.908521344357!2d25.62724661521281!3d-33.969190680628586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1e7ad2904dd018ff%3A0xdbe8085bd20c11dd!2s1+Humewood+Rd%2C+South+End%2C+Port+Elizabeth%2C+6001!5e0!3m2!1sen!2sza!4v1502865449848"
                            width="100" height="300" frameborder="0" style="border:0" allowfullscreen="">
                    </iframe>
                </div>
            </div>

            <br>
            <br>
            <br>
            <br>
            <div class="card col m6 pull-m6 s10" style="margin-left: 2em;margin-right: 1em;">
                <br>
                <form class="contact-form">
                    <div class="input-field">
                        <i class="material-icons prefix">person</i>
                        <input id="name" type="text" class="validate">
                        <label for="name" data-error="incomplete">First Name</label>
                    </div>
                    <div class="input-field">
                        <i class="material-icons prefix">person</i>
                        <input id="surname" type="text" class="validate">
                        <label for="surname" data-error="incomplete">Surname</label>
                    </div>
                    <div class="input-field">
                        <i class="material-icons prefix">phone</i>
                        <input id="cell_number" type="text" class="validate">
                        <label for="cell_number" data-error="incomplete">Cell number</label>
                    </div>

                    <div class="input-field">
                        <i class="material-icons prefix">email</i>
                        <input id="email" type="email" class="validate">
                        <label for="email" data-error="incorrect email address">Email</label>
                    </div>

                    <div class="input-field">
                        <i class="material-icons prefix">mode_edit</i>
                        <textarea id="message" class="materialize-textarea"></textarea>
                        <label for="message" data-error="please write your message">Message</label>
                    </div>

                    <div class="input-wrap">
                        <a class="waves-effect waves-light btn" id="form-submit-button">Submit</a>
                    </div>
                    <br>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('#form-submit-button').on('click', function () {

                let formData = new FormData();

                formData.append('name', $('#name').val());
                formData.append('email', $('#email').val());
                formData.append('message', $('#message').val());
                formData.append('surname', $('#surname').val());
                formData.append('cell_number', $('#cell_number').val());

                // if($('#name').val() === '') {
                //     alert('Name is Required')
                // }


                console.log(formData);
                let url = "{{route('enquiry.store')}}";
                $.ajax({
                    url: url,
                    data: formData,
                    type: 'post',
                    processData: false,
                    contentType: false,
                    headers: {'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')},

                    success: function (response, a, b) {
                        alert(response.message);
                        window.location.reload();
                    },

                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });
            });


            $('.homepage').on('click', function () {
                location.href = "/"
            });
            $('.ourpeople').on('click', function () {
                location.href = "/home/engeliTeam"
            });
            $('.abt').on('click', function () {
                location.href = "/home/aboutpage"
            });
            $('.ourMeth').on('click', function () {
                location.href = "/home/ourMethodology"
            });
            $('.serv').on('click', function () {
                location.href = "/home/ourServices"
            });
            $('.docu').on('click', function () {
                location.href = "/home/documents"
            });
            $('.cntUS').on('click', function () {
                location.href = "/home/contactUs"
            });
            $('.datepicker').datepicker();
        });

    </script>



@endsection
