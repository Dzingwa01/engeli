<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <link rel="icon" type="image/png" href="/images/home/logoo.png">
    <meta charset="utf-8">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Engeli Enterprise Development</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
          integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css"/>
    {{--<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>--}}

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
</head>
<body>
<ul id="dropdown1" class="dropdown-content">
    <li><a class="nav-tab" href="/home/aboutpage"style="color: black">About</a></li>
    <li><a class="nav-tab" href="/home/engeliTeam"style="color:black">Team</a></li>
</ul>

<ul id="dropdown" class="dropdown-content">
    <li><a class="nav-tab" href="/home/aboutpage"style="color: black">About</a></li>
    <li><a class="nav-tab" href="/home/engeliTeam"style="color:black">Team</a></li>
</ul>

<ul id="dropdown2" class="dropdown-content">
    <li><a class="nav-tab" href="{{url('login')}}">Login</a></li>

</ul>
<ul id="dropdown3" class="dropdown-content">
    <li><a class="nav-tab" href="{{url('login')}}">Login</a></li>

</ul>
<div id="app">
    <div class="navbar-fixed">
        <nav class="" style="height: 150px;background-color: #404040">
            <div class="nav-wrapper">
                <a  href="/" class="brand-logo">
                    <img style="width:200px;height: 150px;" src="/images/home/logoo.png" class="engeliLogo"/>
                </a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down" style="text-align:center;margin-top: 6vh">
                    <li><a class="txt" href="/" style="font-size: 1.2em; font-weight: bold;color: white">Home</a></li>
                    <li><a class="txt" style="font-size: 1.2em; font-weight: bold;color: white">Who we are</a>
                        <ul>
                            <li><a class="txt" href="/home/aboutpage" style="font-size: 1em; font-weight: bold;color: white">About us</a></li>
                            <li><a class="txt" href="/home/engeliTeam" style="font-size: 1em; font-weight: bold;color: white">Our team</a></li>
                        </ul>
                    </li>
                    <li><a class="txt" href="/home/ourServices" style="font-size: 1.2em; font-weight: bold;color: white">What we do</a></li>
                    <li><a class="txt" href="/home/ourMethodology" style="font-size: 1.2em; font-weight: bold;color: white">How we do it</a></li>
                    <li><a class="txt" href="/home/documents" style="font-size: 1.2em; font-weight: bold;color: white">Library</a></li>
                    <li><a class="txt" href="/home/contactUs" style="font-size: 1.2em; font-weight: bold;color: white">Get in touch</a></li>
                    <li><a class="txt" href="/home/Yes-4-Youth" style="font-size: 1.2em; font-weight: bold;color: white">Yes-4-Youth</a></li>
                    <li><a class="txt" style="font-size: 1.2em;font-weight: bold;color: white">Account</a>
                        <ul>
                            <li><a class="txt" href="{{url('login')}}" style="font-size: 1em; font-weight: bold;color: white">Login</a></li>
                        </ul>
                    </li>
                </ul>

               {{-- <marquee style="color: grey"><span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;||&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>Challenges</b></span>&nbsp;&nbsp;&nbsp; • &nbsp;&nbsp;&nbsp;<span><b>Business Support</b></span>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>World Class</b>&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;<b>Transformation</b>&nbsp;&nbsp;•&nbsp;&nbsp;<b>Advance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Growth</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Empower</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Enhance</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Support</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Opportunity</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Innovation</b>&nbsp&nbsp;•&nbsp;&nbsp;<b>Best Practice</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Change</b>&nbsp&nbsp; •&nbsp;&nbsp;<b>Sustainability</b>

                </marquee>--}}
            </div>

            <div class="row grey darken-2 " style="height: 8px;">
            </div>
        </nav>

    </div>
    <ul class="sidenav" id="mobile-demo">
        <li><a class="nav-tab" href="/"><i class="material-icons left">business</i>Home</a></li>
        <li><a class="nav-tab" href="/home/aboutpage"><i class="material-icons left">people</i>About us</a></li>
        <li><a class="nav-tab" href="/home/engeliTeam"><i class="material-icons left">people</i>Our Team</a></li>


        {{--<li><a class="dropdown-trigger-cus nav-tab" href="#!" data-target="dropdown1"><i class="material-icons left">lightbulb_outline</i>Who We Are<i--}}
                        {{--class="material-icons right">arrow_drop_down</i></a></li>--}}
        <li><a class="nav-tab" href="/home/ourMethodology"><i class="material-icons left">work</i>How we do it</a></li>
        <li><a class="nav-tab" href="/home/ourServices"><i class="material-icons left">more</i>What we do</a></li>
        <li><a class="nav-tab" href="/home/documents"><i class="material-icons left">attachments</i>Library</a></li>
        <li><a class="nav-tab" href="/home/contactUs"><i class="material-icons left">local_phone</i>Get in touch</a></li>
        <li><a class="nav-tab" href="/home/Yes-4-Youth"><i class="material-icons left">people</i>Yes-4-Youth</a></li>
        <li><a class="nav-tab" href="{{url('login')}}"><i class="material-icons left">screen_lock_portrait</i>Account</a></li>

    </ul>
    <div class="container-fluid">
        @yield('content')
    </div>


    <footer class="page-footer" style="background: #cd4300">
        <div class="container-fluid">
            <div class="row"style="margin-left: 1em;">
                <div class="col l6 s12"style="margin-left: 5px">
                    <h5  class="white-text">Contacts</h5>
                    <p class="grey-text text-lighten-4">For general information about Engeli Enterprise Development,
                        please contact us at:</p>
                    <ul>
                        <div class="row">
                            <div class="col s12">
                                <li><a style="color:white!important;" href=""><i class="material-icons">contact_mail</i> info@engeli.co.za</a></li>
                            </div>
                            <div class="col s12">
                                <li><a style="color:white!important;" href="#"><i class="material-icons">contact_phone</i> 0861 364 354</a></li>
                            </div>
                            <div class="col s12">
                                <li><a style="color:white!important;" href="#"><i class="material-icons">print</i> 0861 55 55 33</a></li>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col s8">
                                <li><a style="color:white!important;" href="#"><i class="material-icons">business</i>7 Oakworth Road, South End, Port Elizabeth, 6001 .</a></li>
                            </div>
                        </div>
                    </ul>
                </div>
                    <div class="col 4"style="margin-left: 2px">
                        <p class="white-text text-lighten-4">Follow us on Linkedin : </p>
                        <a href="https://www.linkedin.com/company/engeli-enterprise-development/?viewAsMember=true" target="_blank" class="white-text" href="https://" style="margin-left: 1em;"><i class="fab fa-linkedin fa-2x"></i></a>
                    </div>
                        <img style="margin-left: 15em" src="/images/logo-removebg-preview.png">

            </div>
        </div>
    </footer>
</div>

<style>

    .txt:hover {
        text-decoration: underline;
    }

    nav a:hover{
        /*background:#223433;*/
        /*color:#f0f1f5;*/
    }

    nav ul{
        list-style: none;
    }

    nav ul li{
        float:left;
        width:160px;
        height:40px;
        line-height: 40px;
    }

    nav ul ul li{
        position:relative;
        display:none;
    }

    nav ul ul ul{
        display:none;
    }

    nav ul li:hover ul li{
        display: block;
        animation: navmenu 500ms forwards;
    }
    @keyframes navmenu{
        0%{
            opacity:0;
            top:10px;
        }
        100%{
            opacity:1;
            top:0px;
        }
    }

    nav ul ul li:hover ul{
        display:block;
        position:absolute;
        width:140px;
        left:140px;
        top:0px;
    }


    .nav-tab {
        font-weight: bolder !important;
        margin : 0 !important;

    }
    @media screen and (max-width: 767px) {
        nav a{
            color:black!important;
        }
        .engeliLogo{
            height: 100px!important;
            margin-left: 7em;
        }
    }


</style>
{{--<script--}}
{{--src="https://code.jquery.com/jquery-3.3.1.min.js"--}}
{{--integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="--}}
{{--crossorigin="anonymous"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>
    $(document).ready(function () {
        $('.tooltipped').tooltip();

//        M.AutoInit();
        $('.parallax').parallax();
        $('.sidenav').sidenav();
        $('.dropdown-trigger-cus').dropdown();
        $('.dropdown-trigger-cus-3').dropdown();
        $('.dropdown-trigger-c').dropdown();
        $('select').formSelect();
        $('.carousel').carousel({
            fullWidth: true,
            indicators: true
        });


    });
</script>
@stack('custom-scripts')
</body>
</html>
