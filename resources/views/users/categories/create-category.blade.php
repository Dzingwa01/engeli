@extends('layouts.admin-layout')

@section('content')

    <div class="card" style="margin-top: 10vh;width: 700px;margin-left: 400px">
        <br>
        <h6 style="margin-left: 15em">Create Category</h6>
        <div class="row" style="margin-left: 7em">
            <div class="input-field col s10">
                <input id="category_name" type="text" class="validate">
                <label for="category_name">Category name</label>
            </div>
        </div>
        <div class="row" style="margin-left: 100px">
            <div class="file-field input-field col s10">
                <div class="btn">
                    <span>Category Image</span>
                    <input type="file" id="category_image">
                </div>
                <div class="file-path-wrapper">
                    <input class="file-path validate" type="text">
                </div>
            </div>
        </div>
        <div class="row" style="margin-left: 600px;">
            <div class="col s4">
                <a class="waves-effect waves-light btn" id="question-category-submit-button">Save</a>
            </div>
        </div>
        <br>
    </div>

    <script>
        $(document).ready(function(){
            $('#question-category-submit-button').on('click',function () {

                let formData = new FormData();
                formData.append('category_name', $('#category_name').val());
                jQuery.each(jQuery('#category_image')[0].files, function (i, file) {
                    formData.append('category_image', file);
                });

                $.ajax({
                    url: '{{route('store-category')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        console.log("success", response);
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        console.log("error", message);
                        let errors = response.responseJSON.errors;

                        for (var error in   errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);

                    }
                });
            });
        });
    </script>

@endsection
