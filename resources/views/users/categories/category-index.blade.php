@extends('layouts.admin-layout')

@section('content')
    <br>
    <div class="container-fluid">
        <div class="row">
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Question categories</h6>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="registration_categories-table">
                    <thead>
                    <tr>
                        <th>Category Name</th>
                        <th>Category Image</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large teal tooltipped btn modal-trigger" data-position="left" data-tooltip="Upload" href="{{url('create-category')}}">
            <i class="large material-icons">add</i>
        </a>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();
            $(function () {
                $('#registration_categories-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-categories')}}',
                    columns: [
                        {data: 'category_name', name: 'category_name'},
                        {data: 'category_image', name: 'category_image'},

                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="registration_categories-table_length"]').css("display","inline");
            });
        });
        function confirm_delete(obj){
            var r = confirm("Are you sure want to delete this category?");
            if (r == true) {
                $.get('/destroy-category/'+obj.id,function(data,status){
                    console.log('Data',data);
                    console.log('Status',status);
                    if(status=='success'){
                        alert(data.message);
                        window.location.reload();
                    }

                });
            } else {
                alert('Delete action cancelled');
            }

        }

    </script>

    @endsection
