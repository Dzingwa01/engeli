@extends('layouts.admin-layout')

@section('content')
    <div class="card" style="margin-top: 10vh;width: 700px;margin-left: 400px">
        <form class="col s12 " id="edit-category" method="post">
            <br>
            @csrf
            <input value="{{$registrationCategory->id}}" id="question_category_id" hidden>
            <br>
            <h6 style="margin-left: 15em">Update Category</h6>
            <div class="row" style="margin-left: 7em">
                <div class="input-field col s10">
                    <input id="category_name" type="text" value="{{$registrationCategory->category_name}}" class="validate">
                    <label for="category_name">Category name</label>
                </div>
            </div>
            <div class="row" style="margin-left: 100px">
                <div class="file-field input-field col s10">
                    <div class="btn">
                        <span>Category Image</span>
                        <input type="file" id="category_image">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" value="{{isset($registrationCategory->category_image)?$registrationCategory->category_image:''}}" type="text">
                    </div>
                </div>
            </div>
            <div class="row" style="margin-left: 600px;">
                <div class="col s4">
                    <a class="waves-effect waves-light btn" id="question-category-submit-button">Save</a>
                </div>
            </div>
            <br>
        </form>
    </div>

    <script>
        $(document).ready(function () {
            $('#question-category-submit-button').on('click',function () {
                let formData = new FormData();

                formData.append('category_id', $('#category_id').val());
                formData.append('category_name', $('#category_name').val());
                jQuery.each(jQuery('#category_image')[0].files, function (i, file) {
                    formData.append('category_image', file);
                });
                console.log(formData);

                let url = '/update-category/'+ '{{$registrationCategory->id}}';
                $.ajax({
                    url: url,
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        alert(response.message);
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        alert(message);
                    }
                });

            });        });
    </script>

@endsection
