@extends('layouts.admin-layout')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <br>
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Host Employees Candidates</h6>
        </div>
        <div class="row center">
            <a class="btn btn-success purple" id="btnExport">Export to Excel</a>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="host_employee_registration_forms-table">
                    <thead>
                    <tr>
                        <th>Company Name</th>
                        <th>Company registration No</th>
                        <th>Contact Name</th>
                        <th>Contact number</th>
                        <th>Contact Email</th>
                        <th>Physical Address</th>
                        <th>Industry</th>
                        <th>No of Candidates</th>
                        <th>Message</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();
            $(function () {
                let table = $('#host_employee_registration_forms-table').DataTable({
                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-host-employees')}}',
                    columns: [
                        {data: 'company_name', name: 'company_name'},
                        {data: 'company_reg_number', name: 'company_reg_number'},
                        {data: 'contact_name', name: 'contact_name'},
                        {data: 'contact_number', name: 'contact_number'},
                        {data: 'contact_email', name: 'contact_email'},
                        {data: 'physical_address', name: 'physical_address'},
                        {data: 'industry', name: 'industry'},
                        {data: 'number_of_candidates', name: 'number_of_candidates'},
                        {data: 'message', name: 'message'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="host_employee_registration_forms-table_length"]').css("display","inline");
                $("#btnExport").click(function(e)
                {
                    table.page.len( -1 ).draw();
                    window.open('data:application/vnd.ms-excel,' +
                        encodeURIComponent($('#host_employee_registration_forms-table').parent().html()));
                    setTimeout(function(){
                        table.page.len(10).draw();
                    }, 1000)

                });
            });
        });

    </script>
@endsection
