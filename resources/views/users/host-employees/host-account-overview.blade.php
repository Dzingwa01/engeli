@extends('layouts.admin-layout')

@section('content')

    <div class="card" style="margin-top: 15vh;width: 800px;margin-left: 350px">
        <br>
        <h4 style="margin-left: 180px">Account Overview of {{$hostEmployeeRegistrationForm->company_name}}</h4>
        <br>
        <div class="row" style="margin-left: 5em">
            <p>Company name :{{$hostEmployeeRegistrationForm->company_name}}</p>
            <p>Company Registration number : {{$hostEmployeeRegistrationForm->company_reg_number}}</p>
            <p>Physical address : {{$hostEmployeeRegistrationForm->physical_address}}</p>
            <p>Contact name : {{$hostEmployeeRegistrationForm->contact_name}}</p>
            <p>Contact email : {{$hostEmployeeRegistrationForm->contact_email}}</p>
            <p>Contact number : {{$hostEmployeeRegistrationForm->contact_number}}</p>
            <p>No of Candidates : {{$hostEmployeeRegistrationForm->number_of_candidates}}</p>
            <p>Industry : {{$hostEmployeeRegistrationForm->industry}}</p>
            <p>Message : {{$hostEmployeeRegistrationForm->message}}</p>
            <br>
            <br>
        </div>
    </div>

@endsection
