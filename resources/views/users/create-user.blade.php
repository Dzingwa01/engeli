@extends('layouts.admin-layout')

@section('content')
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
    </head>
    <!--Desktop-->
    <br>
    <br>
    <br>
    <div class="section"id="createDesktop">
        <div class="card hoverable center" style="width: 800px; margin-left: 350px">
            <form class="col s12" id="add-user" method="post">
                <br />
                <br />
                <h4 style="font-size: 2em;font-family: Arial;">Add User</h4>
                @csrf
                <div class="row"style="margin-left: 95px">
                    <div class="input-field col m5">
                        <input id="name" type="text" class="validate">
                        <label for="name">Name</label>
                    </div>
                    <div class="input-field col m5">
                        <input id="surname" type="text" class="validate">
                        <label for="surname">Surname</label>
                    </div>
                </div>
                <div class="row"style=" margin-left: 95px">
                    <div class="input-field col m5">
                        <input id="email" type="email" class="validate">
                        <label for="email">Email</label>
                    </div>
                    <div class="input-field col m5">
                        <input id="contact_number" type="tel" class="validate">
                        <label for="contact_number">Contact Number</label>
                    </div>
                </div>
                <div class="row"style="margin-left: 95px">
                    <div class="input-field col m5">
                        <select id="role_id">
                            @foreach($roles as $role)
                                <option value="{{$role->id}}">{{$role->display_name}}</option>
                            @endforeach
                        </select>
                        <label>System Role</label>
                    </div>
                    <div class="input-field col m5">
                        <textarea id="address" class="materialize-textarea"></textarea>
                        <label for="address">Address</label>
                    </div>
                </div>

                <a href="#!" class="modal-close waves-effect waves-green btn" >Cancel<i class="material-icons right">close</i> </a>

                <button class="btn waves-effect waves-light" style="margin-left:300px;" id="save-user" name="action">Submit
                    <i class="material-icons right">send</i>
                </button>
            </form>
            <br />
            <br />
        </div>

        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.tooltipped').tooltip();
            $('select').formSelect();

            $('#add-user').on('submit', function (e) {

                e.preventDefault();
                let formData = new FormData();
                formData.append('name', $('#name').val());
                formData.append('surname', $('#surname').val());
                formData.append('email', $('#email').val());
                formData.append('contact_number', $('#contact_number').val());
                formData.append('role_id', $('#role_id').val());
                formData.append('address', $('#address').val());
                console.log(formData);

                $.ajax({
                    url: '{{route('add-users')}}',
                    processData: false,
                    contentType: false,
                    data: formData,
                    type: 'post',
                    headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') },

                    success: function (response, a, b) {
                        console.log("success", response);
                        alert(response.message);
                        window.location.reload();
                    },
                    error: function (response) {
                        console.log("error", response);
                        let message = response.responseJSON.message;
                        console.log("error", message);
                        let errors = response.responseJSON.errors;

                        for (var error in   errors) {
                            console.log("error", error)
                            if (errors.hasOwnProperty(error)) {
                                message += errors[error] + "\n";
                            }
                        }
                        alert(message);

                    }
                });
            });
        });
    </script>

    @endsection