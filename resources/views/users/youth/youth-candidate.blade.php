@extends('layouts.admin-layout')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <br>
            <h6 style="text-transform:uppercase;text-align: center;font-weight: bolder;margin-top:2em;">Youth Candidates</h6>
        </div>
        <div class="row center">
            <a class="btn btn-success purple" id="btnExport">Export to Excel</a>
        </div>
        <div class="row" style="margin-left: 2em;margin-right: 2em;">
            <div class="col s12">
                <table class="table table-bordered" style="width: 100%!important;" id="youth_registration_forms-table">
                    <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email Address</th>
                        <th>Contact number</th>
                        <th>Highest Qualification</th>
                        <th>Physical Address</th>
                        <th>Race</th>
                        <th>Gender</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('select').formSelect();
            $(function () {
                 let table = $('#youth_registration_forms-table').DataTable({

                    processing: true,
                    serverSide: true,
                    paging: true,
                    responsive: true,
                    scrollX: 640,
                    ajax: '{{route('get-youth-candidates')}}',
                    columns: [
                        {data: 'first_name', name: 'first_name'},
                        {data: 'last_name', name: 'last_name'},
                        {data: 'email_address', name: 'email_address'},
                        {data: 'cell_number', name: 'cell_number'},
                        {data: 'highest_qualification', name: 'highest_qualification'},
                        {data: 'physical_address', name: 'physical_address'},
                        {data: 'race', name: 'race'},
                        {data: 'gender', name: 'gender'},
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
                $('select[name="youth_registration_forms-table_length"]').css("display","inline");

                $("#btnExport").click(function(e)
                {
                    table.page.len( -1 ).draw();
                    window.open('data:application/vnd.ms-excel,' +
                        encodeURIComponent($('#youth_registration_forms-table').parent().html()));
                    setTimeout(function(){
                        table.page.len(10).draw();
                    }, 1000)

                });
            });
        });

    </script>
    @endsection
