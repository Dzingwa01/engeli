@extends('layouts.admin-layout')

@section('content')

    <div class="card" style="margin-top: 15vh;width: 800px;margin-left: 350px">
        <br>
        <h4 style="margin-left: 180px">Account Overview of {{$youthRegistrationForm->first_name}}</h4>
        <br>
        <div class="row" style="margin-left: 5em">
            <p>Name & Surname :{{$youthRegistrationForm->first_name}} {{$youthRegistrationForm->last_name}}</p>
            <p>Race : {{$youthRegistrationForm->race}}</p>
            <p>Gender : {{$youthRegistrationForm->gender}}</p>
            <p>ID Number : {{$youthRegistrationForm->identity_number}}</p>
            <p>Physical Address : {{$youthRegistrationForm->physical_address}}</p>
            <p>Email Address : {{$youthRegistrationForm->email_address}}</p>
            <p>Cell number : {{$youthRegistrationForm->cell_number}}</p>
            <p>Highest Qualification : {{$youthRegistrationForm->highest_qualification}}</p>

            @if($youthRegistrationForm->attachments != null)
                <b>Documents Attached : </b><a href="{{'/download-attachments/'.$youthRegistrationForm->id}}" >{{$youthRegistrationForm->first_name}}</a>
            @elseif(isset($youthRegistrationForm->attachments))
                <b>Attachments:</b> Click <a href="{{$youthRegistrationForm->attachments}}"
                                             target="_blank">here</a>
            @else
                <b>Documents Attached:</b> Not uploaded yet.
            @endif
            <br>
            <br>
        </div>
    </div>

    @endsection
