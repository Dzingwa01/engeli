@extends('layouts.admin-layout')

@section('content')
<div class="container">
    <div class="row justify-content-center">
            <br>
            <br>
            <h2 style="font-size: 2em;margin-left: 400px;color: #9d5821"><strong>DASHBOARD</strong></h2>
            <a href="/home/enquiry-index"><div class="card col s4 hoverable z-depth-3"style="width: 400px;border-radius: 10px;height: 200px">
                    <br>
                    <br>
                <h4 style="margin-left: 65px;color: black">Total Enquiries: {{$enquiry_count}}</h4>
            </div> </a>
        <div class="col s1">
        </div>
        <a href="/home/library-index"><div class="card col s4 hoverable z-depth-3"style="width: 400px;border-radius: 10px;height: 200px;">
                <br>
                <br>
                <h4 style="margin-left: 70px;color: black">Library uploads</h4>
            </div> </a>

    </div>
</div>
@endsection
