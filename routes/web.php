<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

//Yes for youth programme

Route::get('/home/Yes-4-Youth','HomeController@youthProgramme');
Route::get('create-category','UserController@createCategory');
Route::post('store-category','UserController@storeRegistrationCategory')->name('store-category');
Route::get('category-index','UserController@categoryIndex');
Route::get('get-category', 'UserController@getCategory')->name('get-categories');
Route::get('/youth-candidates/{registrationCategory}','HomeController@registration');
Route::post('store-youth-registration','HomeController@storeYouthForm')->name('store-youth-registration');
Route::post('store-host-employee-registration','HomeController@storeHostEmployeeForm')->name('store-host-employee-registration');
Route::get('video-uploads','UserController@createVideoUploads');
Route::post('store-videos','UserController@storeVideoUploads')->name('store-video');
Route::get('youth-index','UserController@youthCanditateIndex');
Route::get('get-youth-candidates','UserController@getYouthCandidates')->name('get-youth-candidates');
Route::get('host-employee-index','UserController@hostEmployeesCanditateIndex');
Route::get('get-host-employees','UserController@getHostEmployeeCandidates')->name('get-host-employees');
Route::get('/destroy-host-employees/{id}','UserController@deleteHostEmployeeEmployeeCaandidates');
Route::get('/destroy-youth-candidate/{id}','UserController@deleteYouthCaandidates');
Route::get('/destroy-category/{id}','UserController@deleteCategory');
Route::get('/host-employer-registration/{registrationCategory}','HomeController@hostEmployeeRegistration');
Route::get('/edit-category/{registrationCategory}','UserController@editCategogy');
Route::post('/update-category/{registrationCategory}','UserController@updateCategory');
Route::get('/youth-account-overview/{youthRegistrationForm}','UserController@youthAccountOverview');
Route::get('/download-attachments/{id}','UserController@downloadAttachments');
Route::get('/host-employee-account-overview/{hostEmployeeRegistrationForm}','UserController@hostOverview');


//Contact
Route::get('/home/contact','ContactController@show');
Route::post('/contact','ContactController@mailToAdmin');



//Downloads
Route::get('/download-file-excel/{id}','HomeController@downloadFileExcel');
Route::get('/download-file-pdf/{id}','HomeController@downloadFilePdf');

Route::get('/home/viewPropella','HomeController@propView');
Route::get('/home/personal','HomeController@personal');
Route::get('/home/aboutpage','HomeController@about');
Route::get('/home/engeliTeam','HomeController@team');
Route::get('/home/ourMethodology','HomeController@methodology');
Route::get('/home/ourServices','HomeController@services');
Route::get('/home/documents','HomeController@documentsInfo');
Route::get('/home/contactUs','HomeController@contact');

//Enquiry
Route::get('/home/enquiry-index','EnquiryController@enquiryIndex');
Route::get('/home', 'HomeController@index')->name('home');
Auth::routes(['verify' => true]);

Route::get('/home/enquiry-show/{enquiry}','EnquiryController@enquiryShow');
Route::get('/enquiry/delete/{id}','EnquiryController@destroyEnquiry');

Route::post('status-store','HomeController@storeStatus')->name('status.store');

Route::post('enquiry-store','HomeController@storeEnquiries')->name('enquiry.store');
Route::get('/home/enquiry-index-get','EnquiryController@getEnquiry')->name('get-enquiry');

//Library Admin
Route::get('/home/library-uploads','LibraryController@libraryUpload');
Route::post('library-store','LibraryController@storeLibrary')->name('library.store');
Route::get('/home/library-index','LibraryController@libraryIndex');
Route::get('/home/library-index-get','LibraryController@getLibrary')->name('get-library');
Route::get('/library/delete/{id}','LibraryController@destroyLibrary');
Route::get('/home/library-show/{library}','LibraryController@libraryShow');
Route::get('/home/library-edit/{library}','LibraryController@editLibrary');
Route::post('/library-update/{library}','LibraryController@updateLibrary');
Route::get('/home/library-upload','LibraryController@createLibrary');
Route::get('/home/show-uploaded-libraries/{library}','HomeController@showLibraryUploaded');
Route::post('/getDownload', 'HomeController@getDownload')->name('get-download');


//Excel store
Route::post('excel-store','ExcelController@storeExcel')->name('excel.store');


//pdf store
Route::post('pdf-store','LibraryPdfController@storePDF')->name('pdf.store');

//video store
Route::post('video-store','LibraryVideoController@storeVideo')->name('video.store');

//Library Clerk
Route::get('/clerk-views/library-uploads','LibraryController@libraryUpload');
Route::get('/clerk-views/library-index','LibraryController@libraryIndex');
Route::get('/clerk-views/library-index-get','LibraryController@getLibrary')->name('get-clerk-library');
Route::get('/clerk-views/library-show/{library}','LibraryController@libraryShow');
Route::get('/clerk-views/library-edit/{library}','LibraryController@editLibrary');
Route::post('/library-update/{library}','LibraryController@updateLibrary');
Route::get('/clerk-views/library-upload','LibraryController@createLibrary');

//Users
Route::get('/users/create-user', 'UserController@createUser');
Route::post('/users/create-user', 'UserController@storeUser')->name('add-users');
Route::get('/users/edit-user/{user}', 'UserController@editUser');
Route::post('update-edit-user/{user}','UserController@updateUser');
Route::get('/users/index','UserController@index');
Route::get('/users/index-get-users','UserController@getUsers')->name('get-users');
Route::get('/user/delete/{id}','UserController@destroyUser');








