<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Library extends BaseModel
{
    protected $fillable = [
        'title','description','date_uploaded','category','user_id',
    ];

    public function user(){
        return $this->belongsTo(User::class,"user_id");
    }
    public function excel(){
        return $this->hasMany(LibraryExcelSheet::class,'library_id');
    }

    public function images(){
        return $this->hasMany(LibraryImage::class,'library_id');
    }

    public function videos(){
        return $this->hasMany(LibraryVideo::class,'library_id');
    }

    public function pdfs(){
        return $this->hasMany(LibraryPdf::class,'library_id');
    }
    public function uploads(){
        return $this->hasMany(LibraryUpload::class,'library_id');
    }
}
