<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryUpload extends BaseModel
{
    protected $fillable=[
        'id','image_url','pdf_url','video_url','library_id'
    ];

    public function libraries(){
        return $this->belongsTo(Library::class,'library_id');
    }

}
