<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryImage extends BaseModel
{
    //
    protected $fillable=[
        'id','image_url','library_id'
    ];

    public function library(){
        return $this->belongsTo(Library::class,'library_id');
    }
}
