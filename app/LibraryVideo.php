<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryVideo extends BaseModel
{
    //
    protected $fillable=[
        'id','video_url','library_id'
    ];

    public function library(){
        return $this->belongsTo(Library::class,'library_id');
    }
}
