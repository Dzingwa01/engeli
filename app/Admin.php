<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Admin extends BaseModel
{
    use Notifiable;

    protected $admin;
    protected $email;

   public function _construct(){
       $this->admin = config('admin.name');
       $this->admin = config('admin.email');
   }
}
