<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibraryPdf extends BaseModel
{
    //
    protected $fillable=[
        'id','pdf_url','library_id','fileName'
    ];

    public function library(){
        return $this->belongsTo(Library::class,'library_id');
    }
}
