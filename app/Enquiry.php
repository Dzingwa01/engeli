<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends BaseModel
{
    protected $fillable= [
        'name','surname','email','cell_number','message','date_of_message','status'
    ];


}
