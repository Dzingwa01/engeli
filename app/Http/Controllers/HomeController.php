<?php

namespace App\Http\Controllers;

use App\HostEmployeeRegistrationForm;
use App\LibraryExcelSheet;
use App\LibraryPdf;
use App\RegistrationCategory;
use App\YouthRegistrationForm;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Enquiry;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;
use App\Library;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //Contact us form Enquiry to store enquiry
    public function storeEnquiries(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();
        $date = Carbon::now();

        try {
            $create_enquiry = Enquiry::create(['name' => $input['name'],
                'email' => $input['email'], 'message' => $input['message'],
                'surname' => $input['surname'], 'cell_number' => $input['cell_number'],
                'date_of_message' => $date]);
            $create_enquiry->save();

            DB::commit();

            return response()->json(['message' => 'Message sent']);


        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }

    }

    public function storeStatus(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();

        try {
            $enquiry_id = $input['enquiry_id'];

            $current_enquiry = Enquiry::find($enquiry_id);

            if (isset($current_enquiry)) {
                $current_enquiry->update(['status' => $input['status']]);
                $current_enquiry->save();
                DB::commit();

                return response()->json(['message' => 'Status saved']);
            } else {
                return response("Enquiry not found");
            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }

    }


    public function index()
    {
        $user = Auth::user()->load('roles');
        $enquiry_count = count(Enquiry::all());
        if ($user->roles[0]->name == 'app-admin') {
            return view('/home', compact('enquiry_count'));

        } elseif ($user->roles[0]->name == 'clerk') {
            return view('/clerk-home');
        }
    }

    public function about()
    {
        return view('/home/aboutpage');
    }

    public function team()
    {
        return view('/home/engeliTeam');
    }

    public function methodology()
    {
        return view('/home/ourMethodology');
    }

    public function services()
    {
        return view('/home/ourServices');
    }

    public function documentsInfo()
    {
        $supply_chain_library = Library::where('category', 'Supply Chain Development & Localization')->get()->load('images', 'pdfs', 'videos', 'excel');
        $transformation_solution_library = Library::where('category', 'Transformation Solution')->get()->load('images', 'pdfs', 'videos', 'excel');
        $business_incubation_library = Library::where('category', 'Business Incubation')->get()->load('images', 'pdfs', 'videos', 'excel');
        $loan_management_library = Library::where('category', 'Loan Management')->get()->load('images', 'pdfs', 'videos', 'excel');
        $fund_management_library = Library::where('category', 'Human Capital Solutions')->get()->load('images', 'pdfs', 'videos', 'excel');
        $fund_management = Library::where('category', 'Fund Management')->get()->load('images', 'pdfs', 'videos', 'excel');
        $private_equity_library = Library::where('category', 'Private Equity Solution')->get()->load('images', 'pdfs', 'videos', 'excel');

        return view('/home/documents', compact('supply_chain_library', 'transformation_solution_library','fund_management', 'business_incubation_library', 'loan_management_library', 'fund_management_library', 'private_equity_library'));
    }

    public function contact()
    {
        return view('/home/contactUs');
    }

    //Show uploaded Library
    public function showLibraryUploaded(Library $library)
    {
        return view('/home/show-uploaded-libraries', compact('library'));
    }


    //Download excel sheet
    public function downloadFileExcel($id)
    {
        $libraryExcel = LibraryExcelSheet::find($id);
        //This is the magic function
        return Storage::download($libraryExcel->excel_sheet, $libraryExcel->fileName.'.xls');

    }

    //Download pdf
    public function downloadFilePdf($id)
    {
        $libraryPdf = LibraryPdf::find($id);

        return Storage::download($libraryPdf->pdf_url,$libraryPdf->fileName.'.pdf');
    }

    //Yes Program
    public function youthProgramme(){
        $registrationCategory = RegistrationCategory::all();

        return view('home.yes-for-youth-program',compact('registrationCategory'));
    }

    public function registration(RegistrationCategory $registrationCategory){
        $registrationCategory = RegistrationCategory::all();
        return view('home.registration-form',compact('registrationCategory'));
    }

    public function storeYouthForm(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();
        $category_id = $input['category_id'];
        try {
            $registration = YouthRegistrationForm::create(['first_name' => $input['first_name'],
                'last_name' => $input['last_name'], 'race' => $input['race'], 'gender' => $input['gender'],
                'physical_address' => $input['physical_address'], 'email_address' => $input['email_address'],
                'identity_number' => $input['identity_number'], 'cell_number' => $input['cell_number'],
                'highest_qualification' => $input['highest_qualification'], 'category_id' => $input['category_id']]);
            if ($request->hasFile('attachments')) {
                $attachment = $request->file('attachments')->store('documents_uploads');
                if (isset($registration)) {
                    $registration->update(['attachments' => $attachment]
                    );
                } else {
                    $registration->create(['attachments' => $attachment]
                    );
                }
            }

            $registration->save();

            DB::commit();
            return response()->json(['registration'=>$registration,'message'=>'Registered successfully.'],200);

        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Registration could not be saved  ' . $e->getMessage()], 400);
        }
    }

    public function storeHostEmployeeForm(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();
        $category_id = $input['category_id'];
        try {
            $registration = HostEmployeeRegistrationForm::create(['company_name' => $input['company_name'],
                'company_reg_number' => $input['company_reg_number'], 'physical_address' => $input['physical_address'],
                'contact_number' => $input['contact_number'],
                'contact_email' => $input['contact_email'], 'contact_name' => $input['contact_name'],
                'number_of_candidates' => $input['number_of_candidates'], 'industry' => $input['industry'],
                'category_id' => $category_id,'message' => $input['message']]);
            $registration->save();

            DB::commit();
            return response()->json(['registration'=>$registration,'message'=>'Registered successfully.'],200);

        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Registration could not be saved  ' . $e->getMessage()], 400);
        }
    }

    public function hostEmployeeRegistration(RegistrationCategory $registrationCategory){
        $registrationCategory = RegistrationCategory::all();
        return view('home.host-employer-reg',compact('registrationCategory'));
    }

}
