<?php

namespace App\Http\Controllers;

use App\LibraryImage;
use App\LibraryPdf;
use App\LibraryUpload;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use App\Library;

class LibraryPdfController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
    }

    //Create pdf
    public function create()
    {

    }

    //Store pdf
    public function storePDF(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();
        $user = Auth::user()->load('roles');
        $pdf_count = $input['pdf_array_count'];


        try{
            $create_library = Library::create(['title' => $input['title'],'category'=>$input['category'],
                'description' => $input['description'], 'user_id' => $user->id]);
            $create_library->save();


            for($i = 0; $i < $pdf_count; $i++){
                $cur_pdf_url = $request->file('pdf_'.$i)->store('library_uploads');

                if(isset($cur_pdf_url)){
                    $create_pdf = LibraryPdf::create(['library_id' => $create_library->id,'fileName'=>$input['fileName'],
                        'pdf_url' => $cur_pdf_url]);
                    $create_pdf->save();
                }
            }

            DB::commit();
            return response()->json(['message'=>'PDF file uploaded successfully']);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['message'=> $e]);
        }
    }


    public function show(LibraryPdf $libraryPdf)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LibraryPdf  $libraryPdf
     * @return \Illuminate\Http\Response
     */
    public function edit(LibraryPdf $libraryPdf)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LibraryPdf  $libraryPdf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LibraryPdf $libraryPdf)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LibraryPdf  $libraryPdf
     * @return \Illuminate\Http\Response
     */
    public function destroy(LibraryPdf $libraryPdf)
    {
        //
    }
}
