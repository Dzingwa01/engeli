<?php

namespace App\Http\Controllers;

use App\LibraryVideo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Library;


class LibraryVideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function storeVideo(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();
        $user = Auth::user()->load('roles');

        try{
            $create_library = Library::create(['title' => $input['title'],
                'description' => $input['description'], 'user_id' => $user->id]);
            $create_library->save();

            $create_video= LibraryVideo::create(['video_url' => $input['video_url'],'library_id' => $create_library->id]);
            $create_video->save();

            DB::commit();
            return response()->json(['message'=>'Video uploaded successfully']);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['message'=> $e]);
        }
    }


    public function show(LibraryVideo $libraryVideo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LibraryVideo  $libraryVideo
     * @return \Illuminate\Http\Response
     */
    public function edit(LibraryVideo $libraryVideo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LibraryVideo  $libraryVideo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LibraryVideo $libraryVideo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LibraryVideo  $libraryVideo
     * @return \Illuminate\Http\Response
     */
    public function destroy(LibraryVideo $libraryVideo)
    {
        //
    }
}
