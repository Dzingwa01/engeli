<?php

namespace App\Http\Controllers;

use App\LibraryImage;
use Illuminate\Http\Request;

class LibraryImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LibraryImage  $libraryImage
     * @return \Illuminate\Http\Response
     */
    public function show(LibraryImage $libraryImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LibraryImage  $libraryImage
     * @return \Illuminate\Http\Response
     */
    public function edit(LibraryImage $libraryImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LibraryImage  $libraryImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LibraryImage $libraryImage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LibraryImage  $libraryImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(LibraryImage $libraryImage)
    {
        //
    }
}
