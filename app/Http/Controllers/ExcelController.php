<?php

namespace App\Http\Controllers;

use App\LibraryExcelSheet;
use Illuminate\Http\Request;
use App\Library;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;




class ExcelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //Store pdf
    public function storeExcel(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();
        $user = Auth::user()->load('roles');
        $excel_count = $input['excel_array_count'];

        try{
            $create_library = Library::create(['title' => $input['title'],'category'=>$input['category'],
                'description' => $input['description'], 'user_id' => $user->id]);
            $create_library->save();

            for($i = 0; $i < $excel_count; $i++){
                $cur_excel_url = $request->file('excel_'.$i)->store('library_uploads');
                if(isset($cur_excel_url)){
                    $create_excel = LibraryExcelSheet::create(['library_id' => $create_library->id,'fileName'=>$input['fileName'],
                        'excel_sheet' => $cur_excel_url]);
                    $create_excel->save();
                }
            }

            DB::commit();
            return response()->json(['message'=>'Excel sheet uploaded successfully']);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['message'=> $e]);
        }
    }

}
