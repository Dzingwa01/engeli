<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Enquiry;
use Yajra\Datatables\Datatables;


class EnquiryController extends Controller
{
    //authorization
    public function __construct()
    {
        $this->middleware('auth');
    }
    //Enquiry index
    public function enquiryIndex(){
        $user = Auth::user()->load('roles');
        if($user->roles[0]->name=='app-admin' or $user->roles[0]->name == 'clerk_permissions'){
            return view('/home/enquiry-index');
        }

    }


    //Get Enquiry
    public function getEnquiry(){

        $enquiries = Enquiry::all();
        $user = Auth::user()->load('roles');


        if($user->roles[0]->name=='app-admin' or $user->roles[0]->name == 'clerk_permissions'){
            return Datatables::of($enquiries)->addColumn('action', function ($enquiry) {
                $sh = '/home/enquiry-show/' . $enquiry->id;
                $del = '/enquiry/delete/' . $enquiry->id;
                return '<a href=' . $sh . ' title="Show enquiry" style="color:green!important;"><i class="material-icons">visibility</i></a><a href=' . $del . ' onclick="confirm_delete_enquiry(this)" title="Delete enquiry" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    // Enquiry show
    public function enquiryShow($id){
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'clerk_permissions') {

            $enquiry = Enquiry::find($id);

            return view('/home/enquiry-show', compact('enquiry'));
        }
    }

    //Delete Enquiry
    public function destroyEnquiry($id)
    {
        $enquiry = Enquiry::find($id);
        $enquiry->delete();

        return view('/home/enquiry-index');
    }


}
