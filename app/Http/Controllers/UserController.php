<?php

namespace App\Http\Controllers;

use App\HostEmployeeRegistrationForm;
use App\LibraryVideo;
use App\QuestionsCategory;
use App\RegistrationCategory;
use App\VideoUpload;
use App\YouthRegistrationForm;
use Illuminate\Http\Request;
use App\Http\Requests\UserStoreRequest;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Role;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*Create Users*/
    public function createUser()
    {
        $roles = Role::orderBy('name', 'asc')->get();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'clerk') {
            return view('/users/create-user', compact('types', 'roles'));
        } else {
            return view('welcome');
        }
    }
    //Store user
    public function storeUser(Request $request){

        $input = $request->all();
        DB::beginTransaction();

        try {

            $user = User::create(['role_id'=>$input['role_id'],'name'=>$input['name'],'address'=>$input['address'],
                'surname'=>$input['surname'],'contact_number'=>$input['contact_number'],
                'email'=>$input['email'],'password'=>Hash::make('secret')]);

            $user->roles()->attach($input['role_id']);
            $user = $user->load('roles');
//
            DB::commit();
            return response()->json(['user'=>$user,'message'=>'User added successfully.'],200);

        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'User could not be saved  ' . $e->getMessage()], 400);
        }

    }
    //Edit User
    public function editUser(User $user)
    {
        $user->load('roles');
        $roles = Role::orderBy('name', 'asc')->get();
        return view('/users/edit-user', compact( 'user', 'roles' ));

    }
    //Update User
    public function updateUser(Request $request, User $user)
    {
        DB::beginTransaction();
        try {
            $input = $request->all();
            if ($request->has('role_id')) {

                $user->update(['name'=>$input['name'],'address'=>$input['address'],
                    'surname'=>$input['surname'],'contact_number'=>$input['contact_number'],
                    'email'=>$input['email'],'password'=>Hash::make('secret')]);

                $user->roles()->sync($input['role_id']);
                $user->save();
                DB::commit();
                return response()->json(['message'=>'User updated successfully.','user'=>$user],200);

            } else {
                return ('Not working');
            }
        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'User could not be saved ' . $e->getMessage()], 400);
        }

    }
    //User Index
    public function index()
    {
        //
        $roles = Role::where('name','!=','app-admin')->get();
        return view('/users/index',compact('roles'));

    }
    //Get Users
    public  function getUsers(){

        $user = Auth::user()->load('roles');

        $usersRoles = User::with('roles')->get();

        if ($user->roles[0]->name == 'app-admin' or $user->roles[0]->name == 'material-supervisor') {
            return Datatables::of($usersRoles)->addColumn('action', function ($user) {
                $edit = '/users/edit-user/' . $user->id;
                $sh = '/user/show/' . $user->id;
                $del = '/user/delete/' . $user->id;
                return '<a href=' . $edit . ' title="Edit User"><i class="material-icons">create</i></a><a href=' . $del . ' title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }


    }
    //Delete User
    public function destroyUser($id)
    {
        $user = User::find($id);
        $user->delete();

        return view('/users/index');
    }

    //YES PROGRAMME
    public function createCategory(){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.categories.create-category');
        }
    }

    public function storeRegistrationCategory(Request $request)
    {
        DB::beginTransaction();
        $input = $request->all();
        $user = Auth::user()->load('roles');

        try {
            $create_category = RegistrationCategory::create(['category_name' => $input['category_name']]);
            if ($request->hasFile('category_image')) {
                $image_url_path = $request->file('category_image')->store('category_uploads');
                if (isset($create_category)) {
                    $create_category->update(['category_image' => $image_url_path]
                    );
                } else {
                    $create_category->create(['category_image' => $image_url_path]
                    );
                }
            }
            $create_category->save();


            DB::commit();
            return response()->json(['message' => 'Category added successfully']);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => $e]);
        }
    }

    public function categoryIndex(){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.categories.category-index');
        }
    }

    public function getCategory()
    {

        $category = RegistrationCategory::all();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($category)->addColumn('action', function ($category) {
                $del = '/destroy-category/' . $category->id;
                $edit = '/edit-category/' . $category->id;
                return '<a href=' . $edit . ' title="Edit Category" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete(this)" title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function youthCanditateIndex(){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.youth.youth-candidate');
        }
    }

    public function getYouthCandidates()
    {

        $youth = YouthRegistrationForm::all();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($youth)->addColumn('action', function ($category) {
                $sh = '/youth-account-overview/' . $category->id;
                $del = '/destroy-youth-candidate/' . $category->id;
                return '<a href=' . $sh . ' title="Account overview" style="color:green!important;"><i class="material-icons" style="color: darkslategray;">person</i></a><a href=' . $del . ' onclick="confirm_delete(this)" title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function hostEmployeesCanditateIndex(){
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return view('users.host-employees.host-employees-index');
        }
    }

    public function getHostEmployeeCandidates()
    {

        $host = HostEmployeeRegistrationForm::all();
        $user = Auth::user()->load('roles');

        if ($user->roles[0]->name == 'app-admin') {
            return Datatables::of($host)->addColumn('action', function ($category) {
                $sh = '/host-employee-account-overview/' . $category->id;
                $del = '/destroy-host-employees/' . $category->id;
                return '<a href=' . $sh . ' title="Account overview" style="color:green!important;"><i class="material-icons" style="color: darkslategray;">person</i></a><a href=' . $del . ' onclick="confirm_delete(this)" title="Delete" style="color:red"><i class="material-icons">delete_forever</i></a>';
            })
                ->make(true);
        }
    }

    public function deleteHostEmployeeEmployeeCaandidates($id)
    {
        $host_employees = HostEmployeeRegistrationForm::find($id);
        $host_employees->delete();

        return view('users.host-employees.host-employees-index');
    }

    public function deleteYouthCaandidates($id)
    {
        $youth = YouthRegistrationForm::find($id);
        $youth->delete();

        return view('users.youth.youth-candidate');
    }

    public function deleteCategory($id)
    {
        $youth = RegistrationCategory::find($id);
        $youth->delete();

        return view('users.categories.category-index');
    }

    public  function editCategogy(RegistrationCategory $registrationCategory){
        return view('users.categories.edit-category',compact('registrationCategory'));
    }

    public function updateCategory(Request $request, RegistrationCategory $registrationCategory)
    {
        $user = Auth::user()->load('roles');
        DB::beginTransaction();
        $input = $request->all();

        if ($user->roles[0]->name == 'app-admin') {
            try {
                $registrationCategory->update(['category_name' => $input['category_name']]);

                $registrationCategory->save();
                if ($request->hasFile('category_image')) {
                    $image_url_path = $request->file('category_image')->store('documents_uploads');
                    if (isset($registrationCategory)) {
                        $registrationCategory->update(['category_image' => $image_url_path]
                        );
                    }
                }
                DB::commit();
                return response()->json(['message' => 'Category updated successfully.', 'registrationCategory' => $registrationCategory], 200);
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['message' => 'Category could not be saved at the moment ' . $e->getMessage(), 'registrationCategory' => $registrationCategory], 400);
            }
        }
    }

    public function youthAccountOverview(YouthRegistrationForm $youthRegistrationForm){
        return view('users.youth.youth-account-overview',compact('youthRegistrationForm'));
    }
    //Download File
    public function downloadAttachments($id)
    {
        $file = YouthRegistrationForm::find($id);

        return Storage::download($file->attachments,$file->first_name.'.pdf');
    }

    public function hostOverview(HostEmployeeRegistrationForm $hostEmployeeRegistrationForm){
        return view('users.host-employees.host-account-overview',compact('hostEmployeeRegistrationForm'));
    }
}
