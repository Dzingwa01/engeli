<?php

namespace App\Http\Controllers;

use App\LibraryImage;
use App\LibraryPdf;
use App\LibraryUpload;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use App\Library;
use App\LibraryExcelSheet;

class LibraryController extends Controller
{
    //Access
    public function __construct()
    {
        $this->middleware('auth');
    }

    //Library index
    public function libraryIndex(){
        $user = Auth::user()->load('roles');
        if($user->roles[0]->name=='app-admin'){
            return view('/home/library-index');
        }elseif ($user->roles[0]->name=='clerk'){
            return view('/clerk-views/library-index');
        }
    }

    //Library upload
    public function libraryUpload(){
        $user = Auth::user()->load('roles');
        if($user->roles[0]->name=='app-admin'){
            return view('/home/library-uploads');
        }elseif ($user->roles[0]->name=='clerk'){
            return view('/clerk-views/library-uploads');
        }
    }

    /*Library /uploads store. Use the other controller i.e LibraryVideo and LibraryPDF for storing
    and updating other media*/
    public function storeLibrary(Request $request){
        DB::beginTransaction();
        $input = $request->all();
        $date = Carbon::now();
        $user = Auth::user()->load('roles');
        $images_count = $input['image_array_count'];

        try{
            $create_library = Library::create(['title' => $input['title'],'category'=>$input['category'],
                'description' => $input['description'], 'user_id' => $user->id]);
            $create_library->save();

            for($i = 0; $i < $images_count; $i++){
                $cur_image_url = $request->file('image_'.$i)->store('library_uploads');
                if(isset($cur_image_url)){
                    $create_image = LibraryImage::create(['library_id' => $create_library->id,
                        'image_url' => $cur_image_url]);
                    $create_image->save();
                }
            }

            DB::commit();
            return response()->json(['message'=>'File uploaded successfully']);

        }catch (\Exception $e){
            DB::rollback();
            return response()->json(['message'=> $e]);
        }

    }

    //Get Library
    public function getLibrary(){

        $libraries = Library::all();
        $user = Auth::user()->load('roles');


        if($user->roles[0]->name=='app-admin'){
            return Datatables::of($libraries)->addColumn('action', function ($library) {
                $sh = '/home/library-show/' . $library->id;
                $del = '/library/delete/' . $library->id;
                $edit ='/home/library-edit/'.$library->id;
                return '<a href=' . $sh . ' title="Show library" style="color:green!important;"><i class="material-icons">visibility</i></a><a href=' . $edit . ' title="Edit Library" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete_library(this)" title="Delete library" style="color:red"><i class="material-icons">delete_forever</i>';
            })
                ->make(true);
        }elseif($user->roles[0]->name=='clerk'){
            return Datatables::of($libraries)->addColumn('action', function ($library) {
                $sh = '/clerk-views/library-show/' . $library->id;
                $del = '/library/delete/' . $library->id;
                $edit ='/clerk-views/library-edit/'.$library->id;
                return '<a href=' . $sh . ' title="Show library" style="color:green!important;"><i class="material-icons">visibility</i></a><a href=' . $edit . ' title="Edit Library" style="color:green!important;"><i class="material-icons">create</i></a><a href=' . $del . ' onclick="confirm_delete_library(this)" title="Delete library" style="color:red"><i class="material-icons">delete_forever</i>';
            })
                ->make(true);
        }
    }

    //Delete Library
    public function destroyLibrary($id)
    {
        $library = Library::find($id);
        $library->delete();

        return view('/home/library-index');


    }


    // Library show
    public function libraryShow($id){
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {

            //We need to load all the media for the specific library
            $library = Library::find($id)->load('excel','pdfs');

            return view('/home/library-show', compact('library'));
        }elseif ($user->roles[0]->name == 'clerk'){

            $library = Library::find($id)->load('images','pdfs','videos');
            return view('/clerk-views/library-show', compact('library'));
        }
    }

    //Edit library
    public function editLibrary(Library $library)
    {
        $user = Auth::user()->load('roles');
        if($user->roles[0]->name=="app-admin"){

            $library->load('images','pdfs','videos');
            return view('home.library-edit', compact('library'));

        }elseif ($user->roles[0]->name == 'clerk'){

            $library->load('images','pdfs','videos');
            return view('clerk-views.library-edit', compact('library'));
        }

    }

    //Update Library
    public function updateLibrary(Request $request, Library $library)
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            DB::beginTransaction();

            try {
                $input = $request->all();
                if ($request->has('title')) {

                    $library->update(['title' => $input['title'], 'description' => $input['description']]);

                    $library->save();

                    if ($request->has('image_url')) {
                        if (isset($library)) {
                            $library->update(['image_url' => $input['image_url']]);
                        } else {
                            $library->update(['image_url' => $input['v']]);
                        }
                    }
                    DB::commit();
                    return response()->json(['message' => 'Updated successfully.', 'library' => $library], 200);

                } elseif ($user->roles[0]->name == 'clerk'){
                    $library->update(['title' => $input['title'], 'description' => $input['description']]);

                    $library->save();

                    if ($request->has('image_url')) {
                        if (isset($library)) {
                            $library->update(['image_url' => $input['image_url']]);
                        } else {
                            $library->update(['image_url' => $input['v']]);
                        }
                    }
                    DB::commit();
                    return response()->json(['message' => 'Updated successfully.', 'library' => $library], 200);
                }
            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['message' => 'Error ' . $e->getMessage()], 400);
            }

        }
    }

    //Create Library
    public function createLibrary()
    {
        $user = Auth::user()->load('roles');
        if ($user->roles[0]->name == 'app-admin') {
            return view('/home/library-uploads');
        } elseif ($user->roles[0]->name == 'clerk'){
            return view('/clerk-views/library-uploads');
        }
    }

    //Show uploaded Library
    public function showLibraryUploaded(Library $library)
    {
        return view('/home/show-uploaded-libraries', compact('library'));
    }


}
