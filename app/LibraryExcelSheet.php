<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class LibraryExcelSheet extends BaseModel
{
    protected $fillable=[
        'id','excel_sheet','library_id','fileName'
    ];
    public function library(){
        return $this->belongsTo(Library::class,'library_id');
    }
}
