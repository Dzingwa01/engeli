<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistrationCategory extends BaseModel
{
    protected $fillable = ['category_name','category_image'];

    public function youth_registration(){
        return $this->hasMany(YouthRegistrationForm::class, 'category_id');
    }

    public function host_employee(){
        return $this->hasMany(HostEmployeeRegistrationForm::class, 'category_id');
    }
}
