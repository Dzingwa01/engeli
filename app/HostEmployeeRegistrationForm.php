<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HostEmployeeRegistrationForm extends BaseModel
{
    protected $fillable = ['category_id','company_name','company_reg_number','physical_address',
        'contact_name','contact_email','contact_number','number_of_candidates','industry','message'];

    public function category()
    {
        return $this->belongsTo(RegistrationCategory::class, 'category_id');
    }
}
