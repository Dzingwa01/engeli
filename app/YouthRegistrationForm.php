<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class YouthRegistrationForm extends BaseModel
{
    protected $fillable = ['category_id','first_name','last_name','race','gender','identity_number',
        'physical_address','email_address','cell_number','highest_qualification','attachments'];

    public function category()
    {
        return $this->belongsTo(RegistrationCategory::class, 'category_id');
    }
}
