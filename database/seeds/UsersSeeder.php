<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $password = \Illuminate\Support\Facades\Hash::make('secret');
        $verification_time = \Carbon\Carbon::now();

        $super = User::create(['name'=>'Admin', 'surname'=>'User', 'email'=>'admin@engeli.co.za',
            'contact_number'=>'041112255', 'email_verified_at'=>$verification_time, 'password'=>$password]);

        $super_role = Role::where('name', 'app-admin')->first();
        $super->roles()->attach($super_role->id);


    }
}
