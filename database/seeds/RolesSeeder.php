<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $super_admin = [
            'super-delete' => true,
            'super-add' => true,
            'super-update' => true,
            'super-view' => true,
        ];

        $clerk_permissions = [
            'clerk-delete' => true,
            'clerk-add' => true,
            'clerk-update' => true,
            'clerk-view' => true,
        ];

        $marketing_permissions = [
            'marketing-delete' => true,
            'marketing-add' => true,
            'marketing-update' => true,
            'marketing-view' => true,
        ];

        $hr_role = [
            'hr-delete' => true,
            'hr-add' => true,
            'hr-update' => true,
            'hr-view' => true,
        ];

        $content = [
            'content-delete' => true,
            'content-add' => true,
            'content-update' => true,
            'content-view' => true,
        ];

        $super_user = Role::create([
            'name' => 'app-admin',
            'display_name'=>'App Admin',
            'permissions' =>$super_admin,
            'guard_name'=>'web'
        ]);

        $marketting = Role::create([
            'name' => 'marketing',
            'display_name'=>'Marketing',
            'permissions' =>$marketing_permissions,
            'guard_name'=>'web'
        ]);

        $clerk = Role::create([
            'name' => 'clerk',
            'display_name'=>'Clerk',
            'permissions' =>$clerk_permissions,
            'guard_name'=>'web'
        ]);

        $hr = Role::create([
            'name' => 'human-resources',
            'display_name'=>'Human Resources',
            'permissions' =>$hr_role,
            'guard_name'=>'web'
        ]);

        $content_manager = Role::create([
            'name' => 'content-manager',
            'display_name'=>'Content Manager',
            'permissions' =>$content,
            'guard_name'=>'web'
        ]);

    }
}
